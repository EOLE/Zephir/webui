import { alertActions } from './alert.actions';
import { Zephir } from '../services';

export const execActions = {
  listServerJobs,
  describeJob,
  deployConfiguration,
  saltExec,
};

export const EXEC_LIST_JOBS_REQUEST = 'EXEC_LIST_JOBS_REQUEST';
export const EXEC_LIST_JOBS_SUCCESS = 'EXEC_LIST_JOBS_SUCCESS';
export const EXEC_LIST_JOBS_FAILURE = 'EXEC_LIST_JOBS_FAILURE';

function listServerJobs(server_id) {
  return (dispatch) => {
    dispatch(request(server_id));
    return Zephir.v1.server.exec.list({ server_id })
      .then(({response}) => {
        dispatch(success(response));
        return {jobs: response};
      })
      .catch(err => {
        dispatch(failure(err));
        dispatch(alertActions.error(err.toString()));
        throw err;
      });
  };

  function request(server_id) { return { type: EXEC_LIST_JOBS_REQUEST, server_id }; }
  function success(jobs) { return { type: EXEC_LIST_JOBS_SUCCESS, jobs }; }
  function failure(error) { return { type: EXEC_LIST_JOBS_FAILURE, error }; }
}

export const EXEC_DESCRIBE_JOB_REQUEST = 'EXEC_DESCRIBE_JOB_REQUEST';
export const EXEC_DESCRIBE_JOB_SUCCESS = 'EXEC_DESCRIBE_JOB_SUCCESS';
export const EXEC_DESCRIBE_JOB_FAILURE = 'EXEC_DESCRIBE_JOB_FAILURE';

function describeJob(job_id) {
  return (dispatch) => {
    dispatch(request());
    return Zephir.v1.server.exec.describe({ job_id, automation: 'salt' })
      .then(({response}) => {
        dispatch(success(response[0]));
        return response[0];
      })
      .catch(err => {
        dispatch(failure(err));
        dispatch(alertActions.error(err.toString()));
        throw err;
      });
  };

  function request() { return { type: EXEC_DESCRIBE_JOB_REQUEST }; }
  function success(job) { return { type: EXEC_DESCRIBE_JOB_SUCCESS, job }; }
  function failure(error) { return { type: EXEC_DESCRIBE_JOB_FAILURE, error }; }
}

export const EXEC_DEPLOY_CONFIGURATION_REQUEST = 'EXEC_DEPLOY_CONFIGURATION_REQUEST';
export const EXEC_DEPLOY_CONFIGURATION_SUCCESS = 'EXEC_DEPLOY_CONFIGURATION_SUCCESS';
export const EXEC_DEPLOY_CONFIGURATION_FAILURE = 'EXEC_DEPLOY_CONFIGURATION_FAILURE';

function deployConfiguration(server_id) {
  return (dispatch) => {
    dispatch(request());
    return Zephir.v1.server.exec.deploy({ server_id })
      .then(({response}) => {
        dispatch(success(response));
        dispatch(alertActions.success("L'ordre de déploiement de la configuration a été envoyé."));
        return response;
      })
      .catch(err => {
        dispatch(failure(err));
        dispatch(alertActions.error(err.toString()));
        throw err;
      });
  };

  function request() { return { type: EXEC_DEPLOY_CONFIGURATION_REQUEST }; }
  function success(result) { return { type: EXEC_DEPLOY_CONFIGURATION_SUCCESS, result }; }
  function failure(error) { return { type: EXEC_DEPLOY_CONFIGURATION_FAILURE, error }; }
}


export const EXEC_SALT_EXEC_REQUEST = 'EXEC_SALT_EXEC_REQUEST';
export const EXEC_SALT_EXEC_SUCCESS = 'EXEC_SALT_EXEC_SUCCESS';
export const EXEC_SALT_EXEC_FAILURE = 'EXEC_SALT_EXEC_FAILURE';

function saltExec(server_id, command, args) {
  return (dispatch) => {
    dispatch(request(server_id, command, args));
    return Zephir.v1.server.exec.command({
      server_id,
      command,
      //args: Array.isArray(args) ? args.join(' ') : args.toString(),
    })
      .then(({response}) => {
        dispatch(success(response));
        dispatch(alertActions.success("L'ordre d'exécution de la commande a été envoyé."));
        return response;
      })
      .catch(err => {
        dispatch(failure(err));
        dispatch(alertActions.error(err.toString()));
        throw err;
      });
  };

  function request(server_id, command, args) { return { type: EXEC_SALT_EXEC_REQUEST, server_id, command, args }; }
  function success(result) { return { type: EXEC_SALT_EXEC_SUCCESS, result }; }
  function failure(error) { return { type: EXEC_SALT_EXEC_FAILURE, error }; }
}
