import { Zephir } from '../services/zephir.service';
import { alertActions } from './alert.actions';

export const serverselectionActions = {
    list,
    describe,
    create,
    update,
    updateLocalFilter,
    server_set,
    user_add,
    user_remove,
    user_update,
    delete: _delete,
    getAll: () => ({type: 'GETALL_NOOP'}),
  };

export const SERVERSELECTION_LIST_REQUEST = 'SERVERSELECTION_LIST_REQUEST';
export const SERVERSELECTION_LIST_SUCCESS = 'SERVERSELECTION_LIST_SUCCESS';
export const SERVERSELECTION_LIST_FAILURE = 'SERVERSELECTION_LIST_FAILURE';

function list() {
  return dispatch => {
    dispatch(request());
    return Zephir.v1.serverselection.list()
      .then(({response}) => {
        dispatch(success(response));

        return response;
      })
      .catch(error => {
        dispatch(failure(error));
        dispatch(alertActions.error(error.message));
        throw error;
      });
  };
  function request() { return { type: SERVERSELECTION_LIST_REQUEST }; }
  function success(serverselections) { return { type: SERVERSELECTION_LIST_SUCCESS, serverselections }; }
  function failure(error) { return { type: SERVERSELECTION_LIST_FAILURE, error }; }
}

export const SERVERSELECTION_DESCRIBE_REQUEST = 'SERVERSELECTION_DESCRIBE_REQUEST';
export const SERVERSELECTION_DESCRIBE_SUCCESS = 'SERVERSELECTION_DESCRIBE_SUCCESS';
export const SERVERSELECTION_DESCRIBE_FAILURE = 'SERVERSELECTION_DESCRIBE_FAILURE';

function describe(serverselectionid) {
  return dispatch => {
    const serverselectionId =  parseInt(serverselectionid, 10);
    dispatch(request(serverselectionId));
    return Zephir.v1.serverselection.describe({serverselectionid: serverselectionId})
      .then(({response}) => {
        dispatch(success(response));
        return response;
      })
      .catch(error => {
        dispatch(failure(error));
        dispatch(alertActions.error(error.message));
        throw error;
      });
  };
  function request(serverselectionid) { return { type: SERVERSELECTION_DESCRIBE_REQUEST, serverselectionId: serverselectionid }; }
  function success(serverselection) { return { type: SERVERSELECTION_DESCRIBE_SUCCESS, serverselection }; }
  function failure(error) { return { type: SERVERSELECTION_DESCRIBE_FAILURE, error }; }
}

export const SERVERSELECTION_CREATE_REQUEST = 'SERVERSELECTION_CREATE_REQUEST';
export const SERVERSELECTION_CREATE_SUCCESS = 'SERVERSELECTION_CREATE_SUCCESS';
export const SERVERSELECTION_CREATE_FAILURE = 'SERVERSELECTION_CREATE_FAILURE';

function create({ serverselectionname, serverselectiondescription}) {
  return dispatch => {
    dispatch(request());
    return Zephir.v1.serverselection.create({
      serverselectionname,
      serverselectiondescription
    })
      .then(({response}) => {
        dispatch(success(response));
        dispatch(alertActions.success('Votre sélection de serveurs a été créée.'));
        return response;
      })
      .catch(error => {
        dispatch(failure(error));
        dispatch(alertActions.error(error.message));
        throw error;
      });
  };
  function request() { return { type: SERVERSELECTION_CREATE_REQUEST }; }
  function success(serverselection) { return { type: SERVERSELECTION_CREATE_SUCCESS, serverselection }; }
  function failure(error) { return { type: SERVERSELECTION_CREATE_FAILURE, error }; }
}

export const SERVERSELECTION_UPDATE_REQUEST = 'SERVERSELECTION_UPDATE_REQUEST';
export const SERVERSELECTION_UPDATE_SUCCESS = 'SERVERSELECTION_UPDATE_SUCCESS';
export const SERVERSELECTION_UPDATE_FAILURE = 'SERVERSELECTION_UPDATE_FAILURE';

function update({ serverselectionid, serverselectionname, serverselectiondescription}) {
  return dispatch => {
    dispatch(request());
    return Zephir.v1.serverselection.update({
      serverselectionname,
      serverselectiondescription,
      serverselectionid: serverselectionid,
      requete: ''
    })
      .then(({response}) => {
        dispatch(success(response));
        dispatch(alertActions.success(`La sélection "${serverselectionname}" a été mise à jour.`));
        return response;
      })
      .catch(error => {
        dispatch(failure(error));
        dispatch(alertActions.error(error.message));
        throw error;
      });
  };
  function request() { return { type: SERVERSELECTION_UPDATE_REQUEST }; }
  function success(serverselection) { return { type: SERVERSELECTION_UPDATE_SUCCESS, serverselection }; }
  function failure(error) { return { type: SERVERSELECTION_UPDATE_FAILURE, error }; }
}

export const SERVERSELECTION_DELETE_REQUEST = 'SERVERSELECTION_DELETE_REQUEST';
export const SERVERSELECTION_DELETE_SUCCESS = 'SERVERSELECTION_DELETE_SUCCESS';
export const SERVERSELECTION_DELETE_FAILURE = 'SERVERSELECTION_DELETE_FAILURE';

function _delete(serverselectionId, serverName) {
  return dispatch => {
    dispatch(request(serverselectionId));
    return Zephir.v1.serverselection.delete({ serverselectionid: serverselectionId })
      .then(({response}) => {
        dispatch(success(response));
        dispatch(alertActions.success(`La sélection "#${serverselectionId} ${serverName}" a été supprimée.`));
        return response;
      })
      .catch(error => {
        dispatch(failure(error));
        dispatch(alertActions.error(error.message));
        throw error;
      });
  };
  function request(serverselectionId) { return { type: SERVERSELECTION_DELETE_REQUEST, serverselectionId }; }
  function success(result) { return { type: SERVERSELECTION_DELETE_SUCCESS, result }; }
  function failure(error) { return { type: SERVERSELECTION_DELETE_FAILURE, error }; }
}

export const SERVERSELECTION_SERVER_SET_REQUEST = 'SERVERSELECTION_SERVER_SET_REQUEST';
export const SERVERSELECTION_SERVER_SET_SUCCESS = 'SERVERSELECTION_SERVER_SET_SUCCESS';
export const SERVERSELECTION_SERVER_SET_FAILURE = 'SERVERSELECTION_SERVER_SET_FAILURE';

function server_set({ serverselectionid }, serveridlist) {
  return dispatch => {
    dispatch(request());
    return Zephir.v1.serverselection.server.set({
      serverselectionid,
      serveridlist,
    })
      .then(({response}) => {
        dispatch(success(response));
        dispatch(alertActions.success('La liste des serveurs de la sélection a été mise à jour.'));
        return response;
      })
      .catch(error => {
        dispatch(failure(error));
        dispatch(alertActions.error(error.message));
        throw error;
      });
  };
  function request() { return { type: SERVERSELECTION_SERVER_SET_REQUEST }; }
  function success(serverselection) { return { type: SERVERSELECTION_SERVER_SET_SUCCESS, serverselection }; }
  function failure(error) { return { type: SERVERSELECTION_SERVER_SET_FAILURE, error }; }
}

export const SERVERSELECTION_USER_ADD_REQUEST = 'SERVERSELECTION_USER_ADD_REQUEST';
export const SERVERSELECTION_USER_ADD_SUCCESS = 'SERVERSELECTION_USER_ADD_SUCCESS';
export const SERVERSELECTION_USER_ADD_FAILURE = 'SERVERSELECTION_USER_ADD_FAILURE';

function user_add(serverselectionid , username, role) {
  return dispatch => {
    dispatch(request());
    return Zephir.v1.serverselection.user.add({
      serverselectionid,
      username,
      role
    })
      .then(({response}) => {
        dispatch(success(response));
        dispatch(alertActions.success("L'utilisateur a été ajouté à la sélection de serveur"));
        return response;
      })
      .catch(error => {
        dispatch(failure(error));
        dispatch(alertActions.error(error.message));
        throw error;
      });
  };
  function request() { return { type: SERVERSELECTION_USER_ADD_REQUEST }; }
  function success(serverselection) { return { type: SERVERSELECTION_USER_ADD_SUCCESS, serverselection }; }
  function failure(error) { return { type: SERVERSELECTION_USER_ADD_FAILURE, error }; }
}

export const SERVERSELECTION_USER_DEL_REQUEST = 'SERVERSELECTION_USER_DEL_REQUEST';
export const SERVERSELECTION_USER_DEL_SUCCESS = 'SERVERSELECTION_USER_DEL_SUCCESS';
export const SERVERSELECTION_USER_DEL_FAILURE = 'SERVERSELECTION_USER_DEL_FAILURE';

function user_remove(serverselectionid , username) {
  return dispatch => {
    dispatch(request());
    return Zephir.v1.serverselection.user.remove({
      serverselectionid,
      username
    })
      .then(({response}) => {
        dispatch(success(response));
        dispatch(alertActions.success("L'utilisateur a été retiré de la sélection de serveurs"));
        return response;
      })
      .catch(error => {
        dispatch(failure(error));
        dispatch(alertActions.error(error.message));
        throw error;
      });
  };
  function request() { return { type: SERVERSELECTION_USER_DEL_REQUEST }; }
  function success(serverselection) { return { type: SERVERSELECTION_USER_DEL_SUCCESS, serverselection }; }
  function failure(error) { return { type: SERVERSELECTION_USER_DEL_FAILURE, error }; }
}

export const SERVERSELECTION_USER_UPDATE_REQUEST = 'SERVERSELECTION_USER_UPDATE_REQUEST';
export const SERVERSELECTION_USER_UPDATE_SUCCESS = 'SERVERSELECTION_USER_UPDATE_SUCCESS';
export const SERVERSELECTION_USER_UPDATE_FAILURE = 'SERVERSELECTION_USER_UPDATE_FAILURE';

function user_update(serverselectionid , username, role) {
  return dispatch => {
    dispatch(request());
    return Zephir.v1.serverselection.user.update({
      serverselectionid,
      username,
      role
    })
      .then(({response}) => {
        dispatch(success(response));
        dispatch(alertActions.success("L'utilisateur a été modifié dans la sélection de serveurs"));
        return response;
      })
      .catch(error => {
        dispatch(failure(error));
        dispatch(alertActions.error(error.message));
        throw error;
      });
  };
  function request() { return { type: SERVERSELECTION_USER_UPDATE_REQUEST }; }
  function success(serverselection) { return { type: SERVERSELECTION_USER_UPDATE_SUCCESS, serverselection }; }
  function failure(error) { return { type: SERVERSELECTION_USER_UPDATE_FAILURE, error }; }
}

export const SERVERSELECTION_UPDATE_LOCAL_FILTER = 'SERVERSELECTION_UPDATE_LOCAL_FILTER';
function updateLocalFilter(filter) {
  return { type: SERVERSELECTION_UPDATE_LOCAL_FILTER, filter };
}