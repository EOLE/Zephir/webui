import { Zephir } from '../services/zephir.service';
import { alertActions } from './alert.actions';

export const serverActions = {
  list,
  describe,
  create,
  updateLocalFilter,
  update,
  selectionlist,
  delete: _delete,
  getPeeringConf,
  getAll: () => ({type: 'GETALL_NOOP'}),
  getAllByLocation: () => ({type: 'GETALLBYLOCATION_NOOP'}),
};

export const SERVER_LIST_REQUEST = 'SERVER_LIST_REQUEST';
export const SERVER_LIST_SUCCESS = 'SERVER_LIST_SUCCESS';
export const SERVER_LIST_FAILURE = 'SERVER_LIST_FAILURE';

function list() {
  return dispatch => {
    dispatch(request());
    return Zephir.v1.server.list()
      .then(({response}) => {
        dispatch(success(response));
        return response;
      })
      .catch(error => {
        dispatch(failure(error));
        dispatch(alertActions.error(error.message));
        throw error;
      });
  };
  function request() { return { type: SERVER_LIST_REQUEST }; }
  function success(servers) { return { type: SERVER_LIST_SUCCESS, servers }; }
  function failure(error) { return { type: SERVER_LIST_FAILURE, error }; }
}

export const SERVER_DESCRIBE_REQUEST = 'SERVER_DESCRIBE_REQUEST';
export const SERVER_DESCRIBE_SUCCESS = 'SERVER_DESCRIBE_SUCCESS';
export const SERVER_DESCRIBE_FAILURE = 'SERVER_DESCRIBE_FAILURE';

function describe(serverid) {
  return dispatch => {
    const serverId =  parseInt(serverid, 10);
    dispatch(request(serverId));
    return Zephir.v1.server.describe({serverid: serverId, environment: true})
      .then(({response}) => {
        dispatch(success(response));
        return response;
      })
      .catch(error => {
        dispatch(failure(error));
        dispatch(alertActions.error(error.message));
        throw error;
      });
  };
  function request(serverid) { return { type: SERVER_DESCRIBE_REQUEST, serverId: serverid }; }
  function success(server) { return { type: SERVER_DESCRIBE_SUCCESS, server }; }
  function failure(error) { return { type: SERVER_DESCRIBE_FAILURE, error }; }
}

export const SERVER_CREATE_REQUEST = 'SERVER_CREATE_REQUEST';
export const SERVER_CREATE_SUCCESS = 'SERVER_CREATE_SUCCESS';
export const SERVER_CREATE_FAILURE = 'SERVER_CREATE_FAILURE';

function create({ servername, serverdescription, servermodelid, serverpassphrase }) {
  return dispatch => {
    dispatch(request());
    return Zephir.v1.server.create({
      servername,
      serverdescription,
      serverpassphrase,
      servermodelid: parseInt(servermodelid, 10)
    })
      .then(({response}) => {
        dispatch(success(response));
        dispatch(alertActions.success('Votre serveur a été créé.'));
        return response;
      })
      .catch(error => {
        dispatch(failure(error));
        dispatch(alertActions.error(error.message));
        throw error;
      });
  };
  function request() { return { type: SERVER_CREATE_REQUEST }; }
  function success(server) { return { type: SERVER_CREATE_SUCCESS, server }; }
  function failure(error) { return { type: SERVER_CREATE_FAILURE, error }; }
}

export const SERVER_UPDATE_REQUEST = 'SERVER_UPDATE_REQUEST';
export const SERVER_UPDATE_SUCCESS = 'SERVER_UPDATE_SUCCESS';
export const SERVER_UPDATE_FAILURE = 'SERVER_UPDATE_FAILURE';

function update({ serverid, servername, serverdescription }) {
  return dispatch => {
    dispatch(request());
    return Zephir.v1.server.update({
      servername,
      serverdescription,
      serverid: serverid,
    })
      .then(({response}) => {
        dispatch(success(response));
        dispatch(alertActions.success(`Le serveur "${servername}" a été mis à jour.`));
        return response;
      })
      .catch(error => {
        dispatch(failure(error));
        dispatch(alertActions.error(error.message));
        throw error;
      });
  };
  function request() { return { type: SERVER_UPDATE_REQUEST }; }
  function success(server) { return { type: SERVER_UPDATE_SUCCESS, server }; }
  function failure(error) { return { type: SERVER_UPDATE_FAILURE, error }; }
}

export const SERVER_UPDATE_LOCAL_FILTER = 'SERVER_UPDATE_LOCAL_FILTER';
function updateLocalFilter(filter) {
  return { type: SERVER_UPDATE_LOCAL_FILTER, filter };
}

export const SERVER_DELETE_REQUEST = 'SERVER_DELETE_REQUEST';
export const SERVER_DELETE_SUCCESS = 'SERVER_DELETE_SUCCESS';
export const SERVER_DELETE_FAILURE = 'SERVER_DELETE_FAILURE';

function _delete(serverId, serverName) {
  return dispatch => {
    dispatch(request(serverId));
    return Zephir.v1.server.delete({ serverid: serverId })
      .then(({response}) => {
        dispatch(success(response));
        dispatch(alertActions.success(`Le serveur "#${serverId} ${serverName}" a été supprimé.`));
        return response;
      })
      .catch(error => {
        dispatch(failure(error));
        dispatch(alertActions.error(error.message));
        throw error;
      });
  };
  function request(serverId) { return { type: SERVER_DELETE_REQUEST, serverId }; }
  function success(result) { return { type: SERVER_DELETE_SUCCESS, result }; }
  function failure(error) { return { type: SERVER_DELETE_FAILURE, error }; }
}

export const SERVER_PEERING_CONF_GET_REQUEST = 'SERVER_PEERING_CONF_GET_REQUEST';
export const SERVER_PEERING_CONF_GET_SUCCESS = 'SERVER_PEERING_CONF_GET_SUCCESS';
export const SERVER_PEERING_CONF_GET_FAILURE = 'SERVER_PEERING_CONF_GET_FAILURE';

function getPeeringConf(serverId) {
  return dispatch => {
    dispatch(request(serverId));
    return Zephir.v1.server.describe({serverid: serverId, peering: true})
    .then(({response}) => {
      response = response.peering;
      dispatch(success(response));
      return response;
      })
      .catch(error => {
        dispatch(failure(error));
        dispatch(alertActions.error(error.message));
        throw error;
      });
  };
  function request(serverId) { return { type: SERVER_PEERING_CONF_GET_REQUEST, serverId }; }
  function success(result) { return { type: SERVER_PEERING_CONF_GET_SUCCESS, result }; }
  function failure(error) { return { type: SERVER_PEERING_CONF_GET_FAILURE, error }; }
}

export const SERVER_SERVERSELECTION_LIST_REQUEST = 'SERVER_SERVERSELECTION_LIST_REQUEST';
export const SERVER_SERVERSELECTION_LIST_SUCCESS = 'SERVER_SERVERSELECTION_LIST_SUCCESS';
export const SERVER_SERVERSELECTION_LIST_FAILURE = 'SERVER_SERVERSELECTION_LIST_FAILURE';

function selectionlist(serverId) {
  return dispatch => {
    dispatch(request());
    return Zephir.v1.server.serverselection.list({ serverid: serverId })
      .then(({response}) => {
        dispatch(success(response));
        return response;
      })
      .catch(error => {
        dispatch(failure(error));
        dispatch(alertActions.error(error.message));
        throw error;
      });
  };
  function request(serverId) { return { type: SERVER_SERVERSELECTION_LIST_REQUEST }; }
  function success(selections) { return { type: SERVER_SERVERSELECTION_LIST_SUCCESS, selections }; }
  function failure(error) { return { type: SERVER_SERVERSELECTION_LIST_FAILURE, error }; }
}
