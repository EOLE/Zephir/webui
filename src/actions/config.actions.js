import { Zephir } from '../services';

export const configActions = {
  startSession,
  stopSession
};

export const CONFIG_START_SESSION_REQUEST = 'CONFIG_START_SESSION_REQUEST';
export const CONFIG_START_SESSION_SUCCESS = 'CONFIG_START_SESSION_SUCCESS';
export const CONFIG_START_SESSION_FAILURE = 'CONFIG_START_SESSION_FAILURE';

export function startSession(id, type) {
  return dispatch => {
    dispatch(request(id));
    if (type === 'server'){
      return Zephir.v1.config.session.server.start({
        id: id
      }).then(({response}) => {
        dispatch(success(response));
        return response;
      }).catch(err => {
        dispatch(failure(err));
        throw err;
      });
    }
    if (type === 'servermodel'){
      return Zephir.v1.config.session.servermodel.start({
        id: id
      }).then(({response}) => {
        dispatch(success(response));
        return response;
      }).catch(err => {
        dispatch(failure(err));
        throw err;
      });
    }

  };

  function request(id) { return { type: CONFIG_START_SESSION_REQUEST, id }; }
  function success(response) { return { type: CONFIG_START_SESSION_SUCCESS, ...response }; }
  function failure(error, id) { return { type: CONFIG_START_SESSION_FAILURE, id, error }; }

}

export const CONFIG_STOP_SESSION_REQUEST = 'CONFIG_STOP_SESSION_REQUEST';
export const CONFIG_STOP_SESSION_SUCCESS = 'CONFIG_STOP_SESSION_SUCCESS';
export const CONFIG_STOP_SESSION_FAILURE = 'CONFIG_STOP_SESSION_FAILURE';

export function stopSession(sessionId, type) {
  return dispatch => {

    dispatch(request(sessionId));
    if (type==='server'){
      return Zephir.v1.config.session.server.stop({
        sessionid: sessionId.toString()
      }).then(({response}) => {
        dispatch(success(response));
        return response;
      }).catch(err => {
        dispatch(failure(err, sessionId));
        throw err;
      });
    }
    if (type==='servermodel'){
      return Zephir.v1.config.session.servermodel.stop({
        sessionid: sessionId.toString()
      }).then(({response}) => {
        dispatch(success(response));
        return response;
      }).catch(err => {
        dispatch(failure(err, sessionId));
        throw err;
      });
    }

  };

  function request(sessionId) { return { type: CONFIG_STOP_SESSION_REQUEST, sessionId }; }
  function success(response) { return { type: CONFIG_STOP_SESSION_SUCCESS, ...response }; }
  function failure(error, sessionId) { return { type: CONFIG_STOP_SESSION_FAILURE, sessionId, error }; }

}