import { alertActions } from './alert.actions';
import { Zephir } from '../services';

export const settingsActions = {
  getSetting,
  setSetting
};

export const SETTINGS_GET_REQUEST = 'SETTINGS_GET_REQUEST';
export const SETTINGS_GET_SUCCESS = 'SETTINGS_GET_SUCCESS';
export const SETTINGS_GET_FAILURE = 'SETTINGS_GET_FAILURE';

function getSetting(username) {
  return (dispatch) => {

    dispatch(request(username));
    return Zephir.v1.identity.settings.get({username: username})
      .then(({ response }) => {
        dispatch(success(response));
        return response;
      })
      .catch(error => {
        dispatch(failure(error));
        dispatch(alertActions.error(error.message));
        throw error;
      });
  };

  function request(username) { return { type: SETTINGS_GET_REQUEST, username: username }; }
  function success(settings) { return { type: SETTINGS_GET_SUCCESS, settings }; }
  function failure(error, username) { return { type: SETTINGS_GET_FAILURE, error, username }; }

}

export const SETTINGS_SET_REQUEST = 'SETTINGS_SET_REQUEST';
export const SETTINGS_SET_SUCCESS = 'SETTINGS_SET_SUCCESS';
export const SETTINGS_SET_FAILURE = 'SETTINGS_SET_FAILURE';

function setSetting(user, settings) {
  return (dispatch) => {

    dispatch(request({user, ...settings}));
    return Zephir.v1.identity.settings.set({username: user.username, ...settings})
      .then(({ response }) => {
        dispatch(success(response));
        dispatch(alertActions.success('Les paramètres ont été mis à jour.'));
        return response;
      })
      .catch(error => {
        dispatch(failure(error, settings));
        dispatch(alertActions.error(error.message));
        throw error;
      });
  };

  function request(settings) { return { type: SETTINGS_SET_REQUEST, settings}; }
  function success(response) { return { type: SETTINGS_SET_SUCCESS, response }; }
  function failure(error, settings) { return { type: SETTINGS_SET_FAILURE, error, settings }; }

}
