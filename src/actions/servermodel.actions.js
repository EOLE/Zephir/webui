import { Zephir } from '../services';


export const servermodelActions = {
  describe,
  list,
  updateLocalFilter,
};

export const SERVERMODEL_DESCRIBE_REQUEST = 'SERVERMODEL_DESCRIBE_REQUEST';
export const SERVERMODEL_DESCRIBE_SUCCESS = 'SERVERMODEL_DESCRIBE_SUCCESS';
export const SERVERMODEL_DESCRIBE_FAILURE = 'SERVERMODEL_DESCRIBE_FAILURE';

function describe(servermodelid) {
  return dispatch => {

    dispatch(request(servermodelid));

    return Zephir.v1.servermodel.describe({
      servermodelid: typeof servermodelid === 'string' ? parseInt(servermodelid, 10) : servermodelid
    })
      .then(({response}) => {
        dispatch(success(response));
        return response;
      })
      .catch(error => {
        dispatch(failure(error));
        return error;
      });
  };


  function request(servermodelId) { return { type: SERVERMODEL_DESCRIBE_REQUEST, servermodelId }; }
  function success(servermodel) { return { type: SERVERMODEL_DESCRIBE_SUCCESS, servermodel }; }
  function failure(error) { return { type: SERVERMODEL_DESCRIBE_FAILURE, error }; }

}

export const SERVERMODEL_LIST_REQUEST = 'SERVERMODEL_LIST_REQUEST';
export const SERVERMODEL_LIST_SUCCESS = 'SERVERMODEL_LIST_SUCCESS';
export const SERVERMODEL_LIST_FAILURE = 'SERVERMODEL_LIST_FAILURE';

function list() {
  return dispatch => {
    dispatch(request());
    return Zephir.v1.servermodel.list()
      .then(({response}) => {
        dispatch(success(response));
        return { servermodels: response };
      })
      .catch(error => {
        dispatch(failure(error));
        return error;
      });
  };
  function request() { return { type: SERVERMODEL_LIST_REQUEST }; }
  function success(servermodels) { return { type: SERVERMODEL_LIST_SUCCESS, servermodels }; }
  function failure(error) { return { type: SERVERMODEL_LIST_FAILURE, error }; }
}

export const SERVERMODEL_UPDATE_LOCAL_FILTER = 'SERVERMODEL_UPDATE_LOCAL_FILTER';
function updateLocalFilter(filter) {
  return { type: SERVERMODEL_UPDATE_LOCAL_FILTER, filter };
}