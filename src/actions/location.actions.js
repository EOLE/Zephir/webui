import { locationConstants } from '../constants';
import { locationService } from '../services';
import { alertActions } from './';

export const locationActions = {
  getAll,
  getInRect,
};

function getAll() {
  return dispatch => {
    dispatch(request());
    return locationService.getAll()
      .then(
        locations => dispatch(success(locations)),
        error => {
          dispatch(failure(error));
          dispatch(alertActions.error(error.message || 'Impossible de récupérer la liste des sites géographiques'));
        }
      );
  };

  function request() { return { type: locationConstants.GETALL_REQUEST }; }
  function success(locations) { return { type: locationConstants.GETALL_SUCCESS, locations }; }
  function failure(error) { return { type: locationConstants.GETALL_FAILURE, error }; }
}

function getInRect(lat1, lon1, lat2, lon2) {
  return dispatch => {
    dispatch(request(lat1, lon1, lat2, lon2));
    return locationService.getInRect(lat1, lon1, lat2, lon2)
      .then(
        locations => dispatch(success(locations)),
        error => {
          dispatch(failure(error));
          dispatch(alertActions.error(error.message || 'Impossible de récupérer la liste des sites géographiques'));
        }
      );
  };

  function request(lat1, lon1, lat2, lon2) { return { type: locationConstants.GETINRECT_REQUEST, rect: {lat1, lon1, lat2, lon2} }; }
  function success(locations) { return { type: locationConstants.GETINRECT_SUCCESS, locations }; }
  function failure(error) { return { type: locationConstants.GETINRECT_FAILURE, error }; }
}