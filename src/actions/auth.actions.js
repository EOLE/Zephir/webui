import { Zephir } from '../services';
import { alertActions } from './';

export const AUTH_GET_SESSION_USER_REQUEST = 'AUTH_GET_SESSION_USER_REQUEST';
export const AUTH_GET_SESSION_USER_SUCCESS = 'AUTH_GET_SESSION_USER_SUCCESS';
export const AUTH_GET_SESSION_USER_FAILURE = 'AUTH_GET_SESSION_USER_FAILURE';

export function getSessionUser() {
  return dispatch => {
    dispatch(request());
    return Zephir.v1.identity.sessionUser.get()
      .then(({response}) => {
        dispatch(success(response));
        return response;
      }, error => {
        dispatch(failure(error));
        const message = error instanceof Error ? error.getMessage() : error.toString();
        dispatch(alertActions.error(message));
      });
  };

  function request(user) { return { type: AUTH_GET_SESSION_USER_REQUEST, user }; }
  function success(user) { return { type: AUTH_GET_SESSION_USER_SUCCESS, user }; }
  function failure(error) { return { type: AUTH_GET_SESSION_USER_FAILURE, error }; }
}