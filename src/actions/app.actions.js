/* globals Promise */
import { alertActions } from './alert.actions';
import { openAppInstance, closeAppInstance } from '../helpers/apps';
import { v4 } from 'uuid';

export const appActions = {
  openApp,
  closeApp
};

export const APPS_OPEN_APP_REQUEST = 'APPS_OPEN_APP_REQUEST';
export const APPS_OPEN_APP_SUCCESS = 'APPS_OPEN_APP_SUCCESS';
export const APPS_OPEN_APP_FAILURE = 'APPS_OPEN_APP_FAILURE';

export function openApp(appName, instanceOpts) {
  return (dispatch, getState) => {

    const instance = {id: v4(), appName, opts: instanceOpts};

    dispatch(request(instance));

    // TEMP L'existence des apps est pour l'instant purement "frontend"
    // à terme, il faudrait que celles ci soit "persistées" au niveau backend
    // pour pouvoir les restaurer au rafraichissement de la page

    const { apps } = getState();
    return openAppInstance(instance, apps.opened)
      .then(instance => {
        dispatch(success(instance));
        return instance;
      })
      .catch(err => {
        dispatch(failure(err, instance));
        dispatch(alertActions.error(err.toString()));
        throw err;
      });

  };

  function request(instance) { return { type: APPS_OPEN_APP_REQUEST, instance }; }
  function success(instance) { return { type: APPS_OPEN_APP_SUCCESS, instance }; }
  function failure(error, instance) { return { type: APPS_OPEN_APP_FAILURE, instance, error }; }

}

export const APPS_CLOSE_APP_REQUEST = 'APPS_OPEN_APP_REQUEST';
export const APPS_CLOSE_APP_SUCCESS = 'APPS_CLOSE_APP_SUCCESS';
export const APPS_CLOSE_APP_FAILURE = 'APPS_CLOSE_APP_FAILURE';

export function closeApp(instanceId) {
  return (dispatch, getState) => {

    dispatch(request(instanceId));

    // TEMP L'existence des apps est pour l'instant purement "frontend"
    // à terme, il faudrait que celles ci soit "persistées" au niveau backend
    // pour pouvoir les restaurer au rafraichissement de la page

    const state = getState();
    const { opened } = state.apps;
    let instance;
    for (let i = 0; (instance = opened[i]); i++) {
      if (instance.id === instanceId) break;
    }

    if(!instance) {
      const errMessage = `Impossible de trouver l'instance d'application "${instanceId}"`;
      dispatch(failure(errMessage, instanceId));
      dispatch(alertActions.error(errMessage));
      return Promise.reject(new Error(errMessage));
    }

    return closeAppInstance(instance)
      .then(() => {
        dispatch(success(instance));
        return instance;
      })
      .catch(err => {
        dispatch(failure(err, instanceId));
        dispatch(alertActions.error(err.toString()));
        throw err;
      });

  };

  function request(instanceId) { return { type: APPS_CLOSE_APP_REQUEST, instanceId }; }
  function success(instance) { return { type: APPS_CLOSE_APP_SUCCESS, instance }; }
  function failure(error, instanceId) { return { type: APPS_CLOSE_APP_FAILURE, error, instanceId }; }

}