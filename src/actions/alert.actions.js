export const alertActions = {
  success,
  error,
  clear
};

export const ALERT_SHOW_SUCCESS_MESSAGE = 'ALERT_SHOW_SUCCESS_MESSAGE';
function success(message) {
  return { type: ALERT_SHOW_SUCCESS_MESSAGE, message };
}

export const ALERT_SHOW_ERROR_MESSAGE = 'ALERT_SHOW_ERROR_MESSAGE';
function error(message) {
  return { type: ALERT_SHOW_ERROR_MESSAGE, message };
}

export const ALERT_CLEAR = 'ALERT_CLEAR';
function clear() {
  return { type: ALERT_CLEAR };
}
