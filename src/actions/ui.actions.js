import { uiConstants } from '../constants';

export const uiActions = {
  toggleDrawer,
  changeServersIndexTab
};

function toggleDrawer(open) {
  return { type: uiConstants.TOGGLE_DRAWER, open };
}

function changeServersIndexTab(tab) {
  return { type: uiConstants.CHANGE_SERVERS_INDEX_TAB, tab };
}
