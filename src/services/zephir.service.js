import { callZephir } from '../helpers/backend';

const api = {
  'v1': {
    identity: [
      {
        'session-user': [
          'get'
        ],
        'settings': [
          'set',
          'get'
        ]
      }
    ],
    server: [
      {
        'peering-conf': [
          'get'
        ],
        'exec': [
          'list',
          'describe',
          'deploy',
          'command'
        ]
      },
      'list',
      'describe',
      'create',
      'update',
      'delete',
      {
        'serverselection': [
        'list'
        ]
      }
    ],
    servermodel: [
      'list',
      'describe',
      'create'
    ],
    serverselection: [
      'list',
      'describe',
      'create',
      'delete',
      'update',
      {
        'server': [
          'add',
          'remove',
          'set'
        ]
      },
      {
        'user': [
          'add',
          'remove',
          'update',
        ]
      }
    ],
    config: {
      session: {
        server: [
          'start',
          'stop',
          'list'
        ],
        servermodel: [
          'start',
          'stop',
          'list'
        ]
      }
    },
    execution: {
      salt: [
        'exec',
        {
          configuration: [
            'deploy'
          ],
          job: [
            'list',
            'describe'
          ]
        }
      ]
    }
  }
};

export const Zephir = createEndpointsProxy(api, '');

function createEndpointsProxy(tree, rootPath) {
  let trunk = {};
  if (Array.isArray(tree)) {
    tree.forEach(item => {
      if (item instanceof Object) {
        const branch = createEndpointsProxy(item, rootPath);
        trunk = {
          ...trunk,
          ...branch
        };
      } else if (typeof item === 'string') {
        trunk[camelize(item)] = createCallZephirProxy(`${rootPath ? (rootPath + '.') : ''}${item}`);
      }
    });
  } else if (tree instanceof Object){
    Object.keys(tree).forEach(key => {
      const branch = tree[key];
      trunk[camelize(key)] = createEndpointsProxy(branch, `${rootPath ? (rootPath + '.') : ''}${key}`);
    });
  } else {
    throw new Error('tree must be an array or an object');
  }
  return trunk;
}

function capitalize(word) {
  return `${word.slice(0, 1).toUpperCase()}${word.slice(1).toLowerCase()}`;
}

function camelize(text, separator = '-') {
  const words = text.split(separator);
  return ([words[0], ...words.slice(1).map((word) => capitalize(word))]).join('');
}

function createCallZephirProxy(uri) {
  const proxy = function(payload) {
    return callZephir(uri, {
      body: JSON.stringify(payload || {})
    });
  };
  proxy.raw = callZephir.bind(null, uri);
  return proxy;
}
