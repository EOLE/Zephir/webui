import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { store, theme/*, registerServiceWorker*/ } from './helpers';
import { App } from './App';
import 'typeface-roboto';
import './index.css';
import Keycloak from 'keycloak-js';

if (process.env.NODE_ENV === 'development') {
  // Les dépendances uniquement utilisées en développement devrait
  // être incluses dans ce bloc pour éviter qu'elles soient intégrées
  // aux versions de production
} else { // Environnement de PRODUCTION
  // setup production asset cache
  // TODO: re enable service worker and make it work behind the authentication
  // firewall
  // registerServiceWorker();
  // TEMP: Disable registered service workers
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.getRegistrations().then(function(registrations) {
      for(let registration of registrations) {
        registration.unregister();
      }
    });
  }
}

const keycloak = Keycloak('../../../keycloak/keycloak.json');
const token = sessionStorage.getItem('kc_token');
const refreshToken = sessionStorage.getItem('kc_refreshToken');

const updateLocalStorage = () => {
  sessionStorage.setItem('kc_token', keycloak.token);
  sessionStorage.setItem('kc_refreshToken', keycloak.refreshToken);
};

const app = (
  <MuiThemeProvider theme={theme}>
    <Provider store={store}>
      <App />
    </Provider>
  </MuiThemeProvider>
);

keycloak.init({onLoad: 'login-required', token, refreshToken})
  .success(authenticated => {
    if (authenticated) {
      keycloak.updateToken(10).error(() => keycloak.logout());
      updateLocalStorage();

      if(sessionStorage.getItem('kc_token') != null){
        ReactDOM.render(app, document.getElementById('root'));
      }
      setInterval(() => {
        keycloak.updateToken(10).error(() => keycloak.logout());
        sessionStorage.setItem('kctoken', keycloak.token);
      }, 10000);
    } else {
      keycloak.login();
    }


  });
