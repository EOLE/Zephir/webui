import React from 'react';
import { Page, VSplit } from '../../components';

export const WidgetsPage = () => (
  <Page title="Widgets" backLink="/">
    <VSplit
      left={
        <Page title="Widgets" inner={true} />
      }
      right={
        <Page title="Preview" inner={true} />
      }
    />
  </Page>
);
