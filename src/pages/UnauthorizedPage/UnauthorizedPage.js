import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {
  Card,
  CardContent,
  CardActions,
  CardHeader,
  Grid,
  Typography,
  Button
} from '@material-ui/core';
import bg from '../../assets/login-bg.jpg';
import { connect } from 'react-redux';
import {
  Refresh as RefreshIcon,
  Warning as WarningIcon,
} from '@material-ui/icons';
import { Redirect } from 'react-router';

class UnauthorizedPage extends React.Component {

  constructor(props) {
    super(props);
    this.handlePageReload = this.handlePageReload.bind(this);
  }

  render() {

    const { classes, isAuthenticated } = this.props;

    if (isAuthenticated) return <Redirect to="/" />;

    return (
      <div className={classes.root}>
        <Grid className={classes.container} container alignItems="center" justify="center" direction="row">
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <Card elevation={4} className={classes.paper}>
              <CardHeader avatar={<WarningIcon />} title={
                <Typography variant="h5" component="h4" gutterBottom>
                  Non authentifié
                </Typography>
              } />
              <CardContent>
                <Typography component="p">
                  Nous n&#39;avons pas pu vous identifier. Essayez de rafraîchir la page ou contactez votre administrateur.
                </Typography>
              </CardContent>
              <CardActions>
                <Button onClick={this.handlePageReload} size="small" color="primary" style={{marginLeft: 'auto'}}>
                  <RefreshIcon className={classes.leftIcon} />
                  Recharger la page
                </Button>
              </CardActions>
            </Card>
          </Grid>
        </Grid>
      </div>
    );
  }

  handlePageReload() {
    window.location.replace(window.location.origin);

  }

}

const styles = theme => ({
  root: {
    height: '100%',
    backgroundColor: theme.palette.primary.light,
    backgroundImage: `linear-gradient(rgba(255, 255, 255, 0.45), rgba(200, 150, 255, 0.45)), url(${bg})`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
    backgroundSize: 'cover'
  },
  container: {
    height: '100%',
    width: '100%',
    margin: 0,
    padding: 0,
  },
  card: theme.mixins.gutters({
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    paddingLeft: theme.spacing.unit * 3,
    paddingRight: theme.spacing.unit * 3,

  }),
  leftIcon: {
    marginRight: theme.spacing.unit
  }
});

UnauthorizedPage.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
  return {
    isAuthenticated: !!state.auth.user
  };
};

const styledUnauthorizedPage = withStyles(styles)(UnauthorizedPage);
const connectedUnauthorizedPage = connect(mapStateToProps)(styledUnauthorizedPage);
export { connectedUnauthorizedPage as UnauthorizedPage };
