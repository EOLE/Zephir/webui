import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {
  Card,
  CardContent,
  CardActions,
  CardHeader,
  Grid,
  Typography,
  Button
} from '@material-ui/core';
import bg from '../../assets/login-bg.jpg';
import {
  Home as HomeIcon,
  CloudOff as CloudOffIcon,
} from '@material-ui/icons';

class LogoutPage extends React.Component {
  constructor(props) {
    super(...props);
  }

  render() {

    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Grid className={classes.container} container alignItems="center" justify="center" direction="row">
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <Card elevation={4} className={classes.paper}>
              <CardHeader avatar={<CloudOffIcon />} title={
                <Typography variant="h5" component="h4" gutterBottom>
                  Vous avez été déconnecté
                </Typography>
              } />
              <CardContent>
                <Typography component="p">
                  Vous avez bien été déconnecté. Vous pouvez maintenant quitter cette page ou cliquer sur le bouton ci-dessous pour revenir à l&#39;accueil.
                </Typography>
              </CardContent>
              <CardActions>
                <Button onClick={this.handlePageGoHome} size="small" color="primary" style={{marginLeft: 'auto'}}>
                  <HomeIcon className={classes.leftIcon} />
                    Revenir à l&#39;accueil
                </Button>
              </CardActions>
            </Card>
          </Grid>
        </Grid>
      </div>
    );
  }

  componentDidMount() {
    sessionStorage.removeItem('kc_token', null)
    sessionStorage.removeItem('kc_refreshToken', null)

    //const href = window.location.href;
    const href = 'http://zephir2.ac-test.fr:8888/auth/realms/zephir/protocol/openid-connect/logout';
    fetch(href, {
      method: 'GET',
      mode: 'no-cors'
    }).then(function(response) {
      return response;
    }).then(() => window.location.replace(window.location.origin+"/unauthorized"));

  }

  handlePageGoHome() {
    window.location.replace(window.location.origin+"?nocache=" + (new Date()).getTime());
  }

}

const styles = theme => ({
  root: {
    height: '100%',
    backgroundColor: theme.palette.primary.light,
    backgroundImage: `linear-gradient(rgba(255, 255, 255, 0.45), rgba(200, 150, 255, 0.45)), url(${bg})`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
    backgroundSize: 'cover'
  },
  container: {
    height: '100%',
    width: '100%',
    margin: 0,
    padding: 0,
  },
  card: theme.mixins.gutters({
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    paddingLeft: theme.spacing.unit * 3,
    paddingRight: theme.spacing.unit * 3,

  }),
  leftIcon: {
    marginRight: theme.spacing.unit
  }
});

LogoutPage.propTypes = {
  classes: PropTypes.object.isRequired,
};

const styledLogoutPage = withStyles(styles)(LogoutPage);

export { styledLogoutPage as LogoutPage };
