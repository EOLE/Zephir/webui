import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Route } from 'react-router';
import {
  Tabs,
  Tab
} from '@material-ui/core';

import { Page, SlidingSwitch } from '../../components';
import { WidgetsPage } from '../';
import { ServersList} from '../ServersPage';
import { ServerModelsList} from '../ServerModelsPage';
import { ServerSelectionsList} from '../ServerSelectionsPage';
class HomePage extends React.Component {

  state = {
    activeTab: 0,
  };

  handleTabChange = (event, activeTab) => {
    this.setState({ activeTab });
  };

  render() {
    return (
      <SlidingSwitch>
        <Route exact path="/">
          {/* <Page title="Tableau de bord" navRight={
          <div>
            <IconButton
              component={Link}
              to="/notifications"
              color="inherit"
            >
              <Badge badgeContent={4} color="secondary">
                <NotificationsIcon />
              </Badge>
            </IconButton>
            <ResponsiveButton
              component={Link}
              to="/widgets"
              title="Widgets"
              icon={WidgetsIcon}
              color="inherit"
            />
            <ResponsiveButton
              component={Link}
              to="/logout"
              icon={WidgetsIcon}
              color="inherit"
            />
          </div>
        }>
            */}
          <Page title="Tableau de bord" >
            <Tabs
              value={this.state.activeTab}
              onChange={this.handleTabChange}
              indicatorColor="primary"
              textColor="primary"
              centered
            >
              {/* <Tab label="Mon interface" /> */}
              <Tab label="Serveurs" />
              <Tab label="Sélections" />
              <Tab label="Modèles" />
            </Tabs>

            {/* {this.state.activeTab === 0 && <div>Mon Interface</div>} */}
            {this.state.activeTab === 0 && <ServersList/>}
            {this.state.activeTab === 1 && <ServerSelectionsList/>}
            {this.state.activeTab === 2 && <ServerModelsList/>}

          </Page>
        </Route>
        <Route path="/widgets" component={WidgetsPage} />
      </SlidingSwitch>
    );
  }
}

const styles = (theme) => ({
  root: {
    height: '100%',
    width: '100%',
    padding: theme.spacing.unit * 2
  }
});

function mapStateToProps(state) {
  const { auth } = state;
  const { user } = auth;
  return {
    user
  };
}

const connectedHomePage = connect(mapStateToProps)(HomePage);
const styledHomePage = withStyles(styles)(connectedHomePage);

export { styledHomePage as HomePage };
