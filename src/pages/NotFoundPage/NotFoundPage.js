import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Card, CardContent, CardActions, Button, Grid, Typography } from '@material-ui/core';
import { history } from '../../helpers';
import bg from '../../assets/login-bg.jpg';
import {
  ArrowBack as ArrowBackIcon,
  Home as HomeIcon
} from '@material-ui/icons';


class NotFoundPage extends React.Component {

  handleHistoryBackClick() {
    history.goBack();
  }

  render() {

    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Grid className={classes.container} container alignItems="center" justify="center" direction="row">
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <Card elevation={4} className={classes.paper}>
              <CardContent>
                <Typography variant="h5" component="h2" gutterBottom>
                  Page non trouvée :(
                </Typography>
                <Typography component="p">
                  Peut être que la page que vous cherchez a été déplacée. L&#39;adresse de la page est elle correcte ?
                </Typography>
              </CardContent>
              <CardActions>
                <Button onClick={this.handleHistoryBackClick} size="small" color="primary">
                  <ArrowBackIcon className={classes.leftIcon} />
                  Page précédente
                </Button>
                <Button component={Link} to="/" size="small" color="primary" style={{marginLeft: 'auto'}}>
                  <HomeIcon className={classes.leftIcon} />
                  Retour à l&#39;accueil
                </Button>
              </CardActions>
            </Card>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const styles = theme => ({
  root: {
    height: '100%',
    backgroundColor: theme.palette.primary.light,
    backgroundImage: `linear-gradient(rgba(255, 255, 255, 0.45), rgba(200, 150, 255, 0.45)), url(${bg})`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
    backgroundSize: 'cover'
  },
  container: {
    height: '100%',
    width: '100%',
    margin: 0,
    padding: 0,
  },
  card: theme.mixins.gutters({
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    paddingLeft: theme.spacing.unit * 3,
    paddingRight: theme.spacing.unit * 3,

  }),
  leftIcon: {
    marginRight: theme.spacing.unit
  }
});

NotFoundPage.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapStateToProps() {
  return {
  };
}

const styledNotFoundPage = withStyles(styles)(NotFoundPage);
const connectedNotFoundPage = connect(mapStateToProps)(styledNotFoundPage);
export { connectedNotFoundPage as NotFoundPage };
