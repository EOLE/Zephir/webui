import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router';
import { settingsActions } from '../../actions';
import { withForm } from '../../components';
import {
  ResponsiveButton,
} from '../../components';
import {
  Grid,
  TextField,
  CircularProgress,
  Checkbox,
  FormControlLabel
} from '@material-ui/core';
import {
  Check as CheckIcon
} from '@material-ui/icons';


class PreferencesDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      isSaving: false,
    };
  }
  componentDidMount() {
    const { user } = this.props;
    this.setState(() => {
      this.props.dispatch(settingsActions.getSetting(user.username));
    });
  }

  componentDidUpdate() {
    const { form } = this.props;
    if (form.submitted && form.validated)  this.saveSettings();
  }

  saveSettings() {
    this.setState({ isSaving: true }, () => {
      const { form, user } = this.props;
      const data_settings = form.data;

      let savePromise;
      if (form.data) { // Modification d'un serveur existant
        savePromise = this.props.dispatch(settingsActions.setSetting(user, data_settings));
        this.setState({isSaving: false});
      }
      savePromise.catch(err => {
        form.reset();
        throw err;
      });
    });
  }

  render() {
    const { settings, form, classes, sm, inner, theme} = this.props;
    const { isLoading, isSaving} = this.state;
    const { sshkey, lastname, firstname, email, alertes } = settings ||
    {
      sshkey: '',
      lastname: '',
      firstname: '',
      email: '',
      alertes: 'false'
    };
    return((!isLoading ) && settings &&
        <div className={classes.content}>
          <form name="form" onSubmit={this.handleSubmit}>
            <Grid container style={styles.container} >
              <div style={styles.div}>
                <Grid style={styles.grid}>
                  <div style={styles.text}>
                    <TextField
                      multiline rows='5'
                      name="sshkey"
                      label="Clé SSH Publique"
                      value={ form.field('sshkey', sshkey) || sshkey }
                      onChange={ form.onChange }
                      style={styles.textarea}
                      placeholder="clé publique "
                    />

                    <TextField
                      id="nom"
                      label="Nom"
                      className={classes.textField}
                      name="lastname"
                      disabled={isLoading || isSaving}
                      value={ form.field('lastname', lastname) || lastname }
                      onChange={ form.onChange }
                    />
                    <TextField
                      id="prenom"
                      label="Prenom"
                      className={classes.textField}
                      disabled={isLoading || isSaving}
                      name="firstname"
                      value={ form.field('firstname', firstname) || firstname }
                      onChange={ form.onChange }
                    />
                    <TextField
                      id="email"
                      label="email"
                      className={classes.textField}
                      disabled={isLoading || isSaving}
                      name="email"
                      value={ form.field('email', email) || email }
                      onChange={ form.onChange }
                    />
                    <FormControlLabel
                      labelPlacement='end'
                      label='Alertes email'
                      control={
                        <Checkbox
                          checked={form.field('alertes', alertes)}
                          id="alertes"
                          disabled={isLoading || isSaving}
                          name="alertes"
                          value={ (form.field('alertes', alertes) && form.field('alertes', alertes).toString()) || alertes }
                          onChange={ form.onChange }
                        />
                      }
                    />

                  </div>
                </Grid>
                <ResponsiveButton
                  sm={sm}
                  onClick={form.onSubmit}
                  style={{padding: '20px 0px', margin: '20px 0px'}}
                  title='Enregistrer'
                  color="primary"
                  disabled={isLoading || isSaving}
                  icon={form.submitted && isLoading
                    ? (props =>
                      <CircularProgress
                        {...props}
                        color={inner ? 'primary' : 'inherit'}
                        style={{ marginBottom: inner ? 0 : theme.spacing.unit }}
                        size={20}
                      />
                    )
                    : CheckIcon
                  }
                />
              </div>
            </Grid>
          </form>
        </div>
    );
  }
}

const styles = {
  container: {
    display: "flex",
    flexDirection: "row",
  },
  div: {
    marginLeft: "15%",
    marginRight: "15%",
    paddingTop: 20,
    paddingBottom: 20,
    borderBottom: "solid 1px lightGrey"
  },
  paper: {
    width: 100,
    height: 70,
  },
  text: {
    display: "flex",
    flexDirection: "column",
  },
  grid: {
    display: "flex",
    flexDirection: "row"
  },
  textarea: {
    maxWidth: 500,
    minWidth: 500,
    minHeight: 100,
    marginTop: 20,
    marginBottom: 20
  },

};

const mapStateToProps = ({ settings, auth }) => {
  const { user } = auth;
  return {
    settings: settings.settings,
    user,
  };
};


const managedFormPreferencesPage = withForm({
  sshkey: () => {},
  firstname: () => {},
  lastname: () => {},
  email: () => {},
  alertes: () => {},
})(PreferencesDetail);

const connectedPreferencesPage = connect(mapStateToProps)(managedFormPreferencesPage);
const styledPreferencesPage = withStyles(styles, { withTheme: true })(connectedPreferencesPage);
const routedPreferencespage = withRouter(styledPreferencesPage);

export { routedPreferencespage as PreferencesDetail };
