import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router';
import { settingsActions } from '../../actions';
import {
  PreferencesDetail
} from './';
import {
  Page
} from '../../components';

class PreferencesPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      isSaving: false,
    };
  }
  componentDidMount() {
    const { user } = this.props;
    this.setState(() => {
      this.props.dispatch(settingsActions.getSetting(user.username))
    })
  }

  render() {

    const { style, inner, settings, classes } = this.props;
    const { isLoading} = this.state;

    return((!isLoading ) &&
      <Page
        title="Paramètres"
        className={classes.root}
        style={style}
        inner={inner}
      >
        <div className={classes.content}>
          {settings && <PreferencesDetail settings={settings} />}
        </div>
      </Page>
    );
  }
}


const mapStateToProps = ({ settings, auth }) => {
  const { user } = auth;
  return {
    settings: settings.settings,
    user,
  };
};


const connectedPreferencesPage = connect(mapStateToProps)(PreferencesPage);
const styledPreferencesPage = withStyles( { withTheme: true })(connectedPreferencesPage);
const routedPreferencespage = withRouter(styledPreferencesPage);

export { routedPreferencespage as PreferencesPage };
