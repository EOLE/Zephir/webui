import React from 'react';
import { Route, withRouter } from 'react-router';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import {
  Assessment as AssessmentIcon
} from '@material-ui/icons';

import {
  Page,
  FadingSwitch,
  VSplit,
  RouteDataLoader,
  Redirect,
} from '../../components';

import { Button } from '@material-ui/core';

import {
  ServerModelsIndex,
  ServerModelsDashboard,
  ServerModelDetail
} from './';

import { servermodelActions } from '../../actions';

class ServerModelsPage extends React.Component {

  constructor(props){
    super(...props);
    this.state = {
      serverModelsListExpanded: false
    };
    this.loadRouteServerModel = this.loadRouteServerModel.bind(this);
    this.handleServerModelListExpandClick = this.handleServerModelListExpandClick.bind(this);
    this.handleFilterChange = this.handleFilterChange.bind(this);
  }

  render() {
    const { servermodels, classes } = this.props;
    const { serverModelsListExpanded } = this.state;
    return (
          <Page
            className={classes.page}
            title='Gestion des Modèles de serveur'
            loading={servermodels.itemsLoading}
            navRight={
              <div>
                <Button component={Link} to={`/servermodels/dashboard`} color="inherit">
                  <AssessmentIcon className={classes.leftIcon} />
                  Vue d&#39;ensemble
                </Button>
                {/*
                  <Button component={Link} to={`/servermodels/new`} color="inherit">
                  <AddIcon className={classes.leftIcon} />
                  Nouveau
                </Button>
              */}
              </div>
            }
          >
            <VSplit
              left={
                <Route path="/servermodels/:id?" render={props => this.renderServerModelIndex(props) } />
              }
              right={
                <FadingSwitch>
                  <Route path="/servermodels/dashboard" render={() => <ServerModelsDashboard inner={true} />} />
                  {/*
                    <Route path="/servermodels/new" render={() => <ServerModelForm inner={true} />} />
                  */}
                  <Route path="/servermodels/:id" render={(routeProps) => {
                    return (

                      <RouteDataLoader loadData={this.loadRouteServerModel} {...routeProps}>
                        <ServerModelDetail inner={true} sm={serverModelsListExpanded} />
                      </RouteDataLoader>
                    );
                  }} />
                  <Redirect from="/servermodels" to="/servermodels/dashboard" />
                </FadingSwitch>
              }
            />
          </Page>
    );
  }

  loadRouteServerModel(match, dispatch) {
    const servermodelId = parseInt(match.params.id, 10);
    return dispatch(servermodelActions.describe(servermodelId));
  }

  handleServerModelListExpandClick(expanded) {
    this.setState({
      servermodelsListExpanded: expanded
    });
  }
  handleFilterChange(filter) {
    this.props.dispatch(servermodelActions.updateLocalFilter(filter));
  }

  renderServerModelIndex(routeProps) {
    const { match } = routeProps;
    return (
      <ServerModelsIndex
        selectedServerModel={match.params.id}
        onFilterChange={this.handleFilterChange}
        onExpand={this.handleServerModelListExpandClick}
      />
    );
  }
}

const styles = (/*theme*/) => ({
  page: {},
});

function mapStateToProps(state) {
  const { servermodels } = state;
  return {
    servermodels
  };
}

const connectedServerModelPage = connect(mapStateToProps)(ServerModelsPage);
const styledServerModelPage = withStyles(styles, { withTheme: true })(connectedServerModelPage);
const routedServerModelPage = withRouter(styledServerModelPage);
export { routedServerModelPage as ServerModelsPage };
