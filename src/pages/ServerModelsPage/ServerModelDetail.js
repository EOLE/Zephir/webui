import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { withStyles } from '@material-ui/core/styles';
import {
  selectServermodelById,
} from '../../selectors';

import {
  List,
  ListItem,
  ListItemText,
  Typography,
  GridList,
  GridListTile,
} from '@material-ui/core';
import {
  Settings as SettingsIcon,
} from '@material-ui/icons';
import { appActions } from '../../actions';

import {
  Page,
  VSplit,
} from '../../components';

class ServerModelDetail extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
    };
    this.onAppClick = this.onAppClick.bind(this);
  }

  render(){
    const { classes, inner, servermodel, auth } = this.props;
    const { isLoading } = this.state;
    return (
      <Page
        title={servermodel && servermodel.model.servermodelname}
        loading={isLoading}
        className={classes.root}
        inner={inner}
        backLink={`/servermodels`}
      >
        {servermodel && !isLoading &&
          <div style={{margin: "0 10px"}}>
            <GridList cellHeight={'auto'} className={classes.gridList}>
              <VSplit
                left={
                  <GridListTile>
                    <List dense className={classes.card}>
                      <Typography variant="subtitle1">
                        Informations Générales
                      </Typography>
                      <ListItem divider >
                        <ListItemText primary="Nom" secondary={servermodel.model.servermodelname} />
                      </ListItem>
                      <ListItem divider >
                        <ListItemText primary="Description" secondary={servermodel.model.servermodeldescription} />
                      </ListItem>
                      { auth.user && auth.user.profil === "root" &&
                        <ListItem divider
                          button
                          onClick={this.onAppClick}
                        >
                          <ListItemText primary="Configuration" secondary="Editer la configuration du modèle de serveur" />
                          <SettingsIcon />
                        </ListItem>
                      }
                    </List>

                  </GridListTile>
                }
                right={
                  <GridListTile>
                    <List>
                      <Typography variant="subtitle1">
                        Description des services
                      </Typography>
                      {this.showServices(servermodel)}
                    </List>
                  </GridListTile>
                }
              />
            </GridList>
          </div>
        }
      </Page>
    );
  }

  onAppClick() {
    const { servermodel } = this.props;

    this.props.dispatch(appActions.openApp('ConfigEditor', {servermodel: servermodel.model}))
      .then(instance => this.props.history.push(`/apps/${instance.id}`));
  }

  showServices = (servermodel) => {
    const service = servermodel.services.map(el =>
      <ListItem  key={el.id} divider>
        <ListItemText
          primary={el.name}
          secondary={el.description}
        />
      </ListItem>
    );
    return service;
  };

}

const styles = theme => ({
  root: {
    height: '100%',
    //zIndex: 3
  },
  container: {
    height: 'auto',

  },
  col: {
    backgroundColor: theme.palette.background.paper,
    maxHeight: '100%',
    overflowY: 'auto'
  },
  listSubheader: {
    backgroundColor: theme.palette.background.paper
  }
});


function mapStateToProps({ servermodels, auth }, { match }) {
  const servermodelId = parseInt(match.params.id, 10);
  const servermodel = selectServermodelById(servermodels.describeById, servermodelId);
  return {
    servermodel,
    auth
  };
}

const connectedServerModelDetail = connect(mapStateToProps)(ServerModelDetail);
const styledServerModelDetail = withStyles(styles, { withTheme: true })(connectedServerModelDetail);
const routedServerModelDetail = withRouter(styledServerModelDetail);

export { routedServerModelDetail as ServerModelDetail };
