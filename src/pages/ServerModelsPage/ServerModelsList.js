import React from 'react';
import { connect } from 'react-redux';
import Infinite from 'react-infinite';
import { withStyles } from '@material-ui/core/styles';
import { ServerModelsListItem } from './';
import { selectAllServerModels, selectServerModelsWithFilter } from '../../selectors';
import { servermodelActions } from '../../actions';
import ReactDOM from 'react-dom';
import {
  Loader
} from '../../components';
import {
  List,
} from '@material-ui/core';

const ITEMS_PER_PAGE = 10;

class ServerModelsList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      // si loadMoreServers est réactivé, mettre hasMore: true
      hasMore: false,
      listHeight: 1,
      isLoading: false,
    };
    this.loadMoreServers = this.loadMoreServers.bind(this);
    this.updateListHeight = this.updateListHeight.bind(this);
  }

  loadMoreServers(){

    const { hasMore, isLoading } = this.state;
    if (!hasMore || isLoading ) return;
        this.setState(prevState => {
      const page = prevState.page + 1;
      this.props.dispatch(servermodelActions.list({page, itemsPerPage: ITEMS_PER_PAGE}))
        .then(() =>this.setState({ isLoading: false }));
      return { page, isLoading: true };
    });
  }

  componentDidMount() {
    this.setState( () =>{
      this.props.dispatch(servermodelActions.list());
    });
    window.addEventListener('resize', this.updateListHeight);
    this.loadMoreServers();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateListHeight);
  }

  render() {

    const { classes, servermodels } = this.props;
    return (
      <List className={classes.root}
        ref={el => this.listElement = el}>
        <Infinite
          containerHeight={this.state.listHeight}
          loadingSpinnerDelegate={this.renderLoader()}
          onInfiniteLoad={this.loadMoreServers}
          infiniteLoadBeginEdgeOffset={this.state.listHeight-64}
          elementHeight={64}>
          {servermodels.map((servermodel) =>
            <ServerModelsListItem key={servermodel.model.servermodelid} servermodels={servermodel} />
          )}
        </Infinite>
      </List>
    );
  }
  renderLoader() {
    return (
      this.state.hasMore
        ? <Loader />
        : null
    );
  }

  componentDidUpdate() {
    this.updateListHeight();
  }

  updateListHeight() {
    const el = ReactDOM.findDOMNode(this.listElement); // eslint-disable-line
    const listHeight = el && el.clientHeight ? el.clientHeight-1 : 1;
    if (listHeight === this.state.listHeight) return;
    this.setState({listHeight});
  }

}
const styles = () => ({
  root: {
    height: '100%',
    width: '100%',
    position: 'relative',
    paddingTop: 0,
    paddingBottom: 0,
  },
  noServer: {
    paddingTop: '50px'
  }
});

const mapStateToProps = state => {
  const { servermodels } = state;
  return {
    servermodels: servermodels.filter !== '' ? selectServerModelsWithFilter(servermodels.byId, servermodels.filter) : selectAllServerModels(servermodels.byId)
  };
};

const connectedServerModelsList = connect(mapStateToProps)(ServerModelsList);
const styledServerModelsList = withStyles(styles)(connectedServerModelsList);
export { styledServerModelsList as ServerModelsList };
