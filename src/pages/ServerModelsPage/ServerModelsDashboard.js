import React from 'react';
import { Page } from '../../components';


export class ServerModelsDashboard extends React.Component {
  render() {

    const { inner } = this.props;

    return(
      <Page
        title="Vue d'ensemble"
        inner={inner}
      >
      </Page>
    );
  }
}
