import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router';
import { Route } from 'react-router';
import { NavLink } from 'react-router-dom';

import {
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  Card,
  CardContent,
  Typography,
  ButtonBase
} from '@material-ui/core';
import {
  ExpandMore as ExpandMoreIcon,
  Description as ServerModelIcon
} from '@material-ui/icons';

import styles from './ServerModelsListItem.styles.js';

class ServerModelsListItem extends React.Component {

  state = {
    expanded: false
  };

  handleExpandClick() {
    this.setState({ expanded: !this.state.expanded });
  }

  render() {
    const { classes, className, servermodels } = this.props;
    const { expanded } = this.state;
    const ServerModelsListItem_Card = () => {
      return (
        <Card className={classes.card}>
          <ButtonBase className={classes.content}
            key={`servermodelListItem-${servermodels.model.servermodelid}`}
            component={NavLink}
            to={`/servermodels/${servermodels.model.servermodelid}`}>
            <CardContent style={{textAlign: "center"}}>
              <Typography variant="h5">{servermodels.model.servermodelname}</Typography>
              <Typography variant="subtitle1" color="textSecondary">
                {servermodels.model.subreleasename}
              </Typography>
            </CardContent>
          </ButtonBase>
        </Card>
      );
    };
    const ServerModelsListItem_List = () => {
      return (
        <div className={classNames(classes.root, className)}>
          <ListItem
            key={`servermodelListItem-${servermodels.model.servermodelid}`}
            component={NavLink}
            to={`/servermodels/${servermodels.model.servermodelid}`}
            activeClassName={classes.activeLink}>
            <ServerModelIcon />
            <ListItemText
              key={`servermodelListItemText-${servermodels.model.servermodelid}`}
              inset
              classes={{ primary: classes.primary, secondary: classes.secondary}}
              primary={servermodels.model.servermodelname}
              secondary={servermodels.model.subreleasename}
            />
            {servermodels.model.children && !!servermodels.model.children.length &&
            <ListItemSecondaryAction className={classes.secondaryAction}>
              <IconButton
                className={classes.button}
                onClick={this.handleExpandClick.bind(this)}
              >
                <ExpandMoreIcon
                  className={classNames(classes.expand, {
                    [classes.expandOpen]: expanded,
                  })}
                />
              </IconButton>
            </ListItemSecondaryAction>
            }

          </ListItem>
        </div>
      );
    };
    return (
      <div style={{margin: '0 auto', maxWidth: '1270px'}}>
        <Route exact path="/" component={ServerModelsListItem_Card}/>
        <Route path="/servermodels/" component={ServerModelsListItem_List}/>
      </div>
    );
  }
}


ServerModelsListItem.propTypes = {
  classes: PropTypes.object.isRequired,
};

const routedServerModelsListItem = withRouter(ServerModelsListItem);
const styledServerModelsListItem = withStyles(styles)(routedServerModelsListItem);
export { styledServerModelsListItem as ServerModelsListItem };
