import React from 'react';
import { Page, ResponsiveButton } from '../../components';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import {
  Grid,
  TextField,
  CircularProgress
} from '@material-ui/core';

import {
  Check as CheckIcon
} from '@material-ui/icons';

import {
  servermodelActions
} from '../../actions';

class ServerModelForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      submitted: false,
      serverModel: null,
      formData: {
        servermodelname: "",
        servermodelversion: "",
        servermodelparent: ""
      },
      isLoading: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  render() {

    const { inner, theme, style, sm } = this.props;
    const { submitted, formData, isLoading } = this.state;
    const { servermodelname, servermodelversion, servermodelparent,  } = formData;

    return(
      (!isLoading || submitted) &&
      <Page
        title="Nouveau Modèle de serveur"
        inner={inner}
        style={style}
        navRight={
          <ResponsiveButton
            sm={sm}
            onClick={this.handleSubmit}
            title='Enregistrer'
            disabled={isLoading}
            icon={submitted && isLoading
              ? (props =>
                <CircularProgress
                  {...props}
                  color={inner ? 'primary' : 'inherit'}
                  style={{ marginBottom: inner ? 0 : theme.spacing.unit }}
                  size={20}
                />
              )
              : CheckIcon
            }
          />
        }
      >
        <div>
          <form name="form" onSubmit={this.handleSubmit}>
            <Grid container spacing={theme.spacing.unit * 2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  name="servermodelname"
                  label="nom du modèle"
                  value={servermodelname}
                  onChange={this.handleChange}
                  error={submitted && !servermodelname}
                  helperText={submitted && !servermodelname && "Le nom du modèle est obligatoire."}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  name="servermodelversion"
                  label="version"
                  value={servermodelversion}
                  onChange={this.handleChange}
                  error={submitted && !servermodelversion}
                  helperText={submitted && !servermodelversion && "La version du modèle est obligatoire."}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  name="servermodelparent"
                  label="modèle parent"
                  value={servermodelparent}
                  onChange={this.handleChange}
                  error={submitted && !servermodelparent}
                  helperText={submitted && !servermodelparent && "Le parent du modèle est obligatoire."}
                  fullWidth
                />
              </Grid>
            </Grid>
            <input type="submit" style={{display: 'none'}}/>
          </form>
        </div>
      </Page>
    );
  }

  componentDidMount() {

    if (!this.props.match.params.id) return;

    const routeServerModelId = parseInt(this.props.match.params.id, 10);
    if (!this.state.serverModel || this.state.serverModel.id !== routeServerModelId) {
      this.loadServerModel(this.props.match.params.id);
    }

  }

  handleSubmit(e) {
    e.preventDefault();
    this.setState({ submitted: true });
    if (!this.isFormValid()) return;
    this.saveServer();
  }

  isFormValid() {
    const { serverModel, formData } = this.state;
    return formData.servermodelname
      && formData.servermodelversion
      && formData.servermodelparent
      && (serverModel);
  }

  handleChange(e) {
    const { name, value } = e.target;
    const { formData } = this.state;
    this.setState({
      formData: {
        ...formData, [name]: value
      }
    });
  }

  loadServerModel(servermodelId) {
    this.setState({isLoading: true});
    this.props.dispatch(servermodelActions.describe(servermodelId))
      .then(serverModel => {
        this.setState({serverModel, formData: {...serverModel}, isLoading: false});
      })
      .catch(() => {
        this.setState({serverModel: null, isLoading: false});
      });
  }

  // saveServerModel() {}

}

const styles = theme => ({
  root: {
    width: '100%',
    height: '100%'
  },
  content: {
    padding: theme.spacing.unit * 2
  },
  formControl: {
    '& > div': {
      width: '100%'
    }
  },
  textField: {
  },
  passwordField: {
    //lineHeight: 3,
    height: 'auto'
  }
});

const connectedServerModelForm = connect()(ServerModelForm);
const styledServerModelForm = withStyles(styles, { withTheme: true })(connectedServerModelForm);
const routedServerModelForm = withRouter(styledServerModelForm);
export { routedServerModelForm as ServerModelForm };
