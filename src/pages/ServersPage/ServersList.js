import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Infinite from 'react-infinite';
import ReactDOM from 'react-dom';
import {
  List,
  Typography,
  Button,
} from '@material-ui/core';
import {
  Loader
} from '../../components';
import {
  Add as AddIcon,
} from '@material-ui/icons';
import { serverActions } from '../../actions';
import { selectServersWithFilter, selectAllServers } from '../../selectors';
import { Link } from 'react-router-dom';
import {
  ServersListItem
} from './';

const ITEMS_PER_PAGE = 10;

class ServersList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      hasMore: true,
      listHeight: 1,
      isLoading: false,
    };
    this.loadMoreServers = this.loadMoreServers.bind(this);
    this.updateListHeight = this.updateListHeight.bind(this);
  }

  loadMoreServers(){

    const { hasMore, isLoading } = this.state;
    if (!hasMore || isLoading ) return;

    this.setState(prevState => {
      const page = prevState.page + 1;
      this.props.dispatch(serverActions.list({page, itemsPerPage: ITEMS_PER_PAGE}))
        .then(() =>this.setState({ isLoading: false }));
      return { page, isLoading: true };
    });
  }

  render() {
    const { classes, servers } = this.props;
    const { isLoading, hasMore } = this.state;

    if (!isLoading && servers.length === 0) {
      return (
        <React.Fragment>
          <Typography className={classes.noServer} align='center'>
            Aucun serveur pour le moment.
          </Typography>
          <Typography align='center'>
            <Button component={Link} to={`/servers/new`} color="inherit">
              <AddIcon className={classes.leftIcon} />
              Nouveau
            </Button>
          </Typography>
        </React.Fragment>
      );
    }

    return (
      <List className={classes.root}
        ref={el => this.listElement = el}>
        <Infinite
          containerHeight={this.state.listHeight}
          isInfiniteLoading={isLoading && hasMore}
          loadingSpinnerDelegate={this.renderLoader()}
          elementHeight={67}>
          {servers.map((server) =>
            <ServersListItem key={server.serverid} server={server} />
          )}
        </Infinite>
      </List>
    );
  }

  renderLoader() {
    return (
      this.state.hasMore
        ? <Loader />
        : null
    );
  }

  componentDidUpdate() {
    this.updateListHeight();
  }

  updateListHeight() {
    const el = ReactDOM.findDOMNode(this.listElement); // eslint-disable-line
    const listHeight = el && el.clientHeight ? el.clientHeight-1 : 1;
    if (listHeight === this.state.listHeight) return;
    this.setState({listHeight});
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateListHeight);
    this.loadMoreServers();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateListHeight);
  }
}

const styles = () => ({
  root: {
    height: '100%',
    width: '100%',
    position: 'relative',
    paddingTop: 0,
    paddingBottom: 0,
  },
  noServer: {
    paddingTop: '50px'
  }
});

const mapStateToProps = state => {
  const { servers } = state;

  return {
    servers: servers.filter !== '' ? selectServersWithFilter(servers.byId, servers.filter) : selectAllServers(servers.byId)
  };
};

const connectedServersList = connect(mapStateToProps)(ServersList);
const styledServersList = withStyles(styles)(connectedServersList);
export { styledServersList as ServersList };
