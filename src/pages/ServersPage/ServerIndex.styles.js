
export default theme => ({
  root: {
    position: 'absolute',
    height: '100%',
    width: '100%'
  },
  list: {
    overflowY: 'auto'
  },
  tabs: {
    backgroundColor: theme.palette.primary.main,
    minHeight: 0,
    height: 0,
    transition: 'min-height .2s, height .2s, width: .2s'
  },
  tabsOpen: {
    minHeight: theme.spacing.unit * 6,
    height: theme.spacing.unit * 6,
  },
  tab: {
    minWidth: 'inherit',
    width: 'auto',
    flex: '1 1 auto'
    //height: 60
  },
  tabContainer: {
    height: '100%',
    width: '100%',
  },
  main: {
    marginTop: theme.spacing.unit * 7,
    height: `calc(100% - ${theme.spacing.unit * 7}px)`,
    transition: 'height .2s, margin-top .2s',
  },
  mainOpen: {
    [theme.breakpoints.down('md')]: {
      marginTop: theme.spacing.unit * 13,
      height: `calc(100% - ${theme.spacing.unit * 13}px)`
    }
  },
  expandButton: {
    color: theme.palette.primary.contrastText,
    minWidth: 66,
    padding: 0,
    height: theme.spacing.unit*6,
  },
  expandButtonOpen: {
    transition: 'height .2s',
    [theme.breakpoints.down('md')]: {
      height: theme.spacing.unit*12,
    }
  },
  navbar: {
  },
  toolbar: {
    padding: theme.spacing.unit,
    paddingTop: 0,
    minHeight:0,
  },
  searchBar: {
    marginTop: 0,
    marginBottom: 0,
    //marginLeft: theme.spacing.unit,
    //marginRight: theme.spacing.unit,
    width: '100%',
    background: theme.palette.primary.dark,
    borderRadius: 0
  },
  searchBarInput: {
    color: theme.palette.primary.contrastText,
  },
  searchBarIcon: {
    color: theme.palette.primary.contrastText
  },
  searchBarContainer: {
    backgroundColor: theme.palette.primary.main,
    display: 'flex',
    position: 'relative',
    '& > div > div': {
      width: 'inherit !important',
      minWidth: '50px !important'
    }
    //zIndex: theme.zIndex.appBar + 1,
    // paddingLeft: theme.spacing.unit * 2,
    //paddingRight: theme.spacing.unit * 1
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    [theme.breakpoints.up('lg')]: {
      transform: 'rotate(-90deg)',
    }
  },
  expandOpen: {
    transition: 'transform .2s',
    transform: 'rotate(180deg)',
    [theme.breakpoints.up('lg')]: {
      transform: 'rotate(90deg)',
    }
  },
});