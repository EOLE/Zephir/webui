import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import { /*locationActions,*/ serverActions } from '../../actions';
import 'leaflet/dist/leaflet.css';
import { debounce } from '../../helpers/utils';
import { ServersListItem } from './';

import {
  List
} from '@material-ui/core';

// Fix Leaflet icon loading
// See https://github.com/PaulLeCam/react-leaflet/issues/255
import L from 'leaflet';
delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});


const tilesURL = "http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png";
const attribution = "&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors";


class ServersMap extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      zoom: 13,
      position: [47.3170, 5.0474],
      isLoadingServers: false,
      servers: []
    };
    this.loadNearLocations = debounce(this.loadNearLocations, 1000, true).bind(this);
    this.updatePosition = debounce(this.updatePosition, 250, false).bind(this);
  }

  componentDidMount() {
    this.loadLocations();
  }

  loadLocations() {
    const map = this.map ? this.map.leafletElement : null;
    if (!map) return;
    // const bounds = map.getBounds();
    // const northWest = bounds.getNorthWest();
    // const southEast = bounds.getSouthEast();
    // this.props.dispatch(locationActions.getInRect(
    //   northWest.lat,
    //   northWest.lng,
    //   southEast.lat,
    //   southEast.lng
    // ));
  }

  loadLocationServers(locationId) {
    this.props.dispatch(serverActions.getAllByLocation(locationId))
      .then(servers => {
        this.setState({servers});
      })
    ;
  }

  updatePosition(evt) {
    this.setState(() => {
      return { position: evt.center, zoom: evt.zoom };
    }, () => {
      if (this.props.isLoading) return;
      this.loadLocations();
    });
  }

  render() {
    const { classes } = this.props;
    const { position, zoom, servers } = this.state;
    return (
      <div className={classes.root}>
        <Map className={classes.map}
          style={{height: servers.length > 0 ? '50%' : '100%'}}
          ref={el => this.map = el}
          onViewportChanged={this.updatePosition}
          center={position} zoom={zoom}>
          <TileLayer
            attribution={attribution} url={tilesURL} />
          { this.renderMarkers() }
        </Map>
        { this.renderServers() }
      </div>
    );
  }

  renderMarkers() {
    const { classes } = this.props;
    const { locations } = this.props;
    return locations.map(l => {
      const { lat, lon } = l;
      return (
        <Marker key={`marker-${l.id}`} position={[lat, lon]}>
          <Popup onOpen={() => this.loadLocationServers(l.id)}
            onClose={() => this.setState({servers: []})}>
            <div className={classes.popup}>
              <b>{l.name}</b>
            </div>
          </Popup>
        </Marker>
      );
    });
  }

  renderServers() {
    const { classes } = this.props;
    const { servers } = this.state;
    return (
      <List className={classes.servers} style={{height: servers.length > 0 ? '50%' : 0}}>
        {
          servers.map(s => {
            return (
              <ServersListItem key={s.id} server={s} />
            );
          })
        }
      </List>
    );
  }

}

const styles = () => ({
  root: {
    width: '100%',
    height: '100%',
    position: 'relative',
    overflow: 'hidden'
  },
  map: {
    width: '100%',
    transition: 'height 150ms ease-in-out'
  },
  servers: {
    transition: 'height 150ms ease-in-out'
  }
});

function filterLocations(locations, filter) {
  const re = new RegExp(filter, 'i');
  return locations.filter(l => re.test(l.name));
}

function mapStateToProps(state) {
  const { locations, servers } = state;
  return {
    locations: filterLocations(locations.inRectItems, servers.filter),
    isLoading: locations.inRectItemsLoading,
  };
}

const connectedServersMap = connect(mapStateToProps)(ServersMap);
const styledServersMap = withStyles(styles, { withTheme: true })(connectedServersMap);
export { styledServersMap as ServersMap };
