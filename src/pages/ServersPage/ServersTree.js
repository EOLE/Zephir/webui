import React from 'react';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router';
// import { locationActions } from '../../actions';

import {
  List,
  Divider,
} from '@material-ui/core';

import {
  Loader
} from '../../components';

import {
  ServersTreeItem
} from './';

class ServersTree extends React.Component {

  componentDidMount() {
    // this.props.dispatch(locationActions.getAll());
  }

  render() {
    const { classes, isLoading, locations } = this.props;

    return (
      <div className={classes.root}>
        { isLoading
          ? <Loader />
          : <List className={classNames(classes.list)}>
            {locations.map(location =>
              <React.Fragment key={`serverTreeItem-${location.id}`}>
                <ServersTreeItem serverLocation={location} />
                <Divider />
              </React.Fragment>
            )}
          </List>
        }
      </div>
    );
  }
}

const styles = () => ({
  root: {
    height: '100%',
    width: '100%',
    position: 'relative'
  }
});

function rootLocationsSelector(locations) {
  return (locations.items || []).filter(l => l.parentId === null);
}

function mapStateToProps(state) {
  const { locations } = state;
  return {
    locations: rootLocationsSelector(locations),
    isLoading: locations.itemsLoading,
  };
}

const connectedServersTree = connect(mapStateToProps)(ServersTree);
const styledServersTree = withStyles(styles)(connectedServersTree);
const routedServersTree = withRouter(styledServersTree);
export { routedServersTree as ServersTree };
