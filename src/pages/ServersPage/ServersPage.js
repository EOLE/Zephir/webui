/* globals Promise */
import React from 'react';
import { Route, withRouter } from 'react-router';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import {
  Button,
} from '@material-ui/core';
import {
  Add as AddIcon,
  Assessment as AssessmentIcon
} from '@material-ui/icons';
import {
  Redirect,
  Page,
  FadingSwitch,
  VSplit,
  RouteDataLoader,
} from '../../components';
import {
  ServersIndex,
  ServersDashboard,
  ServerDetail,
  ServerForm,
} from './';
import { serverActions, servermodelActions } from '../../actions';
import { selectServerById, selectServermodelById } from '../../selectors';

class ServersPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      serversListExpanded: false
    };
    this.handleServerListExpandClick = this.handleServerListExpandClick.bind(this);
    this.handleFilterChange = this.handleFilterChange.bind(this);
    this.loadRouteServer = this.loadRouteServer.bind(this);
  }

  render() {
    const { servers, classes } = this.props;
    const { serversListExpanded } = this.state;
    return (
      <Page
        title="Gestion des serveurs"
        loading={servers.itemsLoading}
        navRight={
          <div>
             <Button component={Link} to={`/servers/dashboard`} color="inherit">
              <AssessmentIcon className={classes.leftIcon} />
              Vue d&#39;ensemble
            </Button>

            <Button component={Link} to={`/servers/new`} color="inherit">
              <AddIcon className={classes.leftIcon} />
              Nouveau
            </Button>
          </div>
        }
      >

        <VSplit
          leftProps={serversListExpanded ? { sm: 6, md: 6, lg: 6 } : undefined}
          left={
            <Route path="/servers/:id?" render={props => this.renderServerIndex(props) } />
          }
          rightProps={serversListExpanded ? { sm: 6, md: 6, lg: 6 } : undefined}
          right={
            <FadingSwitch>
              <Route path="/servers/dashboard" render={() => <ServersDashboard inner={true} sm={serversListExpanded} />} />
              <Route path="/servers/new" render={() => <ServerForm inner={true} sm={serversListExpanded} />} />
              <Route path="/servers/:id/edit" render={(routeProps) => {
                return(
                  <RouteDataLoader loadData={this.loadRouteServer} {...routeProps}>
                    <ServerForm inner={true} sm={serversListExpanded} />
                  </RouteDataLoader>
                );
              }}/>
              <Route path="/servers/:id" render={(routeProps) => {
                return (
                  <RouteDataLoader loadData={this.loadRouteServer} {...routeProps}>
                    <ServerDetail inner={true} sm={serversListExpanded} />
                  </RouteDataLoader>
                );
              }} />
              <Redirect from="/servers" to="/servers/dashboard" />
            </FadingSwitch>
          } />
      </Page>
    );
  }

  renderServerIndex(routeProps) {
    const { match } = routeProps;
    return (
      <ServersIndex
        selectedServer={match.params.id}
        onFilterChange={this.handleFilterChange}
        onExpand={this.handleServerListExpandClick} />
    );
  }

  handleServerListExpandClick(expanded) {
    this.setState({
      serversListExpanded: expanded
    });
  }
  handleFilterChange(filter) {
    this.props.dispatch(serverActions.updateLocalFilter(filter));
  }

  loadRouteServer(match, dispatch) {
    const serverId = parseInt(match.params.id, 10);
    const { servers, servermodels } = this.props;
    // On s'assure que le serveur a été chargé
    const server = selectServerById(servers.byId, serverId);

    const serverPromise = server && server.serverenvironment ?
      Promise.resolve(server) :
      dispatch(serverActions.describe(serverId));
      dispatch(serverActions.selectionlist(serverId));

    return serverPromise.then(server => {
      // On s'assure que le modèle associé au serveur est également bien chargé
      const servermodel = selectServermodelById(
        servermodels.byId,
        server.servermodelid
      );
      if (servermodel) return Promise.resolve();
      return dispatch(servermodelActions.list(server.servermodelid));
    });

  }

}

const styles = (theme) => ({
  root: {
  },
  animatedRoute: {
    height: '100%',
    width: '100%',
    position: 'absolute'
  },
  switch: {
    height: '100%',
    width: '100%',
    position: 'absolute'
  },
  padding: {
    padding: theme.spacing.unit * 2
  },
  toolbar: {
    backgroundColor: theme.palette.primary.dark,
  },
  toolbarTitle: {
    color: theme.palette.primary.contrastText
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  }
});

function mapStateToProps(state) {
  const { servers, servermodels } = state;
  return {
    servers,
    servermodels
  };
}

const connectedServersPage = connect(mapStateToProps)(ServersPage);
const styledServersPage = withStyles(styles, { withTheme: true })(connectedServersPage);
const routedServerspage = withRouter(styledServersPage);
export { routedServerspage as ServersPage };
