/* globals Promise */
import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router';
// import { locationService, serverServices } from '../../services';
import { alertActions } from '../../actions';

import {
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  Collapse
} from '@material-ui/core';

import {
  ExpandMore as ExpandMoreIcon,
} from '@material-ui/icons';

import {
  ServerAvatar,
  ServersListItem,
} from './';

import {
  Loader
} from '../../components';

import styles from './ServersTreeItem.styles.js';


class ServersTreeItem extends React.Component {

  state = {
    expanded: false,
    isLoadingItems: false,
    locations: [],
    servers: []
  };

  fetchServersAndLocations() {
    this.setState(prevState => {

      const shouldLoad = !prevState.expanded;

      if (shouldLoad) {
        Promise.all([this.fetchServers(), this.fetchLocations()])
          .then(results => {
            const servers = results[0];
            const locations = results[1];
            this.setState({
              locations,
              servers,
              isLoadingItems: false,
            });
          })
          .catch(err => {
            this.props.dispatch(alertActions.error(err.toString()));
            this.setState({isLoadingItems: false});
          });
      }

      return {
        isLoadingItems: shouldLoad,
        expanded: shouldLoad
      };

    });

  }

  fetchLocations() {
    // return locationService.getAllByParent(this.props.serverLocation.id);
  }

  fetchServers() {
    // return serverService.getAllByLocation(this.props.serverLocation.id);
  }

  filterServers(servers, filter) {
    const re = new RegExp(filter, 'i');
    return servers.filter(s => re.test(s.name));
  }

  render() {

    const { classes, className, serverLocation, filter } = this.props;
    const { expanded, isLoadingItems, locations } = this.state;
    const servers = this.filterServers(this.state.servers, filter);

    return (
      <div className={classNames(classes.root, className)}>
        <ListItem
          key={`serversTreeItem-${serverLocation.id}`}>
          <ServerAvatar
            server={serverLocation}
            iconProps={{ classes: { icon: classes.icon }}} />
          <ListItemText
            inset
            classes={{ primary: classes.primary, secondary: classes.secondary}}
            primary={serverLocation.name} />
          <ListItemSecondaryAction className={classes.secondaryAction}>
            {
              isLoadingItems
                ? <Loader />
                : <IconButton
                  className={classes.button}
                  onClick={this.fetchServersAndLocations.bind(this)}>
                  <ExpandMoreIcon
                    className={classNames(classes.expand, {
                      [classes.expandOpen]: expanded,
                    })} />
                </IconButton>
            }
          </ListItemSecondaryAction>
        </ListItem>
        <Collapse in={expanded && !isLoadingItems} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {
              servers.length + locations.length === 0 && !isLoadingItems
                ? <ListItem>
                  <ListItemText
                    inset primary="Aucun serveur."
                    classes={{ primary: classes.primary, secondary: classes.secondary}} />
                </ListItem>
                : null
            }
            {
              servers.map(s => {
                return (
                  <ServersListItem
                    key={`serversTreeItem-${serverLocation.id}-server-${s.id}`}
                    className={classes.nested}
                    classes={classes}
                    server={s} />
                );
              })
            }
            {
              locations.map(sl => {
                return (
                  <ServersTreeItem
                    key={`serversTreeItem-${serverLocation.id}-location-${sl.id}`}
                    className={classes.nested}
                    classes={classes}
                    serverLocation={sl}
                    filter={filter} />
                );
              })
            }
          </List>
        </Collapse>
      </div>
    );
  }
}

ServersTreeItem.propTypes = {
  classes: PropTypes.object.isRequired,
  filter: PropTypes.string,
  serverLocation: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const { locations, servers } = state;
  return {
    isLoading: locations.itemsLoading,
    filter: servers.filter
  };
}

const styledServersTreeItem = withStyles(styles)(ServersTreeItem);
const connectedServersTreeItem = connect(mapStateToProps)(styledServersTreeItem);
const routedServersTreeItem = withRouter(connectedServersTreeItem);
export { routedServersTreeItem as ServersTreeItem };
