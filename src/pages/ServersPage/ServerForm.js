import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';

import {
  Grid,
  TextField,
  CircularProgress,
} from '@material-ui/core';

import {
  Check as CheckIcon
} from '@material-ui/icons';

import {
  Page,
  ResponsiveButton,
  ServerModelSelector,
  withForm
} from '../../components';

import {
  serverActions
} from '../../actions';

import { selectServerById } from '../../selectors';

class ServerForm extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      isSaving: false,
    };
  }

  componentDidUpdate() {
    const { form } = this.props;
    if (form.submitted && form.validated) this.saveServer();
  }

  saveServer() {
    this.setState({ isSaving: true }, () => {
      const { form, server } = this.props;
      let savePromise;
      if (server) { // Modification d'un serveur existant
        savePromise = this.props.dispatch(serverActions.update({
          ...server,
          ...form.data
        }));
      } else { // Création d'un nouveau serveur
        savePromise = this.props.dispatch(serverActions.create(form.data));
      }
      savePromise.then((server) => {
        this.props.history.push(`/servers/${server.serverid}`);
      })
        .catch(err => {
          form.reset();
          this.setState({isSaving: false});
          throw err;
        });
    });
  }

  render() {
    const {
      classes, theme, inner, style, sm, form, server, isLoading } = this.props;
    const { isSaving } = this.state;
    const { servername, serverdescription, servermodelid } = server || {
      servername: '',
      serverdescription: '',
      servermodelid: -1
    };
    return (
      (!isLoading || form.submitted) &&
      <Page
        title={server
          ? `Modifier ${server.servername}`
          : 'Nouveau Server'
        }
        className={classes.root}
        style={style}
        inner={inner}
        backLink={server && server.serverid ? `/servers/${server.serverid}` : '/servers'}

      >
        <div className={classes.content}>
          <form name="form" onSubmit={this.handleSubmit}>
            <Grid container spacing={theme.spacing.unit * 2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  name="servername"
                  label="Nom du serveur"
                  className={classes.textField}
                  value={form.field("servername", servername)}
                  onChange={form.onChange}
                  error={!!form.errors.servername}
                  disabled={isLoading || isSaving}
                  helperText={form.errors.servername}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  name="serverdescription"
                  label="Description du serveur"
                  className={classes.textField}
                  value={form.field("serverdescription", serverdescription)}
                  onChange={form.onChange}
                  error={!!form.errors.serverdescription}
                  disabled={isLoading || isSaving}
                  helperText={form.errors.serverdescription}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12}>
                <ServerModelSelector
                  name="servermodelid"
                  label="Modèle de serveur"
                  value={form.field("servermodelid", servermodelid)}
                  onChange={form.onChange}
                  error={!!form.errors.servermodelid}
                  disabled={isLoading || isSaving || server ? true : false}
                  helperText={form.errors.servermodelid}
                  fullWidth
                  margin="normal"
                />
              </Grid>
              { server ? null : this.renderPassphraseInputs() }
            </Grid>

            <ResponsiveButton
              sm={sm}
              onClick={form.onSubmit}
              style={{padding: '20px 0px', margin: '20px 0px'}}
              title='Enregistrer'
              color="primary"
              disabled={isLoading || isSaving}
              icon={form.submitted && isLoading
                ? (props =>
                  <CircularProgress
                    {...props}
                    color={inner ? 'primary' : 'inherit'}
                    style={{ marginBottom: inner ? 0 : theme.spacing.unit }}
                    size={20}
                  />
                )
                : CheckIcon
              }
            />

            {/*bouton caché permettant de soumettre le formulaire via la touche entrée*/}
            <input
              type="submit"
              style={{display: 'none'}}
              onClick={form.onSubmit}
            />

          </form>
        </div>
      </Page>
    );
  }

  renderPassphraseInputs() {
    const { classes, theme, form } = this.props;
    const { isLoading } = this.state;
    return (
      <Grid container spacing={theme.spacing.unit * 2}>
        <Grid item xs={12} sm={6}>
          <TextField
            name="serverpassphrase"
            label="Phrase de passe pour l'enrôlement du serveur"
            className={classes.textField}
            value={form.field('serverpassphrase', "")}
            onChange={form.onChange}
            error={!!form.errors.serverpassphrase}
            disabled={isLoading}
            inputProps={{type: "password"}}
            helperText={form.errors.serverpassphrase}
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            name="serverpassphrasecheck"
            label="Phrase de passe pour l'enrôlement du serveur (vérification)"
            className={classes.textField}
            value={form.field('serverpassphrasecheck', "")}
            onChange={form.onChange}
            error={!!form.errors.serverpassphrasecheck}
            disabled={isLoading}
            inputProps={{type: "password"}}
            helperText={form.errors.serverpassphrasecheck}
            fullWidth
          />
        </Grid>
      </Grid>
    );
  }
}

const styles = theme => ({
  root: {
    width: '100%',
    height: '100%'
  },
  content: {
    padding: theme.spacing.unit * 2
  },
  formControl: {
    '& > div': {
      width: '100%'
    }
  },
  textField: {
  },
  passwordField: {
    //lineHeight: 3,
    height: 'auto'
  }
});

ServerForm.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,
  server: PropTypes.object
};

const managedForm = withForm({
  servername: (value, formData, submitting) => {
    if (submitting && !value) return "Le nom du serveur est obligatoire.";
  },
  serverdescription: (value, formData, submitting) => {
    if (submitting && !value) return "La description du serveur est obligatoire.";
  },
  servermodelid: (value, formData, submitting) => {
    if (submitting && (value === undefined || value === -1)) return "Vous devez sélectionner un modèle de serveur.";
  },
  serverpassphrase: (value, formData, submitting) => {
    if (submitting && !value) return "La phrase de passe est obligatoire.";
    if (!value || (value.length < 4 || value.length >= 1024)) return "La phrase de passe doit être comprise entre 4 et 1023 charactères";
  },
  serverpassphrasecheck: (value, {serverpassphrase}) => {
    if (value !== serverpassphrase) return "Les deux phrases de passe ne sont pas identiques.";
  },
})(ServerForm);

const mapStateToProps = ({ servers }, { match }) => {

  let serverId = match.params.id;
  if (serverId === "new") return { server: null };

  serverId =  parseInt(serverId, 10);
  const props =  {
    serverId: serverId,
    server: selectServerById(servers.byId, serverId)
  };

  return props;

};

const connectedServerForm = connect(mapStateToProps)(managedForm);
const styledServerForm = withStyles(styles, { withTheme: true })(connectedServerForm);
const routedServerForm = withRouter(styledServerForm);
export { routedServerForm as ServerForm };
