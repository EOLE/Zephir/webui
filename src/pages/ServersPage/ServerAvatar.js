import React from 'react';
import { withStyles } from '@material-ui/core/styles';

import {
  Avatar
} from '@material-ui/core';

import {
  Dns as DefaultServerIcon,
  VpnLock as VpnIcon,
  Apps as SpIcon,
  Computer as ComputerIcon,
  Router as RouterIcon,
  Cloud as CloudIcon,
  DevicesOther as OtherIcon,
  Security as ProxyIcon,
  Contacts as IdpIcon,
  Web as WebIcon
} from '@material-ui/icons';

import {
  Firewall as FirewallIcon,
  NetworkDisk as NasIcon,
  Zephir as ZephirIcon,
  Pi as RpiIcon,
  Database as DatabaseIcon
} from '../../assets/icons/';

const styles = () => ({
  icon: {}
});

// eslint-disable-next-line no-unused-vars
const ServerIcon = withStyles(styles)(({ server, classes, ...props }) => {
  let Icon = {
    zephir: ZephirIcon,
    router: RouterIcon,
    vpn: VpnIcon,
    firewall: FirewallIcon,
    proxy: ProxyIcon,
    cloud: CloudIcon,
    nas: NasIcon,
    computer: ComputerIcon,
    sp: SpIcon,
    idp: IdpIcon,
    rpi: RpiIcon,
    db: DatabaseIcon,
    web: WebIcon,
    other: OtherIcon,
  }[server.type] || DefaultServerIcon;

  return (
    <Icon {...props} />
  );
});

// eslint-disable-next-line no-unused-vars
const ServerAvatar = ({ server, theme, iconProps, classes, ...props }) => {

  let color = {
    ok: theme.palette.success.main,
    warning: theme.palette.warning.main,
    error: theme.palette.error.main
  }[server.status];

  return (
    <Avatar style={{ backgroundColor: color }} { ...props }>
      <ServerIcon server={server} { ...iconProps } />
    </Avatar>
  );
};

const styledServerAvatar = withStyles(styles, { withTheme: true })(ServerAvatar);
export { styledServerAvatar as ServerAvatar };
