import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router';
import { Route, Switch} from 'react-router';
import { NavLink } from 'react-router-dom';

import {
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  ListSubheader,
  IconButton,
  Collapse,
  Divider,
  Card,
  CardContent,
  Typography,
  ButtonBase
} from '@material-ui/core';

import {
  ExpandMore as ExpandMoreIcon,
} from '@material-ui/icons';

import {
  ServerAvatar
} from './';

import styles from './ServersListItem.styles.js';

class ServersListItem extends React.Component {

  state = {
    expanded: false
  };

  handleExpandClick() {
    this.setState({ expanded: !this.state.expanded });
  }

  render() {
    const { classes, className, server, serverselection } = this.props;
    const { expanded } = this.state;

    const ServersListItem_Card = () => {
      return (
        <Card className={classes.card}>
          <ButtonBase className={classes.content}
            key={`serverListItem-${server.serverid}`}
            component={NavLink}
            to={`/servers/${server.serverid}`}>
            <CardContent style={{textAlign: "center"}}>
              <Typography variant="h5">{server.servername}</Typography>
              <Typography variant="subtitle1" color="textSecondary">
                {server.serverdescription}
              </Typography>
            </CardContent>
          </ButtonBase>
        </Card>
      );
    };
    const ServersListItem_List = () => {
      return (
        <div className={classNames(classes.root, className)}>
          <ListItem
            key={`serverListItem-${server.serverid}`}
            component={NavLink}
            to={{pathname:`/servers/${server.serverid}`, serverselection: serverselection }}
            activeClassName={classes.activeLink}>
            <ServerAvatar
              server={server}
              iconProps={{ classes: { icon: classes.icon }}}
            />
            <ListItemText
              inset
              classes={{ primary: classes.primary, secondary: classes.secondary}}
              primary={server.servername}
              secondary={server.serverdescription}
            />
            {server.children && !!server.children.length &&
            <ListItemSecondaryAction className={classes.secondaryAction}>
              <IconButton
                className={classes.button}
                onClick={this.handleExpandClick.bind(this)}
              >
                <ExpandMoreIcon
                  className={classNames(classes.expand, {
                    [classes.expandOpen]: expanded,
                  })}
                />
              </IconButton>
            </ListItemSecondaryAction>
            }
            {server.children && !!server.children.length &&
              <Collapse in={expanded} timeout="auto" unmountOnExit>
                <Divider />
                <List component="div" disablePadding subheader={
                  server.location
                    ? <ListSubheader>
                      {server.location.name}
                    </ListSubheader>
                    : ''
                }>
                  {server.children.map(child =>
                    <ServersListItem
                      key={`server-${child.serverid}`}
                      className={classes.nested}
                      classes={classes}
                      server={child}
                    />
                  )}
                </List>
                <Divider />
              </Collapse>
            }
          </ListItem>
          </div>
      );
    };

    const ServersListItem_Pick = () => {
      return (
          <ListItem
            divider
            onClick={this.props.onClick}
            key={`serverListItem-${server.serverid}`}>
            <ServerAvatar
              server={server}
              iconProps={{ classes: { icon: classes.icon }}}
            />
            <ListItemText
              inset
              primary={server.servername}
              secondary={server.serverdescription}
            />

          </ListItem>
      );
    };
    return (
      <div style={{margin: '0 auto', maxWidth: '1270px'}}>
        <Switch>
          <Route exact path="/" component={ServersListItem_Card}/>
          <Route path="/servers/" component={ServersListItem_List}/>
          <Route path="/serverselections/new" component={ServersListItem_Pick}/>
          <Route exact path="/serverselections/:id" component={ServersListItem_List} />
          <Route exact path="/serverselections/:id/edit" component={ServersListItem_Pick}/>
        </Switch>
      </div>
    );
  }
}


ServersListItem.propTypes = {
  classes: PropTypes.object.isRequired,
};

const routedServersListItem = withRouter(ServersListItem);
const styledServersListItem = withStyles(styles)(routedServersListItem);
export { styledServersListItem as ServersListItem };
