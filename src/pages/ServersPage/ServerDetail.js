import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import {
  List,
  ListItem,
  ListItemText,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  withMobileDialog,
  Typography,
  GridList,
  GridListTile,
  Chip
} from '@material-ui/core';

import {
  Edit as EditIcon,
  Delete as DeleteIcon,
} from '@material-ui/icons';
import {
  selectServerById,
  selectServermodelById
} from '../../selectors';
import {
  Page,
  Loader,
  ResponsiveButton,
} from '../../components';

import {
  EnvironmentData
} from './';

import { serverActions, appActions } from '../../actions';

import { ServerApps } from './ServerApps';

class ServerDetail extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      deleteDialogOpen: false,
      isDeletingServer: false,
    };
    this.onAppClick = this.onAppClick.bind(this);
  }

  handleDeleteDialogOpenClick() {
    this.setState({ deleteDialogOpen: true });
  }

  handleDeleteDialogCloseClick() {
    this.setState({ deleteDialogOpen: false });
  }

  handleConfirmDeleteClick() {
    const { server } = this.props;
    this.setState({isDeletingServer: true}, () => {
      this.props.dispatch(serverActions.delete(server.serverid, server.servername))
        .finally(() => this.setState({ deleteDialogOpen: false, isDeletingServer: false }))
        .then(() => this.props.dispatch(serverActions.list()))
        .then(() => this.props.history.push(`/servers`));
    });
  }
  handleNone(){}

  render(){
    const { classes, theme, fullScreen, inner, sm, availableApps, isLoading, server, servermodel } = this.props;
    const prevServerSelection = this.props.location.serverselection;
    const back = (prevServerSelection && "/serverselections/"+prevServerSelection) || `/servers`;
    return (
      <Page
        title={server && server.servername}
        loading={isLoading}
        className={classes.root}
        inner={inner}
        backLink={back}
        navRight={
          server &&
          <div>
            <ResponsiveButton
              sm={sm}
              component={Link}
              to={`/servers/${server.serverid}/edit`}
              icon={EditIcon}
              title="Modifier"
            />
            <ResponsiveButton
              sm={sm}
              onClick={this.handleDeleteDialogOpenClick.bind(this)}
              icon={DeleteIcon}
              title="Supprimer"
            />
          </div>
        }
      >
        {server && !isLoading &&
          <div style={{margin: "0 10px"}}>
            <GridList cellHeight={'auto'} className={classes.gridList}>
              <GridListTile>
                <List dense className={classes.card}>
                  <Typography variant="subtitle1">
                    Informations Générales
                  </Typography>
                  <ListItem divider >
                    <ListItemText primary="Nom du serveur" secondary={server.servername} />
                  </ListItem>
                  <ListItem divider >
                    <ListItemText primary="Description du serveur" secondary={server.serverdescription} />
                  </ListItem>
                  <ListItem divider >
                    <ListItemText primary="Zone géographique" secondary="--" />
                  </ListItem>
                  <ListItem divider >
                    <Button component={Link} to={`/servermodels/${server.servermodelid}`}>
                      <ListItemText primary="Modèle" secondary={servermodel && `${servermodel.model.servermodelname} ${servermodel.model.subreleasename}`} />
                    </Button>
                  </ListItem>

                </List>
                <Typography variant="subtitle1">
                  Sélections :
                </Typography>
                {server.selections && server.selections.map((selection) =>
                  <Chip
                    variant="outlined"
                    className={classes.chip}
                    key={`${selection.serverselectionid}`}
                    component={Link}
                    onClick={this.handleNone}
                    to={`/serverselections/${selection.serverselectionid}`}
                    label={`${selection.serverselectionname}`}
                  />
                )}
              </GridListTile>
              <GridListTile>
                <Typography variant="subtitle1">
                  Actions
                </Typography>
                <ServerApps server={server}
                  availableApps={availableApps}
                  peered={server.serverenvironment}
                  onAppClick={this.onAppClick} />
              </GridListTile>
              {server.serverenvironment &&

                <GridListTile cols={2}>
                  <EnvironmentData environment={server.serverenvironment} lastpeerconnection={server.lastpeerconnection} />
                </GridListTile>
              }
            </GridList>
          </div>
        }
        {isLoading && <Loader />}
        <Dialog
          fullScreen={fullScreen}
          open={this.state.deleteDialogOpen}
          onClose={this.handleDeleteDialogCloseClick.bind(this)}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{"Êtes-vous sûr ?"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Le serveur sera définitivement supprimé, êtes-vous sûr de
              vouloir poursuivre ?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleDeleteDialogCloseClick.bind(this)}>
              Annuler
            </Button>
            <Button onClick={this.handleConfirmDeleteClick.bind(this)} style={{color: theme.palette.error.main }} autoFocus>
              Supprimer
            </Button>
          </DialogActions>
        </Dialog>
      </Page>
    );
  }

  onAppClick(app) {
    const { server } = this.props;
    this.props.dispatch(appActions.openApp(app.name, {server}))
      .then(instance => this.props.history.push(`/apps/${instance.id}`));
  }
}

const styles = theme => ({
  root: {
    height: '100%',
    //zIndex: 3
  },
  container: {
    height: 'auto',
    [theme.breakpoints.up('md')]: {
      height: '100%',
      overflow: 'hidden'
    }
  },
  col: {
    backgroundColor: theme.palette.background.paper,
    maxHeight: '100%',
    overflowY: 'auto'
  },
  listSubheader: {
    backgroundColor: theme.palette.background.paper
  },
  chip: {
    margin: '5px'
  },
});

ServerDetail.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  fullScreen: PropTypes.bool.isRequired,
};

function mapStateToProps({ apps, servers, servermodels}, { match }) {
  const serverId = parseInt(match.params.id, 10);
  const server = selectServerById(servers.byId, serverId);

  let servermodel;
  if (server) {
    servermodel = selectServermodelById(
      servermodels.byId,
      server.servermodelid
    );
    server.selections = servers.selections;
  }
  return {
    availableApps: apps.available,
    server,
    servermodel
  };
}

const connectedServerDetail = connect(mapStateToProps)(ServerDetail);
const styledServerDetail = withStyles(styles, { withTheme: true })(connectedServerDetail);
const routedServerDetail = withRouter(styledServerDetail);
const mobiledServerDetail = withMobileDialog()(routedServerDetail);
export { mobiledServerDetail as ServerDetail };
