import React from 'react';
import PropTypes from 'prop-types';
import {
  List, ListItemText,
  ListItem, ListItemIcon,
  Divider
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { AppIcon } from '../../components';

class ServerApps extends React.Component {
  render() {
    return (
      <List>
        { this.renderApps() }
      </List>
    );
  }

  renderApps() {
    const { availableApps, onAppClick, peered} = this.props;
    const categories = this.groupByCategory(availableApps);
    return Object.keys(categories).map(categoryName => {
      const apps = categories[categoryName];
      const nodes = apps.map(app => {
        if ((peered === undefined  || Object.keys(peered).length === 0) && app.category === 'actions'){return}
        return (
          <ListItem key={`app-${app.name}`}
            button
            onClick={() => onAppClick(app)}>
            <ListItemIcon>
              <AppIcon icon={app.icon} />
            </ListItemIcon>
            <ListItemText primary={app.label} />
          </ListItem>
        );
      });
      nodes.push(<Divider key={`divider-${categoryName}`} />);
      return nodes;
    });
  }

  groupByCategory(apps) {
    return apps.reduce((categories, app) => {
      if(!(app.category in categories)) categories[app.category] = [];
      categories[app.category].push(app);
      return categories;
    }, {});
  }

}

ServerApps.propTypes = {
  classes: PropTypes.object.isRequired,
  server: PropTypes.object.isRequired,
  availableApps: PropTypes.array.isRequired,
  onAppClick: PropTypes.func.isRequired
};

const styles = () => {
  return {
    listSubheader: {}
  };
};

const styledServerApps = withStyles(styles, { withTheme: true })(ServerApps);
export { styledServerApps as ServerApps };
