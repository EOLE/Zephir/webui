
export default theme => ({
  root: {

  },
  nested: {
    marginLeft: theme.spacing.unit * 2,
  },
  activeLink: {
    backgroundColor: theme.palette.action.active,
    color: theme.palette.primary.contrastText,
    '& $primary, & $secondary, & $icon, & + $secondaryAction $button': {
      color: theme.palette.primary.contrastText,
    },
  },
  primary: {},
  secondary: {},
  secondaryAction: {},
  icon: {},
  button: {},
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
});