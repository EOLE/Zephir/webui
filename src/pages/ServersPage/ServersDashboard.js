import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import Media from 'react-media';

import {
  List,
  ListItem,
  ListItemText,
  Card,
  CardContent,
  Avatar,
  Typography,
  ListSubheader,
  Button
} from '@material-ui/core';
import {
  Edit as EditIcon,
  Add as AddIcon,
} from '@material-ui/icons';

import {
  Page, VSplit, Loader
} from '../../components';

class ServersDashboard extends React.Component {

  render(){

    const { classes, servers, users, theme, inner, backLink } = this.props;

    return (
      <Media query={{ maxWidth: theme.breakpoints.values.sm }}>
        {() =>
          <Page
            title="Veuillez sélectionner un serveur"
            className={classes.root}
            inner={inner}
            backLink={!inner && backLink}
          >
            {/* Pour une future implentation, n'affiche rien pour l'instant */}
            {servers.itemsLoading
              ? <Loader />
              : <VSplit
                leftProps={{ sm: true, md: true, lg: 9, style: { maxHeight: 'auto', background: theme.palette.background.paper } }}
                rightProps={{ sm: true, md: true, lg: 3, style: { maxHeight: 'auto', background: theme.palette.background.default } }}
                right={servers.items && servers.items.length >= 2 &&
                         users.items && users.items.length >= 2 &&
                    <List subheader={
                      <ListSubheader className={classes.listSubheader}>
                        Activité récente
                      </ListSubheader>
                    }>
                      <ListItem>
                        <Avatar className={classes.avatar}>
                          <EditIcon />
                        </Avatar>
                        <ListItemText
                          primary={
                            <div>
                              {'Serveur modifié : '}
                              <Link to={`/servers/${servers.items[0].id}`}>
                                {servers.items[0].name}
                              </Link>
                            </div>
                          }
                          secondary={
                            <span>
                              <Link to={`/users/${servers.items[1].id}`}>
                                {users.items[1].username}
                              </Link>
                              {' - Il y a 2 min'}
                            </span>
                          }
                        />
                      </ListItem>
                      <ListItem>
                        <Avatar className={classes.avatar}>
                          <AddIcon />
                        </Avatar>
                        <ListItemText
                          primary={
                            <div>
                              {'Serveur créé : '}
                              <Link to={`/servers/${servers.items[0].id}`}>
                                {servers.items[0].name}
                              </Link>
                            </div>
                          }
                          secondary={
                            <span>
                              <Link to={`/users/${servers.items[1].id}`}>
                                {users.items[1].username}
                              </Link>
                              {' - Il y a 5 min'}
                            </span>
                          }
                        />
                      </ListItem>
                      <ListItem style={{ textAlign: 'center' }}>
                        <Button fullWidth>
                          <AddIcon className={classes.leftIcon} />
                          Plus
                        </Button>
                      </ListItem>
                    </List>
                }
                left={servers.items &&
                    <List subheader={
                      <ListSubheader className={classes.listSubheader}>
                        Données globales
                      </ListSubheader>
                    }>
                      <ListItem style={{ padding: theme.spacing.unit * 2 }}>
                        <Card className={classes.card}>
                          <CardContent className={classes.cardContent}>
                            <Typography variant="subtitle1" component="h2" color="textSecondary">
                            Serveurs enregistrés
                            </Typography>
                            <Typography variant="h5" component="span">
                              {servers.count}
                            </Typography>
                          </CardContent>
                        </Card>
                      </ListItem>
                    </List>
                }
              />
            }
          </Page>
        }
      </Media>
    );
  }
}

const styles = theme => ({
  listSubheader: {
    background: theme.palette.background.paper
  },
  leftIcon: {
    marginRight: theme.spacing.unit
  },
  card: {
    width: 'auto',
    position: 'relative',
    overflow: 'hidden'
  },
  cardContent: {

  },
  cardIcon: {
  }
});

ServersDashboard.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

const styledServersDashboard = withStyles(styles, { withTheme: true })(ServersDashboard);

function mapStateToProps(state) {
  const { servers, users } = state;
  return {
    servers,
    users
  };
}

const connectedServersDashboard = connect(mapStateToProps)(styledServersDashboard);
export { connectedServersDashboard as ServersDashboard };
