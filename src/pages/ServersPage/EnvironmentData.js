import React, { Component } from 'react';
import { JsonTable } from 'react-json-to-html';
import { Typography } from '@material-ui/core';


export class EnvironmentData extends Component {

  render() {
    const { environment, lastpeerconnection } = this.props;
    const data = () => {
      if ( JSON.stringify(environment) === "{}" || environment === undefined  || Object.keys(environment).length === 0) {
        return "Il n'y a aucune donnée d'environnement disponible.";
      } else {
        const lowdata_environment = {}
        lowdata_environment["ip"] = environment["creole.general.ip_server"]
        lowdata_environment["netmask"] = environment["creole.general.netmask_server"]
        lowdata_environment["gateway"] = environment["creole.interface_0.test_adresse_ip_gw"]
        lowdata_environment["dns"] = environment["creole.general.test_adresse_ip_dns"]
        return (
          <JsonTable
            json={ lowdata_environment }
          />
        );
      }
    };
    let connection = "Pas de connexion récemment";
    if ( lastpeerconnection !== "") {
      connection = `Dernière connection : ${lastpeerconnection}`;
    }
    return (
      <div>
        <Typography variant="h5" component="h3">
        Données d&#39;environnement du serveur
        </Typography>
        {connection}<br />
        {data()}
      </div>
    );
  }
}
