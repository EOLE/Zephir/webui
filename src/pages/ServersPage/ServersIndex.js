import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router';
import SwipeableViews from 'react-swipeable-views';

import classNames from 'classnames';
import Media from 'react-media';

import {
  VSplit,
  Navbar,
} from '../../components';

import SearchBar from 'material-ui-search-bar';

import {
  Search as SearchIcon,
  Close as CloseIcon,
  FormatAlignJustify as ListIcon,
  FormatAlignRight as TreeIcon,
  Map as MapIcon,
} from '@material-ui/icons';

import { uiActions } from '../../actions';

import {
  ServersTree,
  ServersList,
  ServersMap
} from './';

import styles from './ServerIndex.styles.js';

class ServersIndex extends React.Component {

  constructor(props) {
    super(props);
    const { location, ui, dispatch } = this.props;
    if(location.query.tab){
      dispatch(uiActions.changeServersIndexTab(location.query.tab));
    }
    this.state = {
      expanded: false,
      activeTab: ui.serversIndexActiveTab
    };
    this.tabs = [
      // TODO Réactiver les vues arbres/carto lorsque le backend gérera les
      // zones géographiques
      { value: 'tree', icon: <TreeIcon />, component: ServersTree },
      { value: 'list', icon: <ListIcon />, component: ServersList },
      { value: 'map', icon: <MapIcon />, component: ServersMap },
    ];
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if(  nextProps.ui.serversIndexActiveTab
      && nextProps.ui.serversIndexActiveTab !== prevState.activeTab)
    {
      return { activeTab: nextProps.ui.serversIndexActiveTab };
    }
    return null;
  }

  handleExpandClick = () => {
    this.props.onExpand(!this.state.expanded);
    this.setState({ expanded: !this.state.expanded });
    setTimeout(() => window.dispatchEvent(new Event('resize')), 200);
  };

  handleFilterChange = filter => {
    this.props.onFilterChange(filter);
  };

  handleTabChange = (e, activeTab) => {
    this.props.dispatch(uiActions.changeServersIndexTab(activeTab));
  };

  handleTabChangeIndex = index => {
    this.props.dispatch(uiActions.changeServersIndexTab(this.tabs[index].value));
  };

  render(){

    const { classes, theme, selectedServer } = this.props;
    const { expanded, activeTab } = this.state;
    /*const activeTabIcon = this.tabs.reduce((icon, tab) => {
      if(tab.value === activeTab) return tab.icon;
      return icon;
    }, null);*/
    const activeTabIndex = this.tabs.reduce((tabIndex, tab, i) => {
      if(tab.value === activeTab) return i;
      return tabIndex;
    }, -1);

    return (
      <Media query={{ minWidth: theme.breakpoints.values.lg }}>
        {() =>
          <div className={classes.root}>
            <Navbar classes={{ appBar: classes.navbar, toolbar: classes.toolbar }}>
              <VSplit
                style={{ height: 'auto', width: 0 /* Flex Hack */ }}
                leftProps={expanded ? { sm: 12, md: 12, lg: 6 } : { sm: 12, md: 12, lg: 12 }}
                left={
                  <div className={classes.searchBarContainer}>
                    <SearchBar
                      placeholder="Filtrer les serveurs"
                      classes={{
                        root: classes.searchBar,
                        input: classes.searchBarInput
                      }}
                      onChange={this.handleFilterChange}
                      onRequestSearch={this.handleFilterChange}
                      searchIcon={<SearchIcon className={classes.searchBarIcon} />}
                      closeIcon={<CloseIcon className={classes.searchBarIcon} />}
                    />
                  </div>
                }
              />
            </Navbar>
            <main
              className={classNames(classes.main, {
                [classes.mainOpen]: expanded,
              })}
            >
              <SwipeableViews
                style={{
                  height: '100%',
                  position: 'relative'
                }}
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={activeTabIndex}
                onChangeIndex={this.handleTabChangeIndex}>
                {this.tabs.map(({ value, component: Component }) =>
                  <div key={`${value}-container`} dir={theme.direction} className={classes.tabContainer}>
                    <Component selectedServer={selectedServer} currentTab={activeTab} />
                  </div>
                )}
              </SwipeableViews>
            </main>
          </div>
        }
      </Media>
    );
  }
}

ServersIndex.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  onFilterChange: PropTypes.func.isRequired,
  selectedServer: PropTypes.string
};

const mapStateToProps = ({ ui }) => ({ ui });

const styledServersIndex = withStyles(styles, { withTheme: true })(ServersIndex);
const routedServersIndex = withRouter(styledServersIndex);
const connectedServersIndex = connect(mapStateToProps)(routedServersIndex);
export { connectedServersIndex as ServersIndex };
