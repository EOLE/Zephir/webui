import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Close as CloseIcon } from '@material-ui/icons';
import {
  IconButton
} from '@material-ui/core';
import {
  Page,
  AppIcon
} from '../../components';
import bg from '../../assets/login-bg.jpg';
import { getInstanceLabel, getAppClass } from '../../helpers/apps';

class AppDetail extends React.Component {
  render() {
    const { classes, instance, onClose } = this.props;
    const title = getInstanceLabel(instance);
    let id = '';
    let type = '';
    if (instance.opts.server) {id = instance.opts.server.serverid ; type = 'servers';}
    if (instance.opts.servermodel) {id = instance.opts.servermodel.servermodelid; type = 'servermodels';}
    return (
      <Page title={title}
        className={`${classes.root} ${classes.appBar}`} navLeft={
          <div className={classes.leftIcon}>
            <AppIcon icon={instance.icon}  />
          </div>
        }
        navRight={
          <IconButton onClick={() => onClose(instance.id, id, type)} color="inherit">
            <CloseIcon />
          </IconButton>
        }>
        { this.renderApp() }
      </Page>
    );
  }

  renderApp() {
    const { instance } = this.props;
    const AppClass = getAppClass(instance.appName);
    return <AppClass instance={instance} />;
  }
}

const styles = theme => ({
  root: {
    height: '100%',
    backgroundColor: theme.palette.primary.light,
    backgroundImage: `linear-gradient(rgba(255, 255, 255, 0.45), rgba(200, 150, 255, 0.45)), url(${bg})`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
    backgroundSize: 'cover'
  },
  container: {
    height: '100%',
    width: '100%',
    margin: 0,
    padding: 0,
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
    border: 'none'
  },
  appBar: {
    backgroundColor: theme.palette.grey[900]
  },
  leftIcon: {
    marginRight: theme.spacing.unit
  }
});

AppDetail.propTypes = {
  classes: PropTypes.object.isRequired,
  instance: PropTypes.object.isRequired
};

const styledAppDetail = withStyles(styles)(AppDetail);
export { styledAppDetail as AppDetail };
