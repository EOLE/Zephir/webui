import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { history } from '../../helpers';
import { appActions } from '../../actions';
import {
  AppDetail,
} from './';
import {
  Route,
  Redirect
} from 'react-router';

class AppsPage extends React.Component {

  constructor(props) {
    super(props);
    this.closeApp = this.closeApp.bind(this);
    this.renderApps = this.renderApps.bind(this);
  }

  closeApp(id,srcid,type) {
    this.props.dispatch(appActions.closeApp(id,srcid));
    //history.goBack();
    history.push("/"+type+"/"+srcid);
  }

  render() {
    return <Route path="/apps/:id" render={this.renderApps} />;
  }

  renderApps(routeProps) {

    const { openedApps, classes } = this.props;
    const { id } = routeProps.match.params;

    if(!this.instanceExists(id)) {
      return <Redirect to="/" />;
    }

    return (
      <div className={classes.root}>
        {openedApps.map((instance) => {
          const isFocused = instance.id === id;
          return (
            <div key={`app-container-${instance.id}`}
              className={`${classes.appContainer} ${isFocused ? classes.focusedApp : classes.unfocusedApp}`}>
              <AppDetail instance={instance} onClose={this.closeApp} />
            </div>
          );
        })}
      </div>
    );
  }

  instanceExists(instanceId) {
    for(let instance, i = 0; (instance = this.props.openedApps[i]); ++i) {
      if(instance.id === instanceId) return true;
    }
    return false;
  }
}

const styles = () => ({
  root: {
    position: 'absolute',
    height: '100%',
    width: '100%'
  },
  appContainer: {
    height: '100%',
    width: '100%'
  },
  focusedApp: {
    opacity: 'block',
    zIndex: 1
  },
  unfocusedApp: {
    display: 'none',
    zIndex: -1
  }
});

function mapStateToProps(state) {
  const { apps } = state;
  return {
    openedApps: apps.opened
  };
}

const connectedAppsPage = connect(mapStateToProps)(AppsPage);
const styledAppsPage = withStyles(styles, { withTheme: true })(connectedAppsPage);
export { styledAppsPage as AppsPage };
