import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  Grid, ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  Typography,
  Button
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { Page } from '../../components';
import {
  ExpandMore as ExpandMoreIcon,
  Close as CloseIcon
} from '@material-ui/icons';
import { JobsNavRight } from './JobsNavRight';
import { JobDetails } from './JobDetails';
import { execActions } from '../../actions';


class JobsPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      jobId: null,
      job: null,
      isLoading: true
    };
    this.onRefresh = this.onRefresh.bind(this);
  }

  // FIXME = Should'nt work ====>(nextProps, prevState)
  static getDerivedStateFromProps(nextProps, prevProps) {

    const nextMatch = nextProps.match;
    const prevMatch = prevProps ? prevProps.match : null;
    const nextJobId = nextMatch.params.id;
    const prevJobId = prevMatch ? prevMatch.params.id : null;

    if (nextJobId === prevJobId) return null;

    return { jobId: nextJobId };

  }

  componentDidUpdate(prevProps, prevState) {
    const { jobId } = this.state;
    if (jobId !== prevState.jobId) this.loadJob(jobId);
  }

  componentDidMount() {
    const { jobId } = this.state;
    if (jobId) this.loadJob(jobId);
  }

  render() {
    const { history } = this.props;
    const { jobId } = this.state;
    return (
      <Page title={`Détails du job #${jobId}`} navRight={
        <div>
          <Button color="inherit">
            <JobsNavRight onRefresh={this.onRefresh} />
          </Button>

          <Button onClick={history.goBack} color="inherit">
            <CloseIcon />
            Fermer
          </Button>
        </div>
      }>
        { this.renderContent() }
      </Page>
    );
  }

  renderContent() {

    const { classes } = this.props;
    const { job } = this.state;

    return (
      <div className={classes.content}>
        <Grid container className={classes.section}>
          <Grid item xs={12}>
            {job &&
              <ExpansionPanel key={job.job_id} defaultExpanded={true}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                  <Typography variant="h5">{this.getJobLabel(job)}</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                  <JobDetails job={job} />
                </ExpansionPanelDetails>
              </ExpansionPanel>
            }
          </Grid>
        </Grid>
      </div>
    );
  }

  getJobLabel(job) {
    return `"${job.command}" sur la cible "${job.server_id}"`;
  }

  loadJob(jobId) {
    this.setState({ isLoading: true }, () => {
      this.props.dispatch(execActions.describeJob(jobId))
        .then((job) => {
          this.setState({ job });
        })
        .finally(() => this.setState({ isLoading: false }));
    });
  }

  onRefresh() {
    const { jobId } = this.state;
    this.loadJob(jobId);
  }

}

JobsPage.propTypes = {
  match: PropTypes.object.isRequired
};

const styles = (theme) => ({
  content: {
    padding: theme.spacing.unit * 2,
    width: '100%',
    height: '100%'
  },
  section: {
    marginTop: theme.spacing.unit
  },
  alignRight: {
    textAlign: 'right'
  }
});

function mapStateToProps(/*state*/) {
  return {};
}

const connectedJobsPage = connect(mapStateToProps)(JobsPage);
const styledJobsPage = withStyles(styles, { withTheme: true })(connectedJobsPage);
export { styledJobsPage as JobsPage };
