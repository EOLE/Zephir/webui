import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import * as JobViews from './JobViews';
import { Typography } from '@material-ui/core';

class JobDetails extends React.Component {

  render() {
    const { job, classes } = this.props;
    const JobView = this.selectJobView(job);
    return (
      <div className={classes.root}>
        { JobView
          ? <JobView job={job} />
          : <Typography>
            Le détail de ce type d&#39;opération n&#39;est pas encore géré par Zéphir.
          </Typography>
        }
      </div>
    );
  }

  selectJobView(job) {
    const viewNames = Object.keys(JobViews);
    for(let viewName, i = 0;(viewName = viewNames[i]); i++) {
      const viewClass = JobViews[viewName];
      if (viewClass.matchesJob(job)) return viewClass;
    }
    return null;
  }

}

JobDetails.propTypes = {
  job: PropTypes.object.isRequired
};

const styles = () => ({
  root: {
    width: '100%'
  },
  noJob: {
    textAlign: 'center'
  }
});

const styledJobDetails = withStyles(styles)(JobDetails);
export { styledJobDetails as JobDetails };
