import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {
  Grid,
  Typography,
} from '@material-ui/core';

class ServerExecDeployView extends React.Component {

  static matchesJob(job) {
    return job.command === 'v1.server.exec.deploy';
  }

  render() {
    const { classes, job } = this.props;
    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item sm={3}>
            <Typography variant="subtitle1">Retour d&#39;exécution</Typography>
          </Grid>
          <Grid item sm={9} className={classes.codeContainer}>
            <pre className={classes.code}>{JSON.stringify(JSON.parse(job.return), null, 2)}</pre>
          </Grid>
        </Grid>
        <Grid container>
          <Grid item sm={3}>
            <Typography variant="subtitle1">Code retour</Typography>
          </Grid>
          <Grid item sm={9} className={classes.codeContainer}>
            <pre className={classes.code}>{job.retcode}</pre>
          </Grid>
        </Grid>
      </div>
    );
  }
}

ServerExecDeployView.propTypes = {
  job: PropTypes.object.isRequired
};

const styles = theme => ({
  root: {
    width: '100%'
  },
  codeContainer: {
    width: '100%'
  },
  code: Object.assign({}, theme.code, {
    padding: theme.spacing.unit,
    maxHeight: window.innerHeight * 0.75
  })
});

const styledServerExecDeployView = withStyles(styles)(ServerExecDeployView);
export { styledServerExecDeployView as ServerExecDeployView };
