import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Button } from '@material-ui/core';
import {
  Refresh as RefreshIcon
} from '@material-ui/icons';
import PropTypes from 'prop-types';

class JobsNavRight extends React.Component {

  render() {
    const { classes, onRefresh } = this.props;
    return (
      <div className={classes.root}>
        <Button color="inherit" onClick={onRefresh}>
          <RefreshIcon className={classes.leftIcon} />
          Rafraîchir
        </Button>
      </div>
    );
  }

}

JobsNavRight.propTypes = {
  onRefresh: PropTypes.func.isRequired
};

const styles = (theme) => ({
  leftIcon: {
    marginRight: theme.spacing.unit
  }
});

const styledJobsNavRight = withStyles(styles, { withTheme: true })(JobsNavRight);
export { styledJobsNavRight as JobsNavRight };
