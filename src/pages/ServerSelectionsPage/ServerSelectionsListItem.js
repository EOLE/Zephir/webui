import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router';
import { Route } from 'react-router';
import { NavLink } from 'react-router-dom';

import {
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  ListSubheader,
  IconButton,
  Collapse,
  Divider,
  Card,
  CardContent,
  Typography,
  ButtonBase
} from '@material-ui/core';

import {
  ExpandMore as ExpandMoreIcon,
  ViewModule as ServerSelectionIcon
} from '@material-ui/icons';

import styles from './ServerSelectionsListItem.styles.js';

class ServerSelectionsListItem extends React.Component {

  state = {
    expanded: false
  };

  handleExpandClick() {
    this.setState({ expanded: !this.state.expanded });
  }

  render() {
    const { classes, className, serverselection } = this.props;
    const { expanded } = this.state;

    const ServerSelectionsListItem_Card = () => {
      return (
        <Card className={classes.card}>
          <ButtonBase className={classes.content}
            key={`serverselectionListItem-${serverselection.serverselectionid}`}
            component={NavLink}
            to={`/serverselections/${serverselection.serverselectionid}`}>
            <CardContent style={{textAlign: "center"}}>
              <Typography variant="h5">{serverselection.serverselectionname}</Typography>
              <Typography variant="subtitle1" color="textSecondary">
                {serverselection.serverselectiondescription}
              </Typography>
            </CardContent>
          </ButtonBase>
        </Card>
      );
    };
    const ServerSelectionsListItem_List = () => {
      return (
        <div className={classNames(classes.root, className)}>
          <ListItem
            key={`serverListItem-${serverselection.serverselectionid}`}
            component={NavLink}
            to={`/serverselections/${serverselection.serverselectionid}`}
            activeClassName={classes.activeLink}>
            <ServerSelectionIcon />
            <ListItemText
              inset
              classes={{ primary: classes.primary, secondary: classes.secondary}}
              primary={serverselection.serverselectionname}
              secondary={serverselection.serverselectiondescription}
            />
            {serverselection.children && !!serverselection.children.length &&
            <ListItemSecondaryAction className={classes.secondaryAction}>
              <IconButton
                className={classes.button}
                onClick={this.handleExpandClick.bind(this)}
              >
                <ExpandMoreIcon
                  className={classNames(classes.expand, {
                    [classes.expandOpen]: expanded,
                  })}
                />
              </IconButton>
            </ListItemSecondaryAction>
            }
            {serverselection.children && !!serverselection.children.length &&
              <Collapse in={expanded} timeout="auto" unmountOnExit>
                <Divider />
                <List component="div" disablePadding subheader={
                  serverselection.location
                    ? <ListSubheader>
                      {serverselection.location.name}
                    </ListSubheader>
                    : ''
                }>
                  {serverselection.children.map(child =>
                    <ServerSelectionsListItem
                      key={`serverselection-${child.serverselectionid}`}
                      className={classes.nested}
                      classes={classes}
                      selection={child}
                    />
                  )}
                </List>
                <Divider />
              </Collapse>
            }
          </ListItem>
        </div>
      );
    };
    return (
      <div style={{margin: '0 auto', maxWidth: '1270px'}}>
        <Route exact path="/" component={ServerSelectionsListItem_Card}/>
        <Route path="/serverselections/" component={ServerSelectionsListItem_List}/>
      </div>
    );
  }
}


ServerSelectionsListItem.propTypes = {
  classes: PropTypes.object.isRequired,
};

const routedServerSelectionsListItem = withRouter(ServerSelectionsListItem);
const styledServerSelectionsListItem = withStyles(styles)(routedServerSelectionsListItem);
export { styledServerSelectionsListItem as ServerSelectionsListItem };
