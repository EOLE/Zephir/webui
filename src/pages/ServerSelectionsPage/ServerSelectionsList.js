import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Infinite from 'react-infinite';
import ReactDOM from 'react-dom';
import {
  List,
  Typography,
  Button,
} from '@material-ui/core';
import {
  Loader
} from '../../components';
import {
  Add as AddIcon,
} from '@material-ui/icons';
import { serverselectionActions } from '../../actions';
import { selectServerSelectionsWithFilter, selectAllServerSelections } from '../../selectors';
import { Link } from 'react-router-dom';
import {
  ServerSelectionsListItem
} from './';

const ITEMS_PER_PAGE = 10;

class ServerSelectionsList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      page: 0,
      hasMore: true,
      listHeight: 1,
      isLoading: false,
    };
    this.loadMoreServerSelections = this.loadMoreServerSelections.bind(this);
    this.updateListHeight = this.updateListHeight.bind(this);
  }

  loadMoreServerSelections(){

    const { hasMore, isLoading } = this.state;
    if (!hasMore || isLoading ) return;

    this.setState(prevState => {
      const page = prevState.page + 1;
      this.props.dispatch(serverselectionActions.list({page, itemsPerPage: ITEMS_PER_PAGE}))
        .then(() =>this.setState({ isLoading: false }));
      return { page, isLoading: true };
    });
  }

  render() {
    const { classes, serverselections } = this.props;
    const { isLoading, hasMore } = this.state;

    if (!isLoading && serverselections.length === 0) {
      return (
        <React.Fragment>
          <Typography className={classes.noServerSelection} align='center'>
            Aucun serveur pour le moment.
          </Typography>
          <Typography align='center'>
            <Button component={Link} to={`/serverselections/new`} color="inherit">
              <AddIcon className={classes.leftIcon} />
              Nouveau
            </Button>
          </Typography>
        </React.Fragment>
      );
    }

    return (
      <List className={classes.root}
        ref={el => this.listElement = el}>
        <Infinite
          containerHeight={this.state.listHeight}
          isInfiniteLoading={isLoading && hasMore}
          loadingSpinnerDelegate={this.renderLoader()}
          onInfiniteLoad={this.loadMoreServerSelections}
          infiniteLoadBeginEdgeOffset={this.state.listHeight-64}
          elementHeight={64}>
          {serverselections.map((serverselection) =>
            <ServerSelectionsListItem key={serverselection.serverselectionid} serverselection={serverselection} />
          )}
        </Infinite>
      </List>
    );
  }

  renderLoader() {
    return (
      this.state.hasMore
        ? <Loader />
        : null
    );
  }

  componentDidUpdate() {
    this.updateListHeight();
  }

  updateListHeight() {
    const el = ReactDOM.findDOMNode(this.listElement); // eslint-disable-line
    const listHeight = el && el.clientHeight ? el.clientHeight-1 : 1;
    if (listHeight === this.state.listHeight) return;
    this.setState({listHeight});
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateListHeight);
    this.loadMoreServerSelections();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateListHeight);
  }
}

const styles = () => ({
  root: {
    height: '100%',
    width: '100%',
    position: 'relative',
    paddingTop: 0,
    paddingBottom: 0,
  },
  noServerSelection: {
    paddingTop: '50px'
  }
});

const mapStateToProps = state => {
  const { serverselections } = state;
  return {
    serverselections: serverselections.filter !== '' ? selectServerSelectionsWithFilter(serverselections.byId, serverselections.filter) : selectAllServerSelections(serverselections.byId)
  };
};

const connectedServerSelectionsList = connect(mapStateToProps)(ServerSelectionsList);
const styledServerSelectionsList = withStyles(styles)(connectedServerSelectionsList);
export { styledServerSelectionsList as ServerSelectionsList };
