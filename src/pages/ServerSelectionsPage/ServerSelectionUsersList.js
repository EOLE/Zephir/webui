import React from 'react';
import { connect } from 'react-redux';
import {
    ListItem,
    ListItemText,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Button,
    Tooltip,
    TextField,
    InputLabel,
    Select,
    MenuItem
  } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import {
    AccountCircle as UserIcon,
    Cancel as DeleteUser,
    Edit as EditUser,
} from '@material-ui/icons';
import { serverselectionActions } from '../../actions';
  class ServerSelectionUsersList extends React.Component {

    constructor(props){
        super(props);
        this.state = {
          selrole: this.props.role,
          deleteuserDialogOpen: false,
          edituserDialogOpen: false,
          isDeletingServerSelectionUser: false,
          isOverIcon:false,
        };
        this.handleOnEnterUserItem = this.handleOnEnterUserItem.bind(this)
        this.handleOnLeaveUserItem = this.handleOnLeaveUserItem.bind(this)
        this.handleDeleteUserDialogOpenClick = this.handleDeleteUserDialogOpenClick.bind(this)
        this.handleDeleteUserDialogCloseClick = this.handleDeleteUserDialogCloseClick.bind(this)
        this.handleEditUserDialogOpenClick = this.handleEditUserDialogOpenClick.bind(this)
        this.handleEditUserDialogCloseClick = this.handleEditUserDialogCloseClick.bind(this)
    }
    handleOnEnterUserItem(){
      this.setState({ isOverIcon: true });
    }
    handleOnLeaveUserItem(){
      this.setState({ isOverIcon: false });
    }
    handleDeleteUserDialogOpenClick() {
      this.setState({ deleteuserDialogOpen: true })
    }
    handleDeleteUserDialogCloseClick() {
      this.setState({ deleteuserDialogOpen: false })
    }
    handleEditUserDialogOpenClick() {
      this.setState({ edituserDialogOpen: true })
    }
    handleEditUserDialogCloseClick() {
      this.setState({ edituserDialogOpen: false })
    }
    handleConfirmDeleteUserClick(){
      this.props.dispatch(serverselectionActions.user_remove(this.props.serverselection.serverselectionid, this.props.user))
    }
    handleConfirmEditUserClick(){
      this.props.dispatch(serverselectionActions.user_update(this.props.serverselection.serverselectionid, this.props.user, this.state.selrole))
      .finally(() => this.setState({ edituserDialogOpen: false }))
    }
    handleSelectRole = event => {
      this.setState({ [event.target.name]: event.target.value });
    };
    render() {
        const { user, theme, role, classes } = this.props;
        return(
            <ul>
                <ListItem
                  key={user}
                  onMouseEnter={this.handleOnEnterUserItem}
                  onMouseLeave={this.handleOnLeaveUserItem}
                >
                    <UserIcon />
                    <ListItemText
                      primary={user}
                      secondary={role}
                    />
                      <Tooltip title="Editer">
                      <EditUser className={classes.iconuser} onClick={this.handleEditUserDialogOpenClick} />
                      </Tooltip>
                      <Tooltip title="Supprimer">
                        <DeleteUser className={classes.iconuser} onClick={this.handleDeleteUserDialogOpenClick} />
                      </Tooltip>

                </ListItem>
                <Dialog
                open={this.state.deleteuserDialogOpen}
                onClose={this.handleDeleteUserDialogCloseClick}
                aria-labelledby="responsive-dialog-title-user-delete"
                >
                <DialogTitle id="responsive-dialog-title-user-delete">{`Retirer l'utilisateur "${user}" - ${role} ?`}</DialogTitle>
                <DialogContent>
                  <DialogContentText>
                    L'utilisateur sera définitivement retiré de la sélection, êtes-vous sûr de
                    vouloir poursuivre ?
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button onClick={this.handleDeleteUserDialogCloseClick.bind(this)}>
                    Annuler
                  </Button>
                  <Button onClick={this.handleConfirmDeleteUserClick.bind(this)}  autoFocus>
                    Retirer
                  </Button>
                </DialogActions>
              </Dialog>
              <Dialog
              open={this.state.edituserDialogOpen}
              onClose={this.handleEditUserDialogCloseClick}
              aria-labelledby="responsive-dialog-title-user-edit"
              >
              <DialogTitle id="responsive-dialog-title-user-edit">{`Editer l'utilisateur "${user}" - ${role} ?`}</DialogTitle>
              <DialogContent>
                <DialogContentText>
                    Saisissez l'UID de l'utilisateur et le rôle à ajouter à la sélection
                  </DialogContentText>
                  <TextField
                    id="User"
                    type="text"
                    label="UID de l'utilisateur"
                    fullWidth
                    disabled
                    style={{padding: '10px 0px'}}
                    value={user}
                  />
                  <InputLabel htmlFor="seluser">Role</InputLabel>
                  <Select
                    onChange={this.handleSelectRole}
                    value={this.state.selrole}
                    fullWidth
                    inputProps={{
                      name: 'selrole',
                      id: 'selrole',
                    }}
                    classes={{classes}}
                  >
                    <MenuItem value='admin'>Admin</MenuItem>
                    <MenuItem value='manager'>Manager</MenuItem>
                    <MenuItem value='viewer'>Viewer</MenuItem>
                    <MenuItem value='owner'>Owner</MenuItem>
                </Select>
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleEditUserDialogCloseClick.bind(this)}>
                  Annuler
                </Button>
                <Button onClick={this.handleConfirmEditUserClick.bind(this)} autoFocus>
                  Modifier
                </Button>
              </DialogActions>
            </Dialog>
            </ul>


        )
    }
  }
  const styles = () => ({
    root: {
      height: '100%',
      //zIndex: 3
    },
    iconuser: {
      margin: '5px'
    }
  });
const connectedServerSelectionUsersList = connect()(ServerSelectionUsersList);
const styledServerSelectionUsersList= withStyles(styles, { withTheme: true })(connectedServerSelectionUsersList);
export { styledServerSelectionUsersList as ServerSelectionUsersList };
