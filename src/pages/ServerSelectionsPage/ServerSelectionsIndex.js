import React from 'react';
import Media from 'react-media';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router';

import SearchBar from 'material-ui-search-bar';

import {
  Search as SearchIcon,
  Close as CloseIcon,
  FormatAlignJustify as ListIcon,
} from '@material-ui/icons';

import {
  VSplit,
  Navbar
} from '../../components';

import {
  ServerSelectionsList
} from './';

import styles from './ServerSelectionsIndex.styles.js';

class ServerSelectionsIndex extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      activeTab: 'list'
    };
    this.tabs = [
      // TODO Réactiver les vues arbres/carto lorsque le backend gérera les
      // zones géographiques
      { value: 'list', icon: <ListIcon />, component: ServerSelectionsList },
    ];
    this.handleTabChange = this.handleTabChange.bind(this);
    this.handleTabChangeIndex = this.handleTabChangeIndex.bind(this);
    this.handleFilterChange = this.handleFilterChange.bind(this);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if(nextProps.location.query
      && nextProps.location.query.tab
      && nextProps.location.query.tab !== prevState.activeTab)
    {
      return { activeTab: nextProps.location.query.tab };
    }
    return null;
  }

  handleExpandClick() {
    this.props.onExpand(!this.state.expanded);
    this.setState({ expanded: !this.state.expanded });
    setTimeout(() => window.dispatchEvent(new Event('resize')), 200);
  }

  handleFilterChange(filter) {
    this.props.onFilterChange(filter);
  }

  handleTabChange(e, activeTab) {
    this.setState({ activeTab });
  }

  handleTabChangeIndex = index => {
    this.setState({ activeTab: this.tabs[index].value });
  };

  render(){

    const { classes, theme, selectedServerSelection } = this.props;
    const { expanded, activeTab } = this.state;
    return (
      <Media query={{ minWidth: theme.breakpoints.values.lg }}>
        {() =>
          <div className={classes.root}>
            <Navbar classes={{ appBar: classes.navbar, toolbar: classes.toolbar }}>
              <VSplit
                style={{ height: 'auto', width: 0 /* Flex Hack */ }}
                leftProps={expanded ? { sm: 12, md: 12, lg: 6 } : { sm: 12, md: 12, lg: 12 }}
                left={
                  <div className={classes.searchBarContainer}>
                    <SearchBar
                      placeholder="Filtrer les serveurs"
                      classes={{
                        root: classes.searchBar,
                        input: classes.searchBarInput
                      }}
                      style={styles(theme).searchBar /* Hack: no support for theme */}
                      onChange={this.handleFilterChange}
                      onRequestSearch={this.handleFilterChange}
                      searchIcon={<SearchIcon className={classes.searchBarIcon} />}
                      closeIcon={<CloseIcon className={classes.searchBarIcon} />}
                    />
                  </div>
                }
              />
            </Navbar>
            <main
              className={classNames(classes.main, {
                [classes.mainOpen]: expanded,
              })}
            >
              {this.tabs.map(({ value, component: Component }) =>
                <div key={`${value}-container`} dir={theme.direction} className={classes.tabContainer}>
                  <Component selectedServerSelection={selectedServerSelection} currentTab={activeTab} />
                </div>
              )}
            </main>
          </div>
        }
      </Media>
    );
  }
}

ServerSelectionsIndex.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  onFilterChange: PropTypes.func.isRequired,
  selectedServerSelection: PropTypes.string
};

const mapStateToProps = ({ ui }) => ({ ui });

const styledServerSelectionsIndex = withStyles(styles, { withTheme: true })(ServerSelectionsIndex);
const routedServerSelectionsIndex = withRouter(styledServerSelectionsIndex);
const connectedServerSelectionsIndex = connect(mapStateToProps)(routedServerSelectionsIndex);
export { connectedServerSelectionsIndex as ServerSelectionsIndex };
