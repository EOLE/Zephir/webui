import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import {
  Grid,
  TextField,
  CircularProgress,
  Typography,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  InputLabel
} from '@material-ui/core';

import {
  Check as CheckIcon,
  ExpandMore
} from '@material-ui/icons';

import {
  Page,
  ResponsiveButton,
  withForm,
  ServerPicker,
  ServerModelSelector
} from '../../components';

import {
  serverselectionActions
} from '../../actions';
import { selectServerSelectionById } from '../../selectors';

class ServerSelectionForm extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      isSaving: false,
      dismount: false,
    };
    this.handleSrvList = this.handleSrvList.bind(this);


  }
  componentDidUpdate() {
    const { form } = this.props;
    if (form.submitted && form.validated) {
      this.setState({ dismount: true })
      this.saveServerSelection();
    }

  }

  saveServerSelection() {
    this.setState({ isSaving: true }, () => {
      const { form, serverselection } = this.props;
      const { serverList } = this.state;
      let savePromise;

      if (serverselection) { // Modification d'un serveur existant
        savePromise = this.props.dispatch(serverselectionActions.update({
          ...serverselection,
          ...form.data
        }));

      } else { // Création d'une nouvelle sélection de serveurs
        savePromise = this.props.dispatch(serverselectionActions.create(form.data));
      }
      savePromise.then((serverselection) => {
          if (serverList){
            savePromise = this.props.dispatch(serverselectionActions.server_set({...form.data, ...serverselection}, serverList))
          }
        })
        savePromise.then((serverselection) => {
          this.props.history.push(`/serverselections/${serverselection.serverselectionid}`);
        })
        .catch(err => {
          form.reset();
          this.setState({isSaving: false});
          throw err;
        });
    });
  }

  render() {
    const { classes, theme, inner, style, sm, form, serverselection, isLoading, history } = this.props;
    const { isSaving, dismount } = this.state;
    const { serverselectionname, serverselectiondescription } = serverselection || {
      serverselectionname: '',
      serverselectiondescription: '',
      serverselectionmodelid: -1,
      requete: ''
    };
    history.listen((location, action) => {
        this.setState({ dismount: true })
    });

    return (
      (!isLoading || form.submitted) &&
      <Page
        title={serverselection
          ? `Modifier ${serverselection.serverselectionname}`
          : 'Nouvelle Sélection de Serveurs'
        }
        className={classes.root}
        style={style}
        inner={inner}
        backLink={serverselection && serverselection.serverselectionid ? `/serverselections/${serverselection.serverselectionid}` : '/serverselections'}
      >
        <div className={classes.content}>
          <form name="form" onSubmit={this.handleSubmit}>
            <Grid container spacing={theme.spacing.unit * 2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  name="serverselectionname"
                  label="Nom de la sélection"
                  className={classes.textField}
                  value={form.field("serverselectionname", serverselectionname)}
                  onChange={form.onChange}
                  error={!!form.errors.serverselectionname}
                  disabled={isLoading || isSaving}
                  helperText={form.errors.serverselectionname}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  name="serverselectiondescription"
                  label="Description de la sélection"
                  className={classes.textField}
                  value={form.field("serverselectiondescription", serverselectiondescription)}
                  onChange={form.onChange}
                  error={!!form.errors.serverselectiondescription}
                  disabled={isLoading || isSaving}
                  helperText={form.errors.serverselectiondescription}
                  fullWidth
                />
              </Grid>
            </Grid>
            {/* Work in progress
             <ExpansionPanel style={{margin: '20px 10px'}}>
                <ExpansionPanelSummary expandIcon={<ExpandMore />}>
                  <Typography variant='subtitle1'>Filtres serveurs</Typography>
                </ExpansionPanelSummary>
                  <ExpansionPanelDetails>
                    <Grid item xs={12} sm={6}>
                        <InputLabel htmlFor="serverfilter">Filtre de serveur</InputLabel>
                        <TextField
                            name="serverfilter"
                            className={classes.textField}
                            onChange={form.onChange}
                            disabled={isLoading || isSaving}
                            fullWidth
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <InputLabel htmlFor="servermodelefilter">Modèle de serveur</InputLabel>
                        <ServerModelSelector
                            fullWidth
                            multi='true'
                            name="servermodelefilter"
                            label="Filtre de modèles de serveur"
                            className={classes.textField}
                         />
                    </Grid>

                  </ExpansionPanelDetails>

              </ExpansionPanel>*/}
              <Grid container spacing={theme.spacing.unit * 2}>
              {!dismount && <ServerPicker handleSrvList={this.handleSrvList} />}
              </Grid>

            <ResponsiveButton
              sm={sm}
              onClick={form.onSubmit}
              style={{padding: '20px 0px', margin: '20px 0px'}}
              title='Enregistrer'
              color="primary"
              disabled={isLoading || isSaving}
              icon={form.submitted && isLoading
                ? (props =>
                  <CircularProgress
                    {...props}
                    color={inner ? 'primary' : 'inherit'}
                    style={{ marginBottom: inner ? 0 : theme.spacing.unit }}
                    size={20}
                  />
                )
                : CheckIcon
              }
            />
            {/*bouton caché permettant de soumettre le formulaire via la touche entrée*/}
            <input
              type="submit"
              style={{display: 'none'}}
              onClick={form.onSubmit}
            />

          </form>
        </div>
      </Page>
    );
  }
  handleSrvList(serverList){
    this.setState({
      serverList: serverList.join(',')
    })

  }
}

const styles = theme => ({
  root: {
    width: '100%',
    height: '100%'
  },
  content: {
    padding: theme.spacing.unit * 2
  },
  formControl: {
    '& > div': {
      width: '100%'
    }
  },
  textField: {
  },
  passwordField: {
    //lineHeight: 3,
    height: 'auto'
  }
});

ServerSelectionForm.propTypes = {
  theme: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,
  serverselection: PropTypes.object
};

const managedForm = withForm({
  serverselectionname: (value, formData, submitting) => {
    if (submitting && !value) return "Le nom de la sélection est obligatoire.";
  },
  serverselectiondescription: (value, formData, submitting) => {
    if (submitting && !value) return "La description de la sélection est obligatoire.";
  },
})(ServerSelectionForm);

const mapStateToProps = ({ serverselections }, { match }) => {

  let serverselectionId = match.params.id;
  if (serverselectionId === "new") return { serverselection: null };

  serverselectionId =  parseInt(serverselectionId, 10);
  const props =  {
    serverselectionId: serverselectionId,
    serverselection: selectServerSelectionById(serverselections.byId, serverselectionId)
  };

  return props;

};

const connectedServerSelectionForm = connect(mapStateToProps)(managedForm);
const styledServerSelectionForm = withStyles(styles, { withTheme: true })(connectedServerSelectionForm);
const routedServerSelectionForm = withRouter(styledServerSelectionForm);
export { routedServerSelectionForm as ServerSelectionForm };
