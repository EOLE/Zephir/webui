import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Infinite from 'react-infinite';
import { withRouter } from 'react-router';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import {
  List,
  ListItem,
  ListItemText,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  withMobileDialog,
  Typography,
  GridList,
  GridListTile,
  Checkbox,
  TextField,
  MenuItem,
  Select,
  InputLabel
} from '@material-ui/core';
import {
  ServerSelectionUsersList
} from './';
import {
  Edit as EditIcon,
  Delete as DeleteIcon,
  PersonAdd as AddUserIcon
} from '@material-ui/icons';
import {
  selectServerSelectionById,
} from '../../selectors';
import {
  Page,
  Loader,
  ResponsiveButton,
} from '../../components';
import { ServersListItem } from '../ServersPage';
import { selectServerById } from '../../selectors';
import { serverselectionActions, serverActions } from '../../actions';
class ServerSelectionsDetail extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      seluser: '',
      selrole:'',
      deleteDialogOpen: false,
      isDeletingServerSelection: false,
      isAddindUserServerSelection: false,
      adduserDialogOpen: false
    };
  }

  handleDeleteDialogOpenClick() {
    this.setState({ deleteDialogOpen: true });
  }
  handleAddUserDialogOpenClick() {
    this.setState({ adduserDialogOpen: true });
  }

  handleDeleteDialogCloseClick() {
    this.setState({ deleteDialogOpen: false });
  }
  handleAddUserDialogCloseClick() {
    this.setState({ adduserDialogOpen: false });
  }

  handleConfirmDeleteClick() {
    const { serverselection } = this.props;
    this.setState({isDeletingServerSelection: true}, () => {
      this.props.dispatch(serverselectionActions.delete(serverselection.serverselectionid, serverselection.serverselectionname))
        .finally(() => this.setState({ deleteDialogOpen: false, isDeletingServerSelection: false }))
        .then(() => this.props.dispatch(serverselectionActions.list()))
        .then(() => this.props.history.push(`/serverselections`));
    });
  }
  handleConfirmAddUserClick() {
    const { serverselection } = this.props;
    this.setState({isAddindUserServerSelection: true}, () => {
      this.props.dispatch(serverselectionActions.user_add(serverselection.serverselectionid, this.state.seluser, this.state.selrole))
        .finally(() => this.setState({ adduserDialogOpen: false, isAddindUserServerSelection: false }))
        .then(() => this.props.dispatch(serverselectionActions.list()))
        .then(() => this.props.history.push(`/serverselections/${serverselection.serverselectionid}`));
    });
  }
  handleSelectRole = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
  handleUserUID = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
  componentDidMount() {
    this.setState(() => {
      this.props.dispatch(serverActions.list())
      this.props.dispatch(serverselectionActions.list())
    })
  }
  render(){
    const { classes, theme, fullScreen, inner, sm, isLoading, serverselection, servers} = this.props;

    return (
      <Page
        title={serverselection && serverselection.serverselectionname}
        loading={isLoading}
        className={classes.root}
        inner={inner}
        backLink={`/serverselections`}
        navRight={
          serverselection &&
          <div>
            <ResponsiveButton
              sm={sm}
              component={Link}
              to={`/serverselections/${serverselection.serverselectionid}/edit`}
              icon={EditIcon}
              title="Modifier"
            />
            <ResponsiveButton
              sm={sm}
              onClick={this.handleDeleteDialogOpenClick.bind(this)}
              icon={DeleteIcon}
              title="Supprimer"
            />
          </div>
        }
      >
        {serverselection && !serverselection.isLoading &&
          <div style={{margin: "0 10px"}}>
            <GridList cellHeight={'auto'} className={classes.gridList}>
              <GridListTile cols={1}>
                <List dense className={classes.card}>
                  <Typography variant="subtitle1">
                    Informations Générales
                  </Typography>
                  <ListItem divider >
                    <ListItemText primary="Nom de la Sélection de serveur" secondary={serverselection.serverselectionname} />
                  </ListItem>
                  <ListItem divider >
                    <ListItemText primary="Description" secondary={serverselection.serverselectiondescription} />
                  </ListItem>
                  <ListItem divider >
                    <ListItemText primary="Dynamique" secondary={serverselection.dynamique} />
                    <Checkbox
                      checked={serverselection.dynamique}
                    />
                  </ListItem>
                </List>
                <Typography variant="subtitle1">
                  Serveurs de la sélection :
                </Typography>
                {(!servers.isLoading && serverselection.serverselectionserversid.length > 0 && this.renderListServer(serverselection, servers))
                || <Typography variant="subtitle1">Aucun serveur dans la sélection de serveur</Typography> }
              </GridListTile>
              <GridListTile >
                <Typography variant="subtitle1">
                  Utilisateurs
                </Typography>
                {Object.keys(serverselection.serverselectionusers).map(user =>
                  <ServerSelectionUsersList key={`${user}-user`} user={user}  role={serverselection.serverselectionusers[user]} serverselection={serverselection} />
                )}
                <ResponsiveButton
                  sm={sm}
                  onClick={this.handleAddUserDialogOpenClick.bind(this)}
                  icon={AddUserIcon}
                  title="Ajouter un utilisateur"
                />
              </GridListTile>

            </GridList>

          </div>
        }
        {isLoading && <Loader />}
        <Dialog
          fullScreen={fullScreen}
          open={this.state.deleteDialogOpen}
          onClose={this.handleDeleteDialogCloseClick.bind(this)}
          aria-labelledby="responsive-dialog-title-delete"
        >
          <DialogTitle id="responsive-dialog-title-delete">{"Êtes-vous sûr ?"}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              La sélection sera définitivement supprimé, êtes-vous sûr de
              vouloir poursuivre ?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleDeleteDialogCloseClick.bind(this)}>
              Annuler
            </Button>
            <Button onClick={this.handleConfirmDeleteClick.bind(this)} style={{color: theme.palette.error.main }} autoFocus>
              Supprimer
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          fullScreen={fullScreen}
          maxWidth='sm'
          open={this.state.adduserDialogOpen}
          onClose={this.handleAddUserDialogCloseClick.bind(this)}
          aria-labelledby="responsive-dialog-title-adduser"
        >
        <DialogTitle id="responsive-dialog-title-adduser">{"Ajouter un utilisateur à la sélection de serveurs"}</DialogTitle>
          <DialogContent>
          <DialogContentText>
              Saisissez l'UID de l'utilisateur et le rôle à ajouter à la sélection
            </DialogContentText>
            <TextField
              id="User"
              type="text"
              label="UID de l'utilisateur"
              fullWidth
              onChange={this.handleUserUID}
              style={{padding: '10px 0px'}}
              inputProps={{
                name: 'seluser',
                id: 'seluser',
              }}
            />
            <InputLabel htmlFor="seluser">Role</InputLabel>
            <Select
              onChange={this.handleSelectRole}
              value={this.state.selrole}
              displayEmpty
              fullWidth
              inputProps={{
                name: 'selrole',
                id: 'selrole',
              }}
              classes={classes}
            >
              <MenuItem value='admin'>Admin</MenuItem>
              <MenuItem value='manager'>Manager</MenuItem>
              <MenuItem value='viewer'>Viewer</MenuItem>
              <MenuItem value='owner'>Owner</MenuItem>
          </Select>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleAddUserDialogCloseClick.bind(this)}>
              Annuler
            </Button>
            <Button onClick={this.handleConfirmAddUserClick.bind(this)} style={{color: theme.palette.error.main }} autoFocus>
              Ajouter
            </Button>
          </DialogActions>
        </Dialog>
      </Page>
    );
  }
  renderListServer(serverselection, servers){
    return (
      <Infinite
        containerHeight={350}
        elementHeight={57}>
          {serverselection.serverselectionserversid.map((serverId) =>
            <ServersListItem key={`${serverId}-server`} server={selectServerById(servers.byId, serverId)} serverselection={serverselection.serverselectionid} />
          )}
      </Infinite>
    )
  }
}


const styles = theme => ({
  root: {
    height: '100%',
    //zIndex: 3
  },
  container: {
    height: 'auto',
    [theme.breakpoints.up('md')]: {
      height: '100%',
      overflow: 'hidden'
    }
  },
  col: {
    backgroundColor: theme.palette.background.paper,
    maxHeight: '100%',
    overflowY: 'auto'
  },
  listSubheader: {
    backgroundColor: theme.palette.background.paper
  }
});

ServerSelectionsDetail.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  fullScreen: PropTypes.bool.isRequired,
};

function mapStateToProps({ apps, serverselections, servers }, { match }) {
  const serverselectionId = parseInt(match.params.id, 10);
  const serverselection = selectServerSelectionById(serverselections.byId, serverselectionId);

  return {
    availableApps: apps.available,
    serverselection,
    servers
  };
}

const connectedServerSelectionsDetail = connect(mapStateToProps)(ServerSelectionsDetail);
const styledServerSelectionsDetail = withStyles(styles, { withTheme: true })(connectedServerSelectionsDetail);
const routedServerSelectionsDetail = withRouter(styledServerSelectionsDetail);
const mobiledServerSelectionsDetail = withMobileDialog()(routedServerSelectionsDetail);
export { mobiledServerSelectionsDetail as ServerSelectionsDetail };
