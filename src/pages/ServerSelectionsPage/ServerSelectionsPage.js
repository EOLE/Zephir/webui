/* globals Promise */
import React from 'react';
import { Route, withRouter } from 'react-router';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import {
  Button,
} from '@material-ui/core';
import {
  Add as AddIcon,
  Assessment as AssessmentIcon
} from '@material-ui/icons';
import {
  Redirect,
  Page,
  FadingSwitch,
  VSplit,
  RouteDataLoader,
} from '../../components';
import {
  ServerSelectionsDashboard,
  ServerSelectionsDetail,
  ServerSelectionForm,
  ServerSelectionsIndex,
} from './';

import { serverselectionActions } from '../../actions';

class ServerSelectionsPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      serverselectionsListExpanded: false
    };
    this.loadRouteServerSelection = this.loadRouteServerSelection.bind(this);
    this.handleFilterChange = this.handleFilterChange.bind(this);
    this.handleServerSelectionListExpandClick = this.handleServerSelectionListExpandClick.bind(this);
  }

  render() {
    const { serverselections, classes } = this.props;
    const { serverselectionsListExpanded } = this.state;
    return (
          <Page
            className={classes.page}
            title='Gestion des Sélections de serveur'
            loading={serverselections.itemsLoading}
            navRight={
              <div>
                <Button component={Link} to={`/serverselections/dashboard`} color="inherit">
                  <AssessmentIcon className={classes.leftIcon} />
                  Vue d&#39;ensemble
                </Button>
                <Button component={Link} to={`/serverselections/new`} color="inherit">
                  <AddIcon className={classes.leftIcon} />
                  Nouveau
                </Button>
              </div>
            }
          >
            <VSplit
              left={
                <Route path="/serverselections/:id?" render={props => this.renderServerSelectionIndex(props) } />
              }
              right={
                <FadingSwitch>
                  <Route path="/serverselections/dashboard" render={() => <ServerSelectionsDashboard inner={true} />} />
                  <Route path="/serverselections/new/" render={() => <ServerSelectionForm inner={true} />} />
                  <Route path="/serverselections/:id/edit" render={(routeProps) => {
                    return(
                      <RouteDataLoader loadData={this.loadRouteServerSelection} {...routeProps}>
                        <ServerSelectionForm inner={true} sm={serverselectionsListExpanded} />
                      </RouteDataLoader>
                    );
                  }}/>
                  <Route path="/serverselections/:id" render={(routeProps) => {
                    return (
                      <RouteDataLoader loadData={this.loadRouteServerSelection} {...routeProps}>
                        <ServerSelectionsDetail inner={true} sm={serverselectionsListExpanded} />
                      </RouteDataLoader>
                    );
                  }} />
                  <Redirect from="/serverselections" to="/serverselections/dashboard" />
                </FadingSwitch>
              }
            />
          </Page>

    );
  }
  renderServerSelectionIndex(routeProps) {
    const { match } = routeProps;
    return (
      <ServerSelectionsIndex
        selectedServerSelection={match.params.id}
        onFilterChange={this.handleFilterChange}
        onExpand={this.handleServerSelectionListExpandClick}
       />
    );
  }
  handleServerSelectionListExpandClick(expanded) {
    this.setState({
      serverselectionsListExpanded: expanded
    });
  }

  handleFilterChange(filter) {
    this.props.dispatch(serverselectionActions.updateLocalFilter(filter));
  }

  loadRouteServerSelection(match, dispatch) {
    const serverselectionId = parseInt(match.params.id, 10);
    // On s'assure que la sélection a été chargé

    return dispatch(serverselectionActions.describe(serverselectionId));

    //#TODO précharger la liste des serveur de la sélection
  }

}

const styles = (theme) => ({
  root: {
  },
  animatedRoute: {
    height: '100%',
    width: '100%',
    position: 'absolute'
  },
  switch: {
    height: '100%',
    width: '100%',
    position: 'absolute'
  },
  padding: {
    padding: theme.spacing.unit * 2
  },
  toolbar: {
    backgroundColor: theme.palette.primary.dark,
  },
  toolbarTitle: {
    color: theme.palette.primary.contrastText
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  }
});

function mapStateToProps(state) {
  const { serverselections } = state;
  return {
    serverselections
  };
}

const connectedServerSelectionsPage = connect(mapStateToProps)(ServerSelectionsPage);
const styledServerSelectionsPage = withStyles(styles, { withTheme: true })(connectedServerSelectionsPage);
const routedServerSelectionspage = withRouter(styledServerSelectionsPage);
export { routedServerSelectionspage as ServerSelectionsPage };
