export const serverTemplateConstants = {
  GETALL_REQUEST: 'SERVER_TEMPLATES_GETALL_REQUEST',
  GETALL_SUCCESS: 'SERVER_TEMPLATES_GETALL_SUCCESS',
  GETALL_FAILURE: 'SERVER_TEMPLATES_GETALL_FAILURE',

  CREATE_REQUEST: 'SERVER_TEMPLATES_REGISTER_REQUEST',
  CREATE_SUCCESS: 'SERVER_TEMPLATES_REGISTER_SUCCESS',
  CREATE_FAILURE: 'SERVER_TEMPLATES_REGISTER_FAILURE',

  DELETE_REQUEST: 'SERVER_TEMPLATES_DELETE_REQUEST',
  DELETE_SUCCESS: 'SERVER_TEMPLATES_DELETE_SUCCESS',
  DELETE_FAILURE: 'SERVER_TEMPLATES_DELETE_FAILURE'
};
