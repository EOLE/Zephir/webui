/* eslint-env jest, node */
let localStorageItems = {};
const localStorageMock = {
  getItem: key => localStorageItems[key],
  setItem: (key, value) => localStorageItems[key] = value,
  clear: () => localStorageItems = {}
};
global.localStorage = localStorageMock;