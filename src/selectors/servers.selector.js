export function selectServerById(serversIndex, serverId) {
  return serversIndex[serverId];
}

export function selectAllServers(serversIndex) {
  return Object.keys(serversIndex).map(serverId => serversIndex[serverId]);
}

export function selectServersWithFilter(serversIndex, filter) {
  const re = new RegExp(filter, 'i');
  const props = [
    'servername',
    'serverdescription'
  ];
  return Object.keys(serversIndex)
    .reduce((result, serverId) => {
      const server = serversIndex[serverId];
      if ( props.some(p => re.test(server[p])) ) result.push(server);
      return result;
    }, []);
}