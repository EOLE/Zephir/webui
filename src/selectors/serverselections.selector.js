export function selectServerSelectionById(serverselectionsIndex, serverselectionId) {
    return serverselectionsIndex[serverselectionId];
  }

export function selectAllServerSelections(serverselectionsIndex) {
  return Object.keys(serverselectionsIndex).map(serverselectionId => serverselectionsIndex[serverselectionId]);
}
export function selectServerSelectionsWithFilter(serverselectionsIndex, filter) {
  const re = new RegExp(filter, 'i');
  const props = [
    'serverselectionname',
    'serverselectiondescription'
  ];
  return Object.keys(serverselectionsIndex)
    .reduce((result, serverselectionId) => {
      const serverselection = serverselectionsIndex[serverselectionId];
      if ( props.some(p => re.test(serverselection[p])) ) result.push(serverselection);
      return result;
    }, []);
}