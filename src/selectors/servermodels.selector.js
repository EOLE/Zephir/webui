export function selectServermodelById(servermodelsIndex, servermodelId) {
  return servermodelsIndex[servermodelId];
}

export function selectAllServerModels(servermodelsIndex) {
  return Object.keys(servermodelsIndex).map(servermodelId => servermodelsIndex[servermodelId]);
}
export function selectServerModelsWithFilter(servermodelsIndex, filter) {
  const re = new RegExp(filter, 'i');
  const props = [
    'servermodelname',
    'servermodeldescription',
    'subreleasename'
  ];
  return Object.keys(servermodelsIndex)
    .reduce((result, servermodelId) => {
      const servermodel = servermodelsIndex[servermodelId];
      if ( props.some(p => re.test(servermodel.model[p])) ) result.push(servermodel);
      return result;

    }, []);
}