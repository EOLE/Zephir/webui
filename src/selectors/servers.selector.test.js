/* eslint-env jest */
import {
  selectServersWithFilter,
  selectServerById
} from './servers.selector';

describe('servers selectors', () => {

  const serversById = {
    1: {
      automation: 'salt',
      servermodelid: 1,
      serverdescription: 'base',
      servername: 'base',
      serverid: 1
    },
    2: {
      automation: 'salt',
      servermodelid: 2,
      serverdescription: 'horus',
      servername: 'horus',
      serverid: 2
    }
  };


  it('should select a server by its id', () => {
    const result = selectServerById(serversById, 1);
    expect(result).toMatchObject(serversById[1]);
  });

  it('should select servers with the given filter', () => {
    const results = selectServersWithFilter(serversById, 'base');
    expect(results.length).toEqual(1);
    expect(results[0]).toMatchObject(serversById[1]);
  });

});