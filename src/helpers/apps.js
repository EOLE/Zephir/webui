/* globals Promise */
import * as Apps from '../apps';
import PropTypes from 'prop-types';

export function getAvailableApps() {
  return Object.keys(Apps).reduce((apps, appName) => {
    const appClass = Apps[appName];
    apps.push({...appClass.getAppMetadata(), name: appName});
    return apps;
  }, []);
}

export function getInstanceLabel(instance) {
  const appClass = getAppClass(instance.appName);
  return appClass.getInstanceLabel(instance);
}

export function getAppMetadata(appName) {
  const appClass = getAppClass(appName);
  return appClass.getAppMetadata();
}

export function getAppClass(appName) {
  if(!(appName in Apps)) {
    const errMessage = `Impossible de trouver d'implémentation pour l'application "${appName}"`;
    throw new Error(errMessage);
  }
  return Apps[appName];
}

export function openAppInstance(instance, opened) {
  return Promise.resolve()
    .then(() => {
      const appClass = getAppClass(instance.appName);
      const optsPropTypes = appClass.instanceOptsPropTypes;
      PropTypes.checkPropTypes(optsPropTypes, instance.opts, 'instance.opts', instance.appName);
      return appClass.open(instance, opened);
    });
}

export function closeAppInstance(instance) {
  return Promise.resolve()
    .then(() => {
      const appClass = getAppClass(instance.appName);
      return appClass.close(instance);
    });
}