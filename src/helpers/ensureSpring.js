import spring from 'react-motion/lib/spring';

export function ensureSpring(styles) {
  return Object.keys(styles).reduce((acc, key) => {
    const value = styles[key];
    acc[key] = typeof value === 'number' ? spring(value, {
      //stiffness: 10, damping: 40
    }) : value;
    return acc;
  }, {});
}
