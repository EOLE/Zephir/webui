/* globals window */
import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from '../reducers';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

let reduxMiddlewares = [
  thunkMiddleware
];

if (process.env.NODE_ENV !== 'production') {
  const createLogger = require('redux-logger').createLogger;
  const loggerMiddleware = createLogger({
    collapsed: true,
    diff: true
  });
  reduxMiddlewares.push(loggerMiddleware);
}

export const store = createStore(
  rootReducer,
  composeEnhancers(
    applyMiddleware(...reduxMiddlewares)
  )
);
