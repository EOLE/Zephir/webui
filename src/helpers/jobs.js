export function convertJIDToDate(jid) {

  const d = {
    year: jid.slice(0, 4),
    month: jid.slice(4, 6),
    day: jid.slice(6, 8),
    hours: jid.slice(8, 10),
    minutes: jid.slice(10, 12),
    seconds: jid.slice(12, 14)
  };

  const date = new Date();
  date.setUTCFullYear(d.year);
  date.setUTCMonth(d.month - 1);
  date.setUTCDate(d.day);
  date.setUTCHours(d.hours);
  date.setUTCMinutes(d.minutes);
  date.setUTCSeconds(d.seconds);

  return date;

}
