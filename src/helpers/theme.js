import { createMuiTheme } from '@material-ui/core/styles';
import deepPurple from '@material-ui/core/colors/deepPurple';
import yellow from '@material-ui/core/colors/yellow';
import red from '@material-ui/core/colors/red';
import green from '@material-ui/core/colors/green';
import orange from '@material-ui/core/colors/orange';

export const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  palette: {
    primary: { main: deepPurple[800] },
    secondary: { main: yellow[700] },
    error: { main: red[800], light: red[600] },
    success: { main: green[800], light: green[600] },
    warning: { main: orange[800], light: orange[600] }
  },
  mixins: {
    sidebar: {
      width: 240
    }
  },
  code: {
    overflow: 'auto',
    backgroundColor: 'hsl(0, 0%, 10%)',
    color: '#e7e7e7',
    fontFamily: 'monospace',
    whiteSpace: 'pre',
    wordBreak: 'keep-all',
    fontSize: '1.2em',
    marginTop: 0,
  }
});
