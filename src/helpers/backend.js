/* globals Promise */

import { history } from './history';


export function defaultOptions(opts) {

  const authToken = sessionStorage.getItem('kc_token');
  const myHeaders = new Headers();
  myHeaders.append("Authorization", "Bearer " + authToken);

  const defaultRequestOptions = {
    redirect: 'manual',
    method: 'POST',
    headers: myHeaders,
    credentials: 'include',
    destination: 'object',
    mode: 'cors',
    body: '{}'
  };

  return Object.assign({}, defaultRequestOptions, opts || {});
}

export function callZephir(uri, opts) {
  
  return fetch(messageRoute(uri), defaultOptions(opts))
    .then(handleAPIResponse)
  ;
}

export function messageRoute(uri) {
  const parts = uri.split('.');
  return `/api/${parts[0]}/${parts.slice(1).join('.')}`;
}

export function handleAPIResponse(response) {
  if(response.type === 'opaqueredirect') {
    return Promise.reject(new UnauthenticatedError());
  }
  if (!response.ok) {
    return response.json()
      .then(res => {
        if (!res.error) return Promise.resolve(res);
        const err = hydrateAPIError(res.error);
        return Promise.reject(err);
      })
    ;
  }
  return response.json();
}

export function hydrateAPIError(errData) {
  const err = new APIError(
    errData.uri,
    errData.description,
    errData.kwargs
  );
  return err;
}


// Utilisation de la notation ES5 pour contourner
// les limitations de Babel quant à l'extension
// de classes natives avec la notation ES6
// Voir https://github.com/babel/babel/issues/4485
function CustomError(message) {
  this.constructor.prototype.__proto__ = Error.prototype;
  this.stack = Error().stack;
  this.message = message;
}
CustomError.prototype = Error.prototype;

export class UnauthenticatedError extends CustomError {
  constructor() {
    super('You are not authenticated. Redirecting...');
    this.name = 'UnauthenticatedError';
    setTimeout(history.push, 2000, '/logout');
  }
}

export class APIError extends CustomError {

  constructor(uri, message, kwargs) {
    super(message);
    this.name = 'APIError';
    this.uri = uri;
    this.kwargs = kwargs;
  }

  getKwarg(key) {
    const kwargs = this.kwargs || {};
    return kwargs[key];
  }

}

export function isAPIError(err) {
  return err instanceof Error && err.name === 'APIError';
}

export function isUnauthenticatedError(err) {
  return err instanceof Error && err.name === 'UnauthenticatedError';
}

export function isBackendError(err) {
  return isAPIError(err) || isUnauthenticatedError(err);
}
