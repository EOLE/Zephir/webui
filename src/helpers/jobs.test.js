/* eslint-env jest */
import { convertJIDToDate } from './jobs';

it('should convert a JID to a Date', () => {

  const jid = '20180425111347469538';
  const date = convertJIDToDate(jid);

  expect(date.getUTCFullYear()).toBe(2018);
  expect(date.getUTCMonth()).toBe(3);
  expect(date.getUTCDate()).toBe(25);
  expect(date.getUTCHours()).toBe(11);
  expect(date.getUTCMinutes()).toBe(13);
  expect(date.getUTCSeconds()).toBe(47);

});
