export default [
  {
    id: 1,
    username: 'admin',
    password: 'admin',
    firstName: 'John',
    lastName: 'Doe'
  },
  {
    id: 2,
    username: 'user',
    password: 'user',
    firstName: 'John',
    lastName: 'Lock'
  },
  {
    id: 3,
    username: 'anna',
    password: 'anna',
    firstName: 'Anna',
    lastName: 'Dang'
  }
];
