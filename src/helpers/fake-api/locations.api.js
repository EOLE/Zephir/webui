/* eslint-env browser */
import locationsData from './locations.data';
localStorage.setItem('locations', JSON.stringify(locationsData));

// array in local storage for registered locations
let locations = JSON.parse(localStorage.getItem('locations')) || [];

const getAllByParentRegex = /\/locations\?parent=(.*)$/;
const getInRectRegex = /\/locations\?rect=(.*)$/;

export default function(url, opts, resolve, reject){

  // get locations
  if (url.endsWith('/locations') && opts.method === 'GET') {
    // check for fake auth token in header and return locations if valid, this security is implemented location side in a real application
    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
      resolve({ ok: true, json: () => locations });
    } else {
      // return 401 not authorised if token is null or invalid
      reject('Unauthorised');
    }

    return;
  }

  if (getAllByParentRegex.test(url) && opts.method === 'GET') {
    const matches = getAllByParentRegex.exec(url);
    const parentId = parseInt(matches[1], 10);
    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
      resolve({ ok: true, json: () => locations.filter(loc => loc.parentId === parentId) });
    } else {
      reject('Unauthorised');
    }
    return;
  }

  if (getInRectRegex.test(url) && opts.method === 'GET') {

    const matches = getInRectRegex.exec(url);
    const rect = matches[1].split(';');
    const northEast = { lat: parseFloat(rect[0]), lon: parseFloat(rect[1]) };
    const southWest = { lat: parseFloat(rect[2]), lon: parseFloat(rect[3]) };



    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
      resolve({ ok: true, json: () => locations.filter(loc => {
        return inBounds(loc, northEast, southWest);
      })});
    } else {
      reject('Unauthorised');
    }
    return;
  }

}

function inBounds(p, nw, se) {
  var eastBound = p.lon < se.lon;
  var westBound = p.lon > nw.lon;
  var inLong;

  if (se.lon < nw.lon) {
    inLong = eastBound || westBound;
  } else {
    inLong = eastBound && westBound;
  }

  var inLat = p.lat > se.lat && p.lat < nw.lat;
  return inLat && inLong;
}
