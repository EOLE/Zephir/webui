import usersData from './users.data';
localStorage.setItem('users', JSON.stringify(usersData));

// array in local storage for registered users
let users = JSON.parse(localStorage.getItem('users')) || [];

export default function(url, opts, resolve, reject){

  // authenticate
  if (url.endsWith('/users/authenticate') && opts.method === 'POST') {
    // get parameters from post request
    let params = JSON.parse(opts.body);

    // find if any user matches login credentials
    let filteredUsers = users.filter(user => {
      return user.username === params.username && user.password === params.password;
    });

    if (filteredUsers.length) {
      // if login details are valid return user details and fake jwt token
      let user = filteredUsers[0];
      let responseJson = {
        id: user.id,
        username: user.username,
        firstName: user.firstName,
        lastName: user.lastName,
        token: 'fake-jwt-token'
      };
      resolve({ ok: true, json: () => responseJson });
    } else {
      // else return error
      reject('Identifiant ou mot de passe invalide');
    }

    return;
  }

  // get users
  if (url.endsWith('/users') && opts.method === 'GET') {
    // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
      resolve({ ok: true, json: () => users });
    } else {
      // return 401 not authorised if token is null or invalid
      reject('Unauthorised');
    }

    return;
  }

  // filter users
  if (url.match(/\/users\?q=.*$/) && opts.method === 'GET') {
    // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
      // find user by id in users array
      let _url = new URL('http://fake'+url);
      var query = _url.searchParams.get("q").toLowerCase().split(' ');

      // respond 200 OK with user
      resolve({ ok: true, json: () => users.filter(user => {
        return user.username.toLowerCase().indexOf(query) !== -1
              || user.firstName.toLowerCase().indexOf(query) !== -1
              || user.lastName.toLowerCase().indexOf(query) !== -1;
      })});
    } else {
      // return 401 not authorised if token is null or invalid
      reject('Unauthorised');
    }

    return;
  }

  // get user by id
  if (url.match(/\/users\/\d+$/) && opts.method === 'GET') {
    // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
      // find user by id in users array
      let urlParts = url.split('/');
      let id = parseInt(urlParts[urlParts.length - 1], 10);
      let matchedUsers = users.filter(user => { return user.id === id; });
      let user = matchedUsers.length ? matchedUsers[0] : null;

      if(user){
        // respond 200 OK with user
        resolve({ ok: true, json: () => user});
      } else {
        reject('Not found');
      }
    } else {
      // return 401 not authorised if token is null or invalid
      reject('Unauthorised');
    }

    return;
  }

  // create user
  if (url.endsWith('/users') && opts.method === 'POST') {
    // get new user object from post body
    let newUser = JSON.parse(opts.body);

    // validation
    let duplicateUser = users.filter(user => { return user.username === newUser.username; }).length;
    if (duplicateUser) {
      reject('Username "' + newUser.username + '" is already taken');
      return;
    }

    // save new user
    newUser.id = users.length ? Math.max(...users.map(user => user.id)) + 1 : 1;
    users.push(newUser);
    localStorage.setItem('users', JSON.stringify(users));

    // respond 200 OK
    resolve({ ok: true, json: () => newUser });

    return;
  }

  // update
  if (url.match(/\/users\/\d+$/) && opts.method === 'PUT') {
    // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
      // find user by id in users array
      let urlParts = url.split('/');
      let id = parseInt(urlParts[urlParts.length - 1], 10);
      let matchedUsers = users.filter(user => { return user.id === id; });
      let user = matchedUsers.length ? matchedUsers[0] : null;

      if(!user)
        return reject('User not found');

      let updatedUser = JSON.parse(opts.body);

      users = users.map(user => {
        if(user.id === id) {
          return {
            ...user,
            ...updatedUser
          };
        } else {
          return user;
        }
      });

      localStorage.setItem('users', JSON.stringify(users));

      // respond 200 OK with user
      resolve({ ok: true, json: () => updatedUser});
    } else {
      // return 401 not authorised if token is null or invalid
      reject('Unauthorised');
    }

    return;
  }

  // delete user
  if (url.match(/\/users\/\d+$/) && opts.method === 'DELETE') {
    // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
      // find user by id in users array
      let urlParts = url.split('/');
      let id = parseInt(urlParts[urlParts.length - 1], 10);
      for (let i = 0; i < users.length; i++) {
        let user = users[i];
        if (user.id === id) {
          // delete user
          users.splice(i, 1);
          localStorage.setItem('users', JSON.stringify(users));
          break;
        }
      }

      // respond 200 OK
      resolve({ ok: true, json: () => ({}) });
    } else {
      // return 401 not authorised if token is null or invalid
      reject('Unauthorised');
    }

    return;
  }

}
