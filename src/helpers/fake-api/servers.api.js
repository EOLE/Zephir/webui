import serversData from './servers.data';
localStorage.setItem('servers', JSON.stringify(serversData));

// array in local storage for registered servers
let servers = JSON.parse(localStorage.getItem('servers')) || [];


export default function(url, opts, resolve, reject){

  // get servers
  if (url.endsWith('/servers') && opts.method === 'GET') {
    // check for fake auth token in header and return servers if valid, this security is implemented server side in a real application
    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
      resolve({ ok: true, json: () => servers});
    } else {
      // return 401 not authorised if token is null or invalid
      reject('Unauthorised');
    }

    return;
  }

  // Get some
  if (url.match(/\/servers\?page=.*&limit=.*$/) && opts.method === 'GET') {
    // check for fake auth token in header and return servers if valid, this security is implemented server side in a real application
    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
      let _url = new URL('http://fake'+url);
      const page = parseInt(_url.searchParams.get("page"), 10) || 1;
      const limit = parseInt(_url.searchParams.get("limit"), 10) || servers.length;
      const start_index = (page-1)*limit;
      const end_index = page*limit;
      resolve({ ok: true, json: () => ({servers: servers.slice(start_index, end_index), hasMore: end_index < servers.length })});
    } else {
      // return 401 not authorised if token is null or invalid
      reject('Unauthorised');
    }

    return;
  }

  // filter servers
  if (url.match(/\/servers\?q=.*$/) && opts.method === 'GET') {
    // check for fake auth token in header and return server if valid, this security is implemented server side in a real application
    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
      // find server by id in servers array
      let _url = new URL('http://fake'+url);
      var query = _url.searchParams.get("q").toLowerCase().split(' ');

      // respond 200 OK with server
      resolve({ ok: true, json: () => servers.filter(server => {
        return server.servername.toLowerCase().indexOf(query) !== -1
              || server.firstName.toLowerCase().indexOf(query) !== -1
              || server.lastName.toLowerCase().indexOf(query) !== -1;
      })});
    } else {
      // return 401 not authorised if token is null or invalid
      reject('Unauthorised');
    }

    return;
  }

  // get server by id
  if (url.match(/\/servers\/\d+$/) && opts.method === 'GET') {
    // check for fake auth token in header and return server if valid, this security is implemented server side in a real application
    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
      // find server by id in servers array
      let urlParts = url.split('/');
      let id = parseInt(urlParts[urlParts.length - 1], 10);
      let matchedservers = servers.filter(server => { return server.id === id; });
      let server = matchedservers.length ? matchedservers[0] : null;

      // respond 200 OK with server
      resolve({ ok: true, json: () => server});
    } else {
      // return 401 not authorised if token is null or invalid
      reject('Unauthorised');
    }

    return;
  }

  const getAllByLocationRegex = /\/servers\?location=(.*)$/;
  if (getAllByLocationRegex.test(url) && opts.method === 'GET') {
    const matches = getAllByLocationRegex.exec(url);
    const locationId = parseInt(matches[1], 10);
    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
      resolve({ ok: true, json: () => servers.filter(srv => srv.locationId === locationId) });
    } else {
      reject('Unauthorised');
    }
    return;
  }


  // create server
  if (url.endsWith('/servers') && opts.method === 'POST') {
    // get new server object from post body
    let newserver = JSON.parse(opts.body);

    // validation
    let duplicateserver = servers.filter(server => { return server.servername === newserver.servername; }).length;
    if (duplicateserver) {
      reject('servername "' + newserver.servername + '" is already taken');
      return;
    }

    // save new server
    newserver.id = servers.length ? Math.max(...servers.map(server => server.id)) + 1 : 1;
    servers.push(newserver);
    localStorage.setItem('servers', JSON.stringify(servers));

    // respond 200 OK
    resolve({ ok: true, json: () => newserver });

    return;
  }

  // update
  if (url.match(/\/servers\/\d+$/) && opts.method === 'PUT') {
    // check for fake auth token in header and return server if valid, this security is implemented server side in a real application
    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
      // find server by id in servers array
      let urlParts = url.split('/');
      let id = parseInt(urlParts[urlParts.length - 1], 10);
      let matchedservers = servers.filter(server => { return server.id === id; });
      let server = matchedservers.length ? matchedservers[0] : null;

      if(!server)
        return reject('server not found');

      let updatedserver = JSON.parse(opts.body);

      servers = servers.map(server => {
        if(server.id === id) {
          return {
            ...server,
            ...updatedserver
          };
        } else {
          return server;
        }
      });

      localStorage.setItem('servers', JSON.stringify(servers));

      // respond 200 OK with server
      resolve({ ok: true, json: () => updatedserver});
    } else {
      // return 401 not authorised if token is null or invalid
      reject('Unauthorised');
    }

    return;
  }

  // delete server
  if (url.match(/\/servers\/\d+$/) && opts.method === 'DELETE') {
    // check for fake auth token in header and return server if valid, this security is implemented server side in a real application
    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
      // find server by id in servers array
      let urlParts = url.split('/');
      let id = parseInt(urlParts[urlParts.length - 1], 10);
      for (let i = 0; i < servers.length; i++) {
        let server = servers[i];
        if (server.id === id) {
          // delete server
          servers.splice(i, 1);
          localStorage.setItem('servers', JSON.stringify(servers));
          break;
        }
      }

      // respond 200 OK
      resolve({ ok: true, json: () => ({}) });
    } else {
      // return 401 not authorised if token is null or invalid
      reject('Unauthorised');
    }

    return;
  }

}
