export default [
  {
    id: 1,
    parentId: null,
    name: 'Academie de Dijon',
    lat: 47.3301651,
    lon: 5.0524797
  },
  {
    id: 2,
    parentId: null,
    name: 'Academie de Besançon',
    lat: 47.2340814,
    lon: 6.0299045
  },
  {
    id: 3,
    parentId: null,
    name: 'Siège Cadoles',
    lat: 47.3210715,
    lon: 5.0283015
  },
  {
    id: 4,
    parentId: null,
    name: 'Maison',
    lat: 47.3178794,
    lon: 5.0348448
  },
  {
    id: 5,
    parentId: 1,
    name: 'Rectorat de Dijon',
    lat: 47.3301651,
    lon: 5.0524797
  },
  {
    id: 6,
    parentId: 5,
    name: 'Etablissement Alpha',
    lat: 47.3121935,
    lon: 5.0469872
  },
  {
    id: 7,
    parentId: 5,
    name: 'Etablissement Beta',
    lat: 47.3350237,
    lon: 5.0657916
  },
  {
    id: 8,
    parentId: 5,
    name: 'Etablissement Charlie',
    lat: 47.3495832,
    lon: 5.0392432,
  },
  {
    id: 9,
    parentId: null,
    name: 'OVH Strasbourg Datacenter',
    lat: 48.5854851,
    lon: 7.7952282,
  }
];
