export default [
  {
    name: 'CPU 1',
    type: 'temp',
    data: [
      { value: 63, at: new Date() },
      { value: 60, at: new Date() },
      { value: 61, at: new Date() }
    ]
  },
  {
    name: 'CPU 2',
    type: 'temp',
    data: [
      { value: 63, at: new Date() },
      { value: 60, at: new Date() },
      { value: 61, at: new Date() }
    ]
  },
  {
    name: 'Temps de réponse',
    type: 'ping',
    data: [
      { value: 63, at: new Date() },
      { value: 60, at: new Date() },
      { value: 61, at: new Date() }
    ]
  },
  {
    name: 'Utilisation Disque',
    type: 'treemap',
    data: [
      { value: [
        {
          name: '/home',
          size: 44547000000,
          children: [
            { name: '/home/user1', size: 15302000000 },
            { name: '/home/user2', size: 24593000000 },
            { name: '/home/user3', size: 4652000000 },
          ],
        },
        {
          name: '/var',
          size: 58630000000,
          children: [
            { name: '/var/log', size: 12302000000 },
            { name: '/var/lib', size: 24593000000 },
            { name: '/var/local', size: 652000000 },
            { name: '/var/opt', size: 636000000 },
            { name: '/var/www', size: 9703000000 },
            { name: '/var/cache', size: 4636000000 },
            { name: '/var/snap', size: 436000000 },
            { name: '/var/spool', size: 2636000000 },
            { name: '/var/mail', size: 3036000000 },
          ],
        },
        {
          name: '/boot',
          size: 1954000000,
          children: [
            { name: '/boot/efi', size: 1302000000 },
            { name: '/boot/grub', size: 652000000 },
          ],
        },
        {
          name: '/etc',
          size: 18658000009,
          children: [
            { name: '/etc/gitlab', size: 1202000000 },
            { name: '/etc/nginx', size: 912000000 },
            { name: '/etc/servicex', size: 1502000000 },
            { name: '/etc/servicex', size: 922000000 },
            { name: '/etc/servicex', size: 1902000000 },
            { name: '/etc/servicex', size: 1102000000 },
            { name: '/etc/servicex', size: 1202000000 },
            { name: '/etc/servicex', size: 952000000 },
            { name: '/etc/servicex', size: 132000000 },
            { name: '/etc/servicex', size: 532000000 },
          ],
        },
        {
          name: '/tmp',
          size: 8258000000,
          children: [
            { name: '/tmp/tmp1', size: 1302000000 },
            { name: '/tmp/tmp2', size: 652000000 },
            { name: '/tmp/tmp3', size: 2652000000 },
            { name: '/tmp/tmp4', size: 3652000000 },
          ],
        },
        {
          name: '/bin',
          children: [
            { name: '/bin/bin1', size: 302000 },
            { name: '/bin/bin2', size: 52000 },
            { name: '/bin/bin3', size: 652000 },
            { name: '/bin/bin4', size: 652000 },
            { name: '/bin/bin1', size: 302000 },
            { name: '/bin/bin2', size: 52000 },
            { name: '/bin/bin3', size: 652000 },
            { name: '/bin/bin4', size: 652000 },
          ],
        },
        {
          name: '/sbin',
          children: [
            { name: '/sbin/bin1', size: 312000 },
            { name: '/sbin/bin2', size: 159000 },
            { name: '/sbin/bin3', size: 452000 },
            { name: '/sbin/bin4', size: 852000 },
            { name: '/sbin/bin1', size: 202000 },
            { name: '/sbin/bin2', size: 59000 },
            { name: '/sbin/bin3', size: 612000 },
            { name: '/sbin/bin4', size: 352000 },
          ],
        },
        {
          name: '/proc',
          children: [
            { name: '/proc/1', size: 120000 },
            { name: '/proc/1', size: 150000 },
            { name: '/proc/1', size: 110000 },
            { name: '/proc/1', size: 190000 },
            { name: '/proc/1', size: 90000 },
            { name: '/proc/1', size: 120000 },
            { name: '/proc/1', size: 130000 },
            { name: '/proc/1', size: 160000 },
            { name: '/proc/1', size: 90000 },
            { name: '/proc/1', size: 80000 },
            { name: '/proc/1', size: 110000 },
            { name: '/proc/1', size: 320000 },
            { name: '/proc/1', size: 120000 },
            { name: '/proc/1', size: 150000 },
            { name: '/proc/1', size: 110000 },
            { name: '/proc/1', size: 110000 },
            { name: '/proc/1', size: 190000 },
            { name: '/proc/1', size: 170000 },
            { name: '/proc/1', size: 220000 },
            { name: '/proc/1', size: 130000 },
            { name: '/proc/1', size: 140000 },
            { name: '/proc/1', size: 120000 },
            { name: '/proc/1', size: 120000 },
            { name: '/proc/1', size: 150000 },
            { name: '/proc/1', size: 170000 },
            { name: '/proc/1', size: 80000 },
            { name: '/proc/1', size: 70000 },
            { name: '/proc/1', size: 120000 },
            { name: '/proc/1', size: 150000 },
            { name: '/proc/1', size: 110000 },
            { name: '/proc/1', size: 190000 },
            { name: '/proc/1', size: 90000 },
            { name: '/proc/1', size: 120000 },
            { name: '/proc/1', size: 130000 },
            { name: '/proc/1', size: 160000 },
            { name: '/proc/1', size: 90000 },
            { name: '/proc/1', size: 80000 },
            { name: '/proc/1', size: 110000 },
            { name: '/proc/1', size: 320000 },
            { name: '/proc/1', size: 120000 },
            { name: '/proc/1', size: 150000 },
            { name: '/proc/1', size: 110000 },
            { name: '/proc/1', size: 110000 },
            { name: '/proc/1', size: 190000 },
            { name: '/proc/1', size: 170000 },
            { name: '/proc/1', size: 220000 },
            { name: '/proc/1', size: 130000 },
            { name: '/proc/1', size: 140000 },
            { name: '/proc/1', size: 120000 },
            { name: '/proc/1', size: 120000 },
            { name: '/proc/1', size: 150000 },
            { name: '/proc/1', size: 170000 },
            { name: '/proc/1', size: 80000 },
            { name: '/proc/1', size: 70000 },
          ],
        },
        {
          free: true,
          size: 25422000000
        },
      ], at: new Date() },
      { value: 60, at: new Date() },
      { value: 61, at: new Date() }
    ]
  },
  {
    name: 'Services',
    type: 'services',
    data: [
      { name: 'apache2', status: 'ok', at: new Date() },
      { name: 'elasticsearch', status: 'ok', at: new Date() },
      { name: 'docker', status: 'ok', at: new Date() },
      { name: 'network-manager', status: 'ok', at: new Date() },
      { name: 'sshd', status: 'ok', at: new Date() },
      { name: 'mysqld', status: 'warning', at: new Date() },
      { name: 'creoled', status: 'warning', at: new Date() },
      { name: 'logrotated', status: 'error', at: new Date() },
      { name: 'ftpd', status: 'disabled', at: new Date() },
    ]
  },
];
