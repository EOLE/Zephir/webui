export const graphConfig = {
  "automaticRearrangeAfterDropNode": true,
  "height": 1000,
  "width": 1000,
  "highlightDegree": 1,
  "highlightOpacity": 0.2,
  "linkHighlightBehavior": false,
  "maxZoom": 2,
  "minZoom": 0.5,
  "nodeHighlightBehavior": true,
  "panAndZoom": true,
  "staticGraph": false,
  "node": {
    "color": "#d3d3d3",
    "fontSize": 16,
    "fontWeight": "normal",
    "labelProperty": "id",
    "mouseCursor": "pointer",
    "opacity": 1,
    "renderLabel": true,
    "size": 500,
    "strokeColor": "none",
    "strokeWidth": 1.5,
    "symbolType": "circle",
    "highlightColor": "SAME",
    "highlightFontSize": 16,
    "highlightFontWeight": "bold",
    "highlightStrokeColor": "#000",
    "highlightStrokeWidth": 1.5
  },
  "link": {
    "color": "#d3d3d3",
    "opacity": 1,
    "semanticStrokeWidth": true,
    "strokeWidth": 3,
    "highlightColor": "#000"
  }
};
