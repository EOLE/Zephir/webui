import {
  ALERT_SHOW_SUCCESS_MESSAGE,
  ALERT_SHOW_ERROR_MESSAGE,
  ALERT_CLEAR
} from '../actions';

export default function alert(state = {}, action) {
  switch (action.type) {
  case ALERT_SHOW_SUCCESS_MESSAGE:
    return {
      type: 'success',
      message: action.message
    };
  case ALERT_SHOW_ERROR_MESSAGE:
    return {
      type: 'error',
      message: action.message
    };
  case ALERT_CLEAR:
    return {};
  default:
    return state;
  }
}
