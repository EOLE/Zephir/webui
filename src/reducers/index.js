import { combineReducers } from 'redux';
import alert from './alert.reducer';
import auth from './auth.reducer';
import servers from './servers.reducer';
import servermodels from './servermodels.reducer';
import ui from './ui.reducer';
import apps from './apps.reducer';
import locations from './locations.reducer';
import backend from './backend.reducer';
import settings from './settings.reducer';
import serverselections from './serverselections.reducer';

export default combineReducers({
  alert,
  ui,
  auth,
  servers,
  apps,
  locations,
  backend,
  servermodels,
  settings,
  serverselections
});
