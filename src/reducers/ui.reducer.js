import { uiConstants } from '../constants';

const defaultState = {
  drawerOpen: false,
  serversIndexActiveTab: 'list'
};

export default function ui(state = defaultState, action) {
  switch (action.type) {
  case uiConstants.TOGGLE_DRAWER:
    return {
      ...state,
      drawerOpen: typeof(action.open) === typeof(true)
        ? action.open
        : !state.drawerOpen
    };
  case uiConstants.CHANGE_SERVERS_INDEX_TAB:
    return {
      ...state,
      serversIndexActiveTab: action.tab
    };
  default:
    return state;
  }
}
