import {
  SERVERSELECTION_LIST_REQUEST,
  SERVERSELECTION_LIST_SUCCESS,
  SERVERSELECTION_LIST_FAILURE,
  SERVERSELECTION_DESCRIBE_SUCCESS,
  SERVERSELECTION_CREATE_REQUEST,
  SERVERSELECTION_CREATE_SUCCESS,
  SERVERSELECTION_CREATE_FAILURE,
  SERVERSELECTION_UPDATE_REQUEST,
  SERVERSELECTION_UPDATE_SUCCESS,
  SERVERSELECTION_UPDATE_FAILURE,
  SERVERSELECTION_UPDATE_LOCAL_FILTER,
  SERVERSELECTION_SERVER_SET_REQUEST,
  SERVERSELECTION_SERVER_SET_FAILURE,
  SERVERSELECTION_SERVER_SET_SUCCESS,
  SERVERSELECTION_USER_ADD_REQUEST,
  SERVERSELECTION_USER_ADD_SUCCESS,
  SERVERSELECTION_USER_ADD_FAILURE,
  SERVERSELECTION_USER_DEL_REQUEST,
  SERVERSELECTION_USER_DEL_SUCCESS,
  SERVERSELECTION_USER_DEL_FAILURE,
  SERVERSELECTION_USER_UPDATE_REQUEST,
  SERVERSELECTION_USER_UPDATE_SUCCESS,
  SERVERSELECTION_USER_UPDATE_FAILURE
} from '../actions/serverselection.actions';

const initialState = {
  isLoading: false,
  byId: {}
};

export default function serverselections(state = initialState, action) {
  switch (action.type) {

  // Mise à jour du flag "isLoading" à true pour
  // toutes les requetes
  case SERVERSELECTION_CREATE_REQUEST:
  case SERVERSELECTION_LIST_REQUEST:
  case SERVERSELECTION_UPDATE_REQUEST:
  case SERVERSELECTION_SERVER_SET_REQUEST:
  case SERVERSELECTION_USER_ADD_REQUEST:
  case SERVERSELECTION_USER_DEL_REQUEST:
  case SERVERSELECTION_USER_UPDATE_REQUEST:
    return updateLoadingFlag(state, true);

  // Mise à jour du flag "isLoading" à false pour
  // tous les échecs
  case SERVERSELECTION_CREATE_FAILURE:
  case SERVERSELECTION_UPDATE_FAILURE:
  case SERVERSELECTION_LIST_FAILURE:
  case SERVERSELECTION_SERVER_SET_FAILURE:
  case SERVERSELECTION_USER_ADD_FAILURE:
  case SERVERSELECTION_USER_DEL_FAILURE:
  case SERVERSELECTION_USER_UPDATE_FAILURE:
    return updateLoadingFlag(state, false);

  case SERVERSELECTION_CREATE_SUCCESS:
  case SERVERSELECTION_UPDATE_SUCCESS:
  case SERVERSELECTION_SERVER_SET_SUCCESS:
  case SERVERSELECTION_USER_ADD_SUCCESS:
  case SERVERSELECTION_USER_DEL_SUCCESS:
  case SERVERSELECTION_USER_UPDATE_SUCCESS:
    return updateLoadingFlag(handleServerSelectionUpdateSuccess(state, action), false);

  case SERVERSELECTION_LIST_SUCCESS:
    return updateLoadingFlag(handleServerSelectionListSuccess(state, action), false);

  case SERVERSELECTION_DESCRIBE_SUCCESS:
    return updateLoadingFlag(handleServerSelectionDescribeSuccess(state, action), false);

  case SERVERSELECTION_UPDATE_LOCAL_FILTER:
    return handleUpdateLocalFilter(state, action);

  default:
    return state;
  }
}

function updateLoadingFlag(state, isLoading) {
  return {
    ...state,
    isLoading
  };
}

function handleServerSelectionListSuccess(state, action) {
  return {
    ...state,
    byId: action.serverselections.reduce((serverselections, serverselection) => {
      serverselections[serverselection.serverselectionid] = serverselection;
      return serverselections;
    }, {...state.serverselectionsById})
  };
}

function handleServerSelectionDescribeSuccess(state, action) {
  return {
    ...state,
    byId: Object.assign({...state.byId}, {
      [action.serverselection.serverselectionid]: {...action.serverselection}
    })
  };
}
function handleUpdateLocalFilter(state, action) {
  return {
    ...state,
    filter: action.filter
  };
}
function handleServerSelectionUpdateSuccess(state, action) {
  return {
    ...state,
    byId: Object.assign({...state.byId}, {
      [action.serverselection.serverselectionid]: {...action.serverselection}
    })
  };
}
