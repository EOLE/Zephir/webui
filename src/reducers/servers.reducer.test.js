/* eslint-env jest */
import servers from './servers.reducer';
import { createStore } from 'redux';
import {
  SERVER_DESCRIBE_SUCCESS,
  SERVER_LIST_SUCCESS
} from '../actions/server.actions';

describe('servers reducer', () => {

  it('should apply server.list result to the state', () => {

    const store = createStore(servers, {});

    const serverMock = {
      automation: 'salt',
      servermodelid: 2,
      serverdescription: 'test',
      servername: 'test1',
      serverid: 1
    };

    store.dispatch({
      type: SERVER_DESCRIBE_SUCCESS,
      server: serverMock
    });

    const state = store.getState();

    expect(state.byId[1]).toMatchObject(serverMock);

  });

  it('should apply server.describe result to the state', () => {

    const store = createStore(servers, {});

    const serversMock = [
      {
        automation: 'salt',
        servermodelid: 1,
        serverdescription: 'base',
        servername: 'base',
        serverid: 1
      },
      {
        automation: 'salt',
        servermodelid: 2,
        serverdescription: 'horus',
        servername: 'horus',
        serverid: 2
      }
    ];

    store.dispatch({
      type: SERVER_LIST_SUCCESS,
      servers: serversMock
    });

    const state = store.getState();

    expect(state.byId[1]).toMatchObject(serversMock[0]);
    expect(state.byId[2]).toMatchObject(serversMock[1]);

  });

});
