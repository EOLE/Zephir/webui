import {
  SERVERMODEL_DESCRIBE_SUCCESS,
  SERVERMODEL_LIST_SUCCESS,
  SERVERMODEL_UPDATE_LOCAL_FILTER
} from '../actions/servermodel.actions';

const initialState = {
  byId: {},
  describeById: {},
  lastRefresh: null,
  filter: ''
};

export default function servermodels(state = initialState, action) {
  switch (action.type) {

  case SERVERMODEL_DESCRIBE_SUCCESS:
    return updateLastRefresh(handleServermodelDescribeSuccess(state, action));

  case SERVERMODEL_LIST_SUCCESS:
    return updateLastRefresh(handleServermodelListSuccess(state, action));

  case SERVERMODEL_UPDATE_LOCAL_FILTER:
    return handleUpdateLocalFilter(state, action);

  default:
    return state;
  }
}

function updateLastRefresh(state) {
  return {
    ...state,
    lastRefresh: new Date()
  };
}

function handleServermodelListSuccess(state, action) {
  return {
    ...state,
    byId: action.servermodels.reduce((servermodels, model) => {
      servermodels[model.servermodelid] = { ...servermodels[model.servermodelid], model };
      return servermodels;
    }, {...state.servermodelsById})
  };
}
function handleUpdateLocalFilter(state, action) {
  return {
    ...state,
    filter: action.filter
  };
}

function handleServermodelDescribeSuccess(state, action) {
  return {
    ...state,
    describeById: {
      ...state.servermodelsById,
      [action.servermodel.model.servermodelid]: {...action.servermodel}
    }
  };
}
