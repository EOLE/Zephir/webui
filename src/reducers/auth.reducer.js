import {
  AUTH_GET_SESSION_USER_SUCCESS,
} from '../actions';

const initState = {
  user: null
};

export default function auth (state = initState, action) {
  switch (action.type) {
  case AUTH_GET_SESSION_USER_SUCCESS:
    return {
      ...state,
      user: action.user
    };
  default:
    return state;
  }
}
