const initialState = {
  pendingRequests: 0, // Compteur d'actions asynchrones en cours de résolution
  isLoading: false,   // Passe à true si une action asynchrone est en cours de résolution
};

const requestActionRegExp = /_REQUEST$/;
const successActionRegExp = /_SUCCESS$/;
const failureActionRegExp = /_FAILURE$/;

// Backend reducer
// Ce reducer détecte toutes les actions asynchrones du type *_REQUEST, *_SUCCESS et *_FAILURE
// et maintient un compte de celles ci afin d'identifier si l'application est en
// "attente" de résultats de la part du serveur
export default function ajax(state = initialState, action) {

  if ( requestActionRegExp.test(action.type) ) {
    const pendingRequests = state.pendingRequests+1;
    return {
      ...state,
      pendingRequests,
      isLoading: pendingRequests !== 0
    };
  }

  if ( successActionRegExp.test(action.type) || failureActionRegExp.test(action.type) ) {
    const pendingRequests = state.pendingRequests-1;
    return {
      ...state,
      pendingRequests: pendingRequests,
      isLoading: pendingRequests !== 0
    };
  }

  return state;
}