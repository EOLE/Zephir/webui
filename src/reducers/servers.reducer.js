import {
  SERVER_LIST_REQUEST,
  SERVER_LIST_SUCCESS,
  SERVER_LIST_FAILURE,
  SERVER_DESCRIBE_SUCCESS,
  SERVER_CREATE_REQUEST,
  SERVER_CREATE_SUCCESS,
  SERVER_CREATE_FAILURE,
  SERVER_UPDATE_REQUEST,
  SERVER_UPDATE_SUCCESS,
  SERVER_UPDATE_FAILURE,
  SERVER_UPDATE_LOCAL_FILTER,
  SERVER_SERVERSELECTION_LIST_REQUEST,
  SERVER_SERVERSELECTION_LIST_SUCCESS,
  SERVER_SERVERSELECTION_LIST_FAILURE,
} from '../actions/server.actions';

const initialState = {
  isLoading: false,
  byId: {},
  filter: ''
};

export default function servers(state = initialState, action) {
  switch (action.type) {

  // Mise à jour du flag "isLoading" à true pour
  // toutes les requetes
  case SERVER_CREATE_REQUEST:
  case SERVER_LIST_REQUEST:
  case SERVER_UPDATE_REQUEST:
  case SERVER_SERVERSELECTION_LIST_REQUEST:
    return updateLoadingFlag(state, true);

  // Mise à jour du flag "isLoading" à false pour
  // tous les échecs
  case SERVER_CREATE_FAILURE:
  case SERVER_UPDATE_FAILURE:
  case SERVER_LIST_FAILURE:
  case SERVER_SERVERSELECTION_LIST_FAILURE:
    return updateLoadingFlag(state, false);

  case SERVER_CREATE_SUCCESS:
  case SERVER_UPDATE_SUCCESS:
    return updateLoadingFlag(handleServerUpdateSuccess(state, action), false);

  case SERVER_LIST_SUCCESS:
    return updateLoadingFlag(handleServerListSuccess(state, action), false);

  case SERVER_SERVERSELECTION_LIST_SUCCESS:
    return updateLoadingFlag(handleServerSelectionsListSuccess(state, action), false);

  case SERVER_DESCRIBE_SUCCESS:
    return updateLoadingFlag(handleServerDescribeSuccess(state, action), false);

  case SERVER_UPDATE_LOCAL_FILTER:
    return handleUpdateLocalFilter(state, action);

  default:
    return state;
  }
}

function updateLoadingFlag(state, isLoading) {
  return {
    ...state,
    isLoading
  };
}

function handleServerListSuccess(state, action) {
  return {
    ...state,
    byId: action.servers.reduce((servers, server) => {
      servers[server.serverid] = server;
      return servers;
    }, {...state.serversById})
  };
}
function handleServerSelectionsListSuccess(state, action) {

  return {
    ...state,
    selections: action.selections
  };
}

function handleServerDescribeSuccess(state, action) {
  return {
    ...state,
    byId: Object.assign({...state.byId}, {
      [action.server.serverid]: {...action.server}
    })
  };
}

function handleUpdateLocalFilter(state, action) {
  return {
    ...state,
    filter: action.filter
  };
}

function handleServerUpdateSuccess(state, action) {
  return {
    ...state,
    byId: Object.assign({...state.byId}, {
      [action.server.serverid]: {...action.server}
    })
  };
}
