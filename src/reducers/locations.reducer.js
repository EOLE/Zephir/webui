import { locationConstants } from '../constants';

const initState = {
  itemsLoading: false,
  item: null,
  items: [],
  inRectItems: [],
  inRectItemsLoading: false,
};

export default function locations(state = initState, action) {
  switch (action.type) {
  case locationConstants.GETALL_REQUEST:
    return {
      ...state,
      itemsLoading: true,
      items: null
    };
  case locationConstants.GETALL_SUCCESS:
    return {
      ...state,
      itemsLoading: false,
      items: action.locations
    };
  case locationConstants.FILTER_FAILURE:
    return {
      ...state,
      itemsLoading: false,
      error: action.error
    };
  case locationConstants.GETINRECT_REQUEST:
    return {
      ...state,
      inRectItemsLoading: true
    };
  case locationConstants.GETINRECT_SUCCESS:
    return {
      ...state,
      inRectItemsLoading: false,
      inRectItems: action.locations
    };
  case locationConstants.GETNEAR_FAILURE:
    return {
      ...state,
      inRectItemsLoading: false,
      error: action.error
    };
  default:
    return state;
  }
}
