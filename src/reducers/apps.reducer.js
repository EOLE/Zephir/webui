import {
  APPS_OPEN_APP_SUCCESS,
  APPS_CLOSE_APP_SUCCESS
} from '../actions';
import {
  getAvailableApps,
  getAppMetadata,
} from '../helpers/apps';

const initialState = {
  // TEMP Ces données devrait être récupérées
  // depuis le backend (ou déduite en fonction du modèle de server, des services sur le serveur etc)
  available: getAvailableApps(),
  opened: []
};

export default function apps(state = initialState, action) {
  switch (action.type) {

  case APPS_OPEN_APP_SUCCESS:
    return handleOpenAppSuccess(state, action);

  case APPS_CLOSE_APP_SUCCESS:
    return handleCloseAppSuccess(state, action);

  default:
    return state;

  }
}

function handleOpenAppSuccess(state, action) {
  const { opened } = state;
  const foundInstances = opened.filter(({id}) => id === action.instance.id);
  const appMetadata = getAppMetadata(action.instance.appName);
  return {
    ...state,
    opened: (
      foundInstances.length > 0
        ? opened
        : [...opened, { ...appMetadata, ...action.instance }]
    )
  };
}

function handleCloseAppSuccess(state, action) {
  return {
    ...state,
    opened: state.opened.filter(instance => instance.id !== action.instance.id)
  };
}
