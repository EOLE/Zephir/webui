// import { PUBLIC_KEY } from '../actions';
import {
  SETTINGS_GET_SUCCESS,
  SETTINGS_GET_REQUEST,
  SETTINGS_GET_FAILURE
} from '../actions/settings.actions';

const initialState = {
  isLoading: false
};

export default function settings(state = initialState, action) {
  switch(action.type) {

    case SETTINGS_GET_REQUEST:
      return updateLoadingFlag(state, true);

    case SETTINGS_GET_FAILURE:
      return updateLoadingFlag(state, false);

    case SETTINGS_GET_SUCCESS:
      return updateLoadingFlag(handleGetSettingSuccess(state, action), false);
    default:
      return state;
  }
}
function updateLoadingFlag(state, isLoading) {
  return {
    ...state,
    isLoading
  };
}
function handleGetSettingSuccess(state, action) {
  return {
    ...state,
    ...action.settings

  };
}
