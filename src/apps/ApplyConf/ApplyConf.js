/* globals Promise */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { ActionApp } from '../../components';
import { FlashOn as FlashOnIcon } from '@material-ui/icons';
import { Button } from '@material-ui/core';
import { execActions } from '../../actions';
import { connect } from 'react-redux';

class ApplyConf extends React.Component {

  static getAppMetadata() {
    return {
      label: 'Appliquer la configuration',
      icon: 'FlashOn',
      category: 'actions'
    };
  }

  static getInstanceLabel(instance) {
    const { servername } = instance.opts.server ? instance.opts.server : '';
    return `${servername ? (servername+' - ') : ''} Appliquer la configuration`;
  }

  static open(instance) {
    return Promise.resolve(instance);
  }

  static close() {
    return Promise.resolve();
  }

  constructor(props) {
    super(props);
    this.state = {
      jobsOutdated: true,
    };
    this.filterJob = this.filterJob.bind(this);
    this.onApplyConfClick = this.onApplyConfClick.bind(this);
    this.onJobsRefreshed = this.onJobsRefreshed.bind(this);
  }

  render() {
    const { classes, instance } = this.props;
    const { jobsOutdated } = this.state;
    return (
      <div className={classes.root}>
        <ActionApp jobsOutdated={jobsOutdated}
          instance={instance}
          onJobsRefreshed={this.onJobsRefreshed}
          jobFilter={this.filterJob}
          actionsBar={
            <React.Fragment>
              <Button onClick={this.onApplyConfClick} variant="raised" size="large" color="primary">
              Appliquer la configuration
                <FlashOnIcon className={classes.rightIcon} />
              </Button>
            </React.Fragment>
          } />
      </div>
    );
  }

  filterJob(job) {
    const { instance } = this.props;
    const { server } = instance.opts;
    return job.server_id === server.serverid
      && job.command === 'reconfigure'
    ;
  }

  onApplyConfClick() {
    const { instance } = this.props;
    const { server } = instance.opts;
    this.applyConfiguration(server.serverid);
  }

  applyConfiguration(serverId) {
    const action = execActions.saltExec(
      serverId,
      'reconfigure',
      []
    );
    return this.props.dispatch(action)
      .then(() => {
        this.setState({ jobsOutdated: true });
      });
  }

  onJobsRefreshed() {
    this.setState({ jobsOutdated: false });
  }

}

ApplyConf.propTypes = {
  classes: PropTypes.object.isRequired,
  instance: PropTypes.object.isRequired
};

// App instance options definition
ApplyConf.instanceOptsPropTypes = {};

const styles = theme => {
  return {
    root: {
      height: '100%',
      width: '100%',
      margin: 0,
      padding: 0,
      backgroundColor: theme.palette.background.paper,
    },
    rightIcon: {
      marginLeft: theme.spacing.unit,
    },
  };
};

const styledApplyConf = withStyles(styles)(ApplyConf);
const connectedApplyConf = connect()(styledApplyConf);
export { connectedApplyConf as ApplyConf };
