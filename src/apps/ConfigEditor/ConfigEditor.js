/* globals Promise */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { configActions, alertActions } from '../../actions';
import { isAPIError } from '../../helpers/backend';
import { store } from '../../helpers/store';

class ConfigEditor extends React.Component {

  static getAppMetadata() {
    return {
      label: "Éditeur de configuration",
      icon: 'GenConfig',
      category: 'configuration'
    };
  }

  static getInstanceLabel(instance) {
    const { server } = instance.opts || {};
    const { servermodel } = instance.opts || {};

    let name = ''

    if (server) {name = server.servername}
    if (servermodel) {name = servermodel.servermodelname}

    const { label } = ConfigEditor.getAppMetadata();
    return `${name} - ${label}`;
  }

  static open(instance, opened) {
    const { server } = instance.opts || {};
    const { servermodel } = instance.opts || {};

    if (!server && ! servermodel) return Promise.reject('Un serveur ou un modèle doit être spécifié.');

    let id = 0;
    if (server) id = parseInt(server.serverid, 10);
    if (servermodel) id = parseInt(servermodel.servermodelid, 10);

    // L'édition de la configuration est en mode "singleton"
    // on doit donc rechercher si une instance est déjà ouverte pour le serveur
    for(let openedInstance, i = 0; (openedInstance = opened[i]); ++i) {
      if (openedInstance.appName !== instance.appName) continue;
      if (server && openedInstance.opts.server.serverid === id) {
        return Promise.resolve(openedInstance);
      }
      if (servermodel && openedInstance.opts.servermodel.servermodelid === id) {
        return Promise.resolve(openedInstance);
      }
    }
    let type = '';
    if (server) type = 'server';
    if (servermodel) type = 'servermodel';

    return store.dispatch(configActions.startSession(id, type))
      .then(res => {
        instance.opts.configSession = {id: res.sessionid, url: `/config/${res.sessionid}/`};
        return instance;
      })
      .catch(err => {

        // Si l'erreur n'est pas une erreur d'API
        // on stoppe la tentative d'ouverture de session ici
        if (!isAPIError(err)) throw err;

        const state = store.getState();

        // Si une session est déjà ouverte par cet utilisateur, on tente
        // de réafficher celle ci
        // TODO Importer les erreurs depuis les messages pour éviter
        // une "magic string" ?
        if (err.uri === 'v1.config.session.error.locked' && err.getKwarg('username') === state.auth.user.username) {
          const sessionId = err.getKwarg('sessionid');
          instance.opts.configSession = {id: sessionId, url: `/config/${sessionId}/`};
          return instance;
        }

        throw err;

      });
  }

  static close(instance) {

    const { configSession } = instance.opts || {};
    const { server } = instance.opts || {};
    const { servermodel } = instance.opts || {};
    let type = "";
    if (server) type = "server"
    if (servermodel) type = "servermodel"
    if (!configSession) return Promise.reject('Aucune session d\'édition de configuration n\'est associée à l\'application.');
    document.cookie = "X-Userinfo=; expires=Thu, 01 Jan 1970 00:00:00 GMT";
    return store.dispatch(configActions.stopSession(configSession.id, type))
      .catch(err => {
        store.dispatch(alertActions.error(err.message));
      });
  }

  render() {
    const { classes, instance } = this.props;
    const { url } = instance.opts.configSession;

    var date = new Date();
    date.setTime(date.getTime()+(30*24*60*60*1000));
    const authToken = sessionStorage.getItem('kc_token');
    document.cookie = "X-Userinfo=" + authToken + "; expires=" + date.toGMTString();
    return <iframe title={`app-frame-${instance.id}`}
      className={classes.frame} src={url} />;
  }

}

ConfigEditor.propTypes = {
  classes: PropTypes.object.isRequired,
  instance: PropTypes.object.isRequired
};

const styles = theme => {
  return {
    frame: {
      height: 'calc(100% - 5px)',
      width: '100%',
      margin: 0,
      padding: 0,
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
      border: 'none'
    }
  };
};

const styledConfigEditor = withStyles(styles)(ConfigEditor);
export { styledConfigEditor as ConfigEditor };
