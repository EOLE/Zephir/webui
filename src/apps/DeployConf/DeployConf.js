/* globals Promise */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Button } from '@material-ui/core';
import {
  CloudUpload as CloudUploadIcon,
} from '@material-ui/icons';
import { execActions } from '../../actions';
import { ActionApp } from '../../components';

class DeployConf extends React.Component {

  static getAppMetadata() {
    return {
      label: 'Déploiement de la configuration',
      icon: 'CloudUpload',
      category: 'actions'
    };
  }

  static getInstanceLabel(instance) {
    const { servername } = instance.opts.server ? instance.opts.server : '';
    return `${servername ? (servername+' - ') : ''} Déploiement`;
  }

  static open(instance) {
    return Promise.resolve(instance);
  }

  static close() {
    return Promise.resolve();
  }

  constructor(props) {
    super(props);
    this.state = {
      jobsOutdated: true
    };
    this.onDeployClick = this.onDeployClick.bind(this);
    this.filterJob = this.filterJob.bind(this);
    this.onJobsRefreshed = this.onJobsRefreshed.bind(this);
  }

  render() {
    const { jobsOutdated } = this.state;
    const { classes, instance } = this.props;
    return (
      <div className={classes.root}>
        <ActionApp
          instance={instance}
          jobsOutdated={jobsOutdated}
          onJobsRefreshed={this.onJobsRefreshed}
          jobFilter={this.filterJob} actionsBar={
            <React.Fragment>
              <Button onClick={this.onDeployClick} variant="raised" size="large" color="primary">
                Déployer la configuration
                <CloudUploadIcon className={classes.rightIcon} />
              </Button>
            </React.Fragment>
          } />
      </div>
    );
  }

  deployConfiguration(serverId) {
    return this.props.dispatch(execActions.deployConfiguration(serverId))
      .then(() => {
        this.setState({ jobsOutdated: true });
      });
  }

  onDeployClick() {
    const { instance } = this.props;
    const { server } = instance.opts;
    this.deployConfiguration(server.serverid);
  }

  filterJob(job) {
    const { instance } = this.props;
    const { server } = instance.opts;
    return job.server_id === server.serverid
      && job.command === 'v1.server.exec.deploy'
    ;
  }

  onJobsRefreshed() {
    this.setState({ jobsOutdated: false });
  }

}

DeployConf.propTypes = {
  classes: PropTypes.object.isRequired,
  instance: PropTypes.object.isRequired
};

DeployConf.instanceOptsPropTypes = {
  server: PropTypes.object,
  servers: PropTypes.arrayOf(PropTypes.object)
};

const styles = theme => {
  return {
    root: {
      height: '100%',
      width: '100%',
      margin: 0,
      padding: theme.spacing.unit * 2,
      backgroundColor: theme.palette.background.paper,
    },
    rightIcon: {
      marginLeft: theme.spacing.unit,
    },
  };
};

const mapStateToProps = () => {
  return {};
};

const styledDeployConf = withStyles(styles)(DeployConf);
const connectedDeployConf = connect(mapStateToProps)(styledDeployConf);
export { connectedDeployConf as DeployConf };
