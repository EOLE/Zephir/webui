import React from 'react';
import { Typography, Grid } from '@material-ui/core';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

class PeeringConfDetails extends React.Component {

  render() {
    const { classes, peeringConf } = this.props;
    return (
      <React.Fragment>
        <Typography className={classes.title}
          variant="h5"
        >
          Informations d&#39;appairage
        </Typography>
        <Grid container>
          <Grid item sm={4}>
            <Typography variant="subtitle1">Configuration du client</Typography>
          </Grid>
          <Grid item sm={8} className={classes.codeContainer}>
            <pre className={classes.code}>
              {peeringConf.client_configuration}
            </pre>
          </Grid>
        </Grid>
        <Grid container>
          <Grid item sm={4}>
            <Typography variant="subtitle1">Clé publique</Typography>
          </Grid>
          <Grid item sm={8} className={classes.codeContainer}>
            <pre className={classes.code}>
              {peeringConf.pub_key}
            </pre>
          </Grid>
        </Grid>
        <Grid container>
          <Grid item sm={4}>
            <Typography variant="subtitle1">Clé privée</Typography>
          </Grid>
          <Grid item sm={8} className={classes.codeContainer}>
            <pre className={classes.code}>
              {peeringConf.ciphered_priv_key}
            </pre>
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }

}

PeeringConfDetails.propTypes = {
  peeringConf: PropTypes.object.isRequired
};

const styles = theme => {
  return {
    title: {
      marginBottom: theme.spacing.unit * 2,
    },
    codeContainer: {
      width: '100%'
    },
    code: {
      overflow: 'auto',
      backgroundColor: 'hsla(0, 0%, 95%, 1)',
      padding: theme.spacing.unit,
      fontFamily: 'monospace',
      whiteSpace: 'pre-wrap',
      wordBreak: 'keep-all'
    }
  };
};

const styledPeeringConfDetails = withStyles(styles)(PeeringConfDetails);
export { styledPeeringConfDetails as PeeringConfDetails };