/* globals Promise */
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { serverActions } from '../../actions';
import { connect } from 'react-redux';
import { Button, Grid, Paper, Typography } from '@material-ui/core';
import {
  SaveAlt as FileDownloadIcon
} from '@material-ui/icons';
import { Loader } from '../../components';
import { PeeringConfDetails } from './PeeringConfDetails';

class PeeringConfViewer extends React.Component {

  static getAppMetadata() {
    return {
      label: 'Appairage du serveur',
      icon: 'Link',
      category: 'peering'
    };
  }

  static getInstanceLabel(instance) {
    const { server } = instance.opts;
    return `${server.servername} - Appairage du serveur`;
  }

  static open(instance) {
    return Promise.resolve(instance);
  }

  static close() {
    return Promise.resolve();
  }

  constructor(props) {
    super(props);
    this.state = {
      peeringConf: null,
      isLoading: false
    };
    this.onDownloadFile = this.onDownloadFile.bind(this);
  }

  render() {

    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <Grid container>
          <Grid className={classes.section} container justify="flex-end">
            <Button onClick={this.onDownloadFile} variant='contained' size="large" color="primary">
              Télécharger le fichier de configuration
              <FileDownloadIcon className={classes.rightIcon} />
            </Button>
          </Grid>
          <Grid className={classes.section} container>
            <Paper className={classes.details}>
              { this.renderDetails() }
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }

  renderDetails() {
    const { classes } = this.props;
    const { peeringConf, isLoading } = this.state;
    if (isLoading) return <Loader />;
    if (!peeringConf) return <Typography className={classes.warning}>Impossible de récupérer la configuration.</Typography>;
    return  <PeeringConfDetails peeringConf={peeringConf} />;
  }

  componentDidMount() {
    this.loadPeeringConf();
  }

  loadPeeringConf() {
    const { instance } = this.props;
    const { server } = instance.opts;
    this.setState({isLoading: true}, () => {
      this.props.dispatch(serverActions.getPeeringConf(server.serverid))
        .then(result => {
          this.setState({ peeringConf: result });
        }).catch(e => {console.log(e)})
        .finally(() => this.setState({ isLoading: false }));
    });
  }

  onDownloadFile() {
    const { instance } = this.props;
    const { peeringConf } = this.state;
    const { server } = instance.opts;
    const now = new Date();
    const data = btoa(JSON.stringify(peeringConf));
    const link = document.createElement("a");
    window.document.body.appendChild(link);
    link.download = `zephir_${this.toSnakeCase(server.servername)}_${this.toDateString(now)}.conf.json`;
    link.href = `data:application/json;base64,${data}`;
    link.target = '_blank';
    link.click();
    link.parentElement.removeChild(link);
  }

  toSnakeCase(str) {
    return str.toLowerCase().replace(/ +/g, '_');
  }

  toDateString(date) {
    const month = date.getMonth() + 1;
    const paddedMonth = month < 10 ? `0${month}` : month;
    return `${date.getFullYear()}${paddedMonth}${date.getDate()}`;
  }

}

PeeringConfViewer.propTypes = {
  classes: PropTypes.object.isRequired,
  instance: PropTypes.object.isRequired
};

// App instance options definition
PeeringConfViewer.instanceOptsPropTypes = {
  server: PropTypes.object.isRequired
};

const styles = theme => {
  return {
    root: {
      height: '100%',
      width: '100%',
      margin: 0,
      padding: theme.spacing.unit * 2,
      backgroundColor: theme.palette.background.paper,
    },
    section: {
      padding: theme.spacing.unit * 2,
      marginBottom: theme.spacing.unit
    },
    rightIcon: {
      marginLeft: theme.spacing.unit,
    },
    details: {
      width: '100%',
      padding: theme.spacing.unit * 2,
    },
    warning: {
      textAlign: 'center'
    }
  };
};

const styledPeeringConfViewer = withStyles(styles)(PeeringConfViewer);
const connectedPeeringConfiViewer = connect()(styledPeeringConfViewer);
export { connectedPeeringConfiViewer as PeeringConfViewer };
