import React, { Component } from 'react';
import { Router, Route, Switch, Redirect } from 'react-router';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { history } from './helpers';
import { Alert, Layout, Loader } from './components';
import { getSessionUser } from './actions';
import { LinearProgress } from '@material-ui/core';


import './App.css';

import {
  NotFoundPage,
  HomePage,
  ServersPage,
  ServerSelectionsPage,
  ServerModelsPage,
  AppsPage,
  UnauthorizedPage,
  LogoutPage,
  PreferencesPage,
  JobsPage
} from './pages';

class App extends Component {

  constructor(props) {
    super(...props);
    this.state = {
      isLoadingUser: true,
    };
  }

  render() {
    const { classes } = this.props;
    const { isLoadingUser } = this.state;
    return (
      <div className={classes.fitHeight}>
        { isLoadingUser ?  <Loader /> : this.renderRoutes() }
      </div>
    );
  }

  renderRoutes() {
    const { classes, user, isLoading } = this.props;
    return (
      <React.Fragment>
        <Router history={history}>
          <div className={classes.fitHeight}>
            <LinearProgress
              className={`${classes.progress} ${isLoading ? classes.visibleProgress : ''}`}
              variant="indeterminate" color="secondary"
            />
            <Switch>
              {/* PUBLIC ROUTES */}
              <Route path='/logout' component={LogoutPage} />
              <Route path="/unauthorized" component={UnauthorizedPage} />
              {/* PRIVATE ROUTES */}
              {user
                ? <Layout>
                  <Switch>
                    <Route exact path="(/|/widgets)" component={HomePage} />
                    <Route path="/servers" component={ServersPage} />
                    <Route path="/serverselections" component ={ServerSelectionsPage} />
                    <Route path="/servermodels" component={ServerModelsPage} />
                    <Route path="/jobs/:id" component={JobsPage} />
                    <Route path="/preferences" component={PreferencesPage} />
                    <Route path="/apps" />
                    <Route component={NotFoundPage} />
                  </Switch>
                  <Switch>
                    <Route path="/apps/:id" component={AppsPage} />
                    <Route path="*" component={AppsPage} />
                  </Switch>
                </Layout>
                : <Redirect to="/unauthorized" />
              }
              <Route component={NotFoundPage} />
            </Switch>
          </div>
        </Router>
        <Alert />
      </React.Fragment>
    );
  }

  componentDidMount() {
    this.props.dispatch(getSessionUser())
      .then(() => {
        this.setState({isLoadingUser: false});
      })
      .catch(() => {
        this.setState({isLoadingUser: false});
      })
    ;
  }

}

const styles = () => ({
  fitHeight: {
    height: '100%',
    display: 'block',
    position: 'relative'
  },
  progress: {
    height: '5px',
    position: 'absolute',
    zIndex: 10000,
    opacity: 0,
    top: 0,
    left: 0,
    right: 0,
    transition: '100ms linear opacity'
  },
  visibleProgress: {
    opacity: 1
  }
});

function mapStateToProps(state) {
  const { auth, backend } = state;
  return {
    user: auth.user,
    isLoading: backend.isLoading
  };
}

const connectedApp = connect(mapStateToProps)(App);
const styledApp = withStyles(styles)(connectedApp);
export { styledApp as App };
export default App;
