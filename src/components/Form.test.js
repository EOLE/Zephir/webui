/* eslint-env jest */
import React from 'react';
import renderer from 'react-test-renderer';
import { withForm } from './Form';

describe('Form', () => {

  class MyTestForm extends React.Component {
    render() {
      const { form } = this.props;
      return (
        <div>
          <input type="text" name="testField" value={form.field('testField', 'foo')} />
          <span>{form.errors.testField}</span>
        </div>
      );
    }
  }

  it('should render the default value', () => {

    const ManagedTestForm = withForm()(MyTestForm);

    let component = renderer.create(
      <ManagedTestForm />
    );
    let tree = component.toJSON();

    expect(tree.children[0].props.value).toBe('foo');

  });

  it('should validate the field', () => {

    const validators = {
      testField: (value, data, submitting) => {
        expect(submitting).toBe(false);
        expect(value).toBe('bar');
        return "MY ERROR MESSAGE";
      }
    };

    const ManagedTestForm = withForm(validators)(MyTestForm);

    let component = renderer.create(
      <ManagedTestForm />
    );

    // On simule un changement de valeur
    const formInstance = component.root.findByType(MyTestForm);
    const fakeChangeEvent = {
      target: {
        name: 'testField',
        value: 'bar'
      }
    };
    formInstance.props.form.onChange(fakeChangeEvent);

    let tree = component.toJSON();

    expect(tree.children[0].props.value).toBe('bar');
    expect(tree.children[1].children[0]).toBe("MY ERROR MESSAGE");

  });

});


