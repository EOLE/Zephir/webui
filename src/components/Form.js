import React from 'react';

export function withForm(validators = {}) {

  return WrappedComponent => {

    return class Form extends React.Component {

      constructor(props) {
        super(props);
        this.state = {
          formData: {},
          formErrors: {},
          changed: false,
          submitted: false,
        };
        this.defaults = {};
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.resetForm = this.resetForm.bind(this);
        this.formField = this.formField.bind(this);
      }

      render() {
        const { formData, formErrors, submitted, changed } = this.state;
        const form = {
          data: formData,
          errors: formErrors,
          validated: this.isFormValid(),
          changed: changed,
          submitted: submitted,
          onChange: this.handleChange,
          onSubmit: this.handleSubmit,
          field: this.formField,
          reset: this.resetForm,
        };
        this.defaults = {};
        return <WrappedComponent {...this.props} form={form} />;
      }

      componentDidUpdate() {

        const { submitted, formData } = this.state;

        if (submitted) this.setState({ submitted: false });

        // Update formData with fields default values if necessary
        let formDataUpdate = false;
        const defaultValues = {};

        Object.keys(this.defaults).forEach(fieldName => {
          if ( !(fieldName in formData) && this.defaults[fieldName] !== undefined ) {
            defaultValues[fieldName] = this.defaults[fieldName];
            formDataUpdate = true;
          }
        });

        if (formDataUpdate) this.setState({ formData: {...formData, ...defaultValues}});

      }

      handleChange(evt) {
        const { name, value, type } = evt.target;
        if (type === "checkbox"){
          let valcheck = value === 'true' ? false : true;
          this.setFieldValue(name, valcheck);
        }else {
          const errorMessage = this.validate(name, value, false);
          if (errorMessage) {
            this.setFieldError(name, errorMessage);
          } else {
            this.unsetFieldError(name);
          }
          this.setFieldValue(name, value);
        }
      }

      handleSubmit(evt) {
        evt.preventDefault();
        const formErrors = this.validateAllFields(true);
        this.setState({formErrors, submitted: true});
      }

      resetForm() {
        this.setState({
          formData: {},
          formErrors: {},
          submitted: false,
          changed: false,
        });
      }

      validate(fieldName, value, submitting) {
        if (!(fieldName in validators)) return true;
        const { formData } = this.state;
        return validators[fieldName](
          value,
          formData,
          submitting
        );
      }

      validateAllFields(submitting) {
        const { formData } = this.state;
        const formErrors = {};
        Object.keys(formData).forEach(fieldName => {
          const errorMessage = this.validate(fieldName, formData[fieldName], submitting);
          if (errorMessage) {
            formErrors[fieldName] = errorMessage;
          }
        });
        return formErrors;
      }

      setFieldValue(fieldName, value) {
        this.setState(state => {
          return {
            formData: {
              ...state.formData,
              [fieldName]: value
            },
            changed: true
          };
        });
      }

      setFieldError(fieldName, message) {
        this.setState(state => {
          return {
            formErrors: {
              ...state.formErrors,
              [fieldName]: message
            }
          };
        });
      }

      unsetFieldError(fieldName) {
        this.setState(state => {
          const formErrors = {...state.formErrors};
          delete formErrors[fieldName];
          return {formErrors};
        });
      }

      isFormValid() {
        return Object.keys(this.state.formErrors).length === 0;
      }

      formField(fieldName, defaultValue) {
        const { formData } = this.state;
        this.defaults[fieldName] = defaultValue;
        return fieldName in formData ? formData[fieldName] : defaultValue;
      }

    };
  };

}
