import React from 'react';
import {
  Card,
  CardContent
} from '@material-ui/core';

export const ServerActions = () => (
  <Card>
    <CardContent>
      Server actions
    </CardContent>
  </Card>
);
