import React, { Component } from 'react';
import { connect } from 'react-redux';

import { alertActions } from '../actions';
import { Snackbar } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import WarningIcon from '@material-ui/icons/Warning';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';

class Alert extends Component {

  handleSnackbarClose() {
    const { dispatch } = this.props;
    return () => dispatch(alertActions.clear());
  }

  render() {
    const { classes, theme, alert } = this.props;
    return (
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        open={!!alert.message}
        onClose={this.handleSnackbarClose()}
        autoHideDuration={5000}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
        message={
          <span id="message-id" style={{display: 'flex'}}>
            {{
              error: (<WarningIcon className={classes.leftIcon} style={{ color: theme.palette.error.main }} />),
              warning: (<WarningIcon className={classes.leftIcon} style={{ color: theme.palette.warning.main }}/>),
              success: (<CheckCircleIcon className={classes.leftIcon} style={{ color: theme.palette.success.main }} />)
            }[alert.type]}
            {alert.message}
          </span>
        }
      />
    );
  }
}

const styles = (theme) => ({
  leftIcon: {
    marginRight: theme.spacing.unit,
    height: theme.spacing.unit * 2.5,
    width: theme.spacing.unit * 2.5,
  }
});

const styledAlert = withStyles(styles, { withTheme: true })(Alert);

function mapStateToProps(state) {
  const { alert } = state;
  return {
    alert
  };
}

const connectedAlert = connect(mapStateToProps)(styledAlert);
export { connectedAlert as Alert };
export default Alert;
