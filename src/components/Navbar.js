import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import {
  LinearProgress,
  Fade,
  Typography,
  IconButton,
  Toolbar,
  AppBar
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { history } from '../helpers';

import { uiActions } from '../actions';

class Navbar extends React.Component {

  handleHistoryBackClick() {
    history.goBack();
  }

  handleDrawerToggleClick() {
    this.props.dispatch(uiActions.toggleDrawer(true));
  }

  render() {
    const { classes, title, loading, navRight, navLeft, backLink, inner, children } = this.props;

    const _navLeft = backLink
      ? <IconButton
        color="inherit"
        aria-label="history back"
        component={typeof(backLink) !== typeof(true) ? Link : null}
        to={typeof(backLink) !== typeof(true) ? backLink : null}
        onClick={typeof(backLink) === typeof(true) && backLink ? this.handleHistoryBackClick.bind(this) : null}
      >
        <ArrowBackIcon />
      </IconButton>
      : !inner && <IconButton
        color="inherit"
        aria-label="open drawer"
        onClick={this.handleDrawerToggleClick.bind(this)}
        className={classes.navIconHide}
      >
        <MenuIcon />
      </IconButton>
    ;

    const toolbar =
      <div className={classes.root}>
        <Fade
          in={loading}
          style={{
            transitionDelay: loading ? '800ms' : '0ms'
          }}
          unmountOnExit
        >
          <LinearProgress color={inner ? 'secondary' : 'primary'} classes={{
            root: classes.progress
          }} />
        </Fade>
        { children
          ? <Toolbar className={classes.toolbar}>
            {children}
          </Toolbar>
          : <Toolbar className={classes.toolbar}>
            {_navLeft}
            {navLeft}
            <Typography className={classes.title} variant="h6" color="inherit" noWrap>
              {title}
            </Typography>
            <div className={classes.navRight}>
              {navRight}
            </div>
          </Toolbar>
        }
      </div>
    ;

    return (
      inner
        ? toolbar
        : <AppBar className={classes.appBar}>
          {toolbar}
        </AppBar>
    );

  }
}

const styles = theme => ({
  appBar: {
    position: 'absolute',
    [theme.breakpoints.up('md')]: {
    },
  },
  navIconHide: {
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  title: {
    flex: 1
  },
  navRight: {
  },
  progress: {
    marginBottom: -5,
    height: 5
  },
  toolbar: {}
});

Navbar.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const { auth } = state;
  const { user } = auth;
  return {
    user
  };
}

const connectedNavbar = connect(mapStateToProps)(Navbar);
const styledNavbar = withStyles(styles, { withTheme: true })(connectedNavbar);
export { styledNavbar as Navbar };
