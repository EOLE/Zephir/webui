import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { withRouter, NavLink } from 'react-router-dom';
import {
  Dashboard as DashboardIcon,
  Dns as ServersIcon,
  List as ServerSelectionIcon,
  BorderHorizontal as TemplatesIcon,
  ExitToApp as LogoutIcon,
  Settings as SettingsIcon,
} from '@material-ui/icons';
import {
  Divider,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ListSubheader,
} from '@material-ui/core';

import bg from '../assets/logo.svg';
import { AppIcon } from './AppIcon';
import { appActions } from '../actions';
import { getInstanceLabel } from '../helpers/apps';

class Sidebar extends React.Component {

  closeApp = function(id) {
    return () => {
      this.props.dispatch(appActions.closeApp(id));
    };
  };

  render() {
    const { classes, user, apps } = this.props;

    const SidebarItem = ({ to, title, exact, icon: Icon, /*onClose*/ }) => (
      <ListItem button component={NavLink} exact={exact} to={to} activeClassName={classes.activeLink}>
        <ListItemIcon className={classes.icon}>
          <Icon />
        </ListItemIcon>
        <ListItemText classes={{ primary: classes.primary }} primary={title} />
        {/* Bouton close
          {onClose &&
          <ListItemSecondaryAction className={classes.secondaryAction}>
            <IconButton color='inherit' onClick={onClose} className={classes.button}>
              <CloseIcon className={classes.icon} />
            </IconButton>
          </ListItemSecondaryAction>
        }*/}
      </ListItem>
    );

    return (
      <div className={classes.drawer}>
        <div className={classes.drawerHeader}>
          <List style={{ padding: 0 }}>
            <ListItem
              button
              className={classes.userLink}
            >
              <div className={classes.userText}>
                <ListItemText
                  classes={{
                    primary: classes.whiteText,
                    secondary: classes.whiteText
                  }}
                  primary={user.username}
                />
              </div>
            </ListItem>
          </List>
        </div>



        <List>
          <SidebarItem
            exact to="/"
            title="Tableau de bord"
            icon={DashboardIcon}
          />
          <SidebarItem
            to="/servers/dashboard"
            title="Serveurs"
            icon={ServersIcon}
          />
          <SidebarItem
            to="/serverselections/dashboard"
            title="Sélections"
            icon={ServerSelectionIcon}
          />
          <SidebarItem
            to="/servermodels/dashboard"
            title="Modèles"
            icon={TemplatesIcon}
          />
        </List>
        <Divider />
        {!!apps.length &&
          <React.Fragment>
            <List subheader={
              <ListSubheader>
                Applications en cours
              </ListSubheader>
            }>
              { apps.map(app =>
                <SidebarItem
                  key={`app-${app.id}`}
                  to={`/apps/${app.id}`}
                  title={getInstanceLabel(app)}
                  icon={() => <AppIcon icon={app.icon} />}
                  onClose={this.closeApp(app.id)}
                />
              )}
            </List>
            <Divider />
          </React.Fragment>
        }
        <List subheader={
          <ListSubheader>
            {user.firstName} {user.lastName}
          </ListSubheader>
        }>
          <SidebarItem
            to="/preferences"
            title="Préférences"
            icon={SettingsIcon}
          />
          <SidebarItem
            to="/logout"
            title="Déconnexion"
            icon={LogoutIcon}
          />
        </List>
      </div>
    );
  }
}

const styles = theme => ({
  activeLink: {
    backgroundColor: theme.palette.primary.main,
    '& $primary, & $icon, & + $secondaryAction $button': {
      color: theme.palette.primary.contrastText,
    },
  },
  userLink: {
    paddingTop: theme.spacing.unit * 3,
    height: 'auto',
    flexDirection: 'column'
  },
  primary: {
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    overflow: 'hidden'
  },
  icon: {},
  button: {},
  secondaryAction: {},
  drawer: {
    height: '100%',
    overflowY: 'auto',
    overflowX: 'hidden'
  },
  drawerHeader: {
    ...theme.mixins.toolbar,
    backgroundImage: `url(${bg}), linear-gradient(135deg, ${theme.palette.primary.light}, ${theme.palette.primary.dark})`,
    backgroundPosition: '-135px -590px',
    backgroundSize: '800px 800px',
    height: '100px',
  },
  avatarWrapper: {
    flex: '0 0 auto',
    width: '100%',
  },
  avatar: {
    //margin: 10,
  },
  bigAvatar: {
    width: 60,
    height: 60,
    backgroundColor: theme.palette.primary.light
  },
  whiteText: {
    color: theme.palette.primary.contrastText,
  },
  userText: {
    padding: `${theme.spacing.unit}px 0`,
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'end'
  },
  arrowIcon: {
    marginBottom: -theme.spacing.unit
  },
  searchBar: {
    background: theme.palette.primary.dark,
    borderRadius: 0
  },
  searchBarInput: {
    color: theme.palette.primary.contrastText
  },
  searchBarIcon: {
    color: theme.palette.primary.contrastText
  },
  searchBarButton: {
    '& $primary, & $icon': {
      color: theme.palette.primary.contrastText,
    },
  }
});

Sidebar.propTypes = {
  classes: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const { auth, apps } = state;
  const { user } = auth;
  return {
    user,
    apps: apps.opened
  };
}

const connectedSidebar = connect(mapStateToProps)(Sidebar);
const styledSidebar = withStyles(styles, { withTheme: true })(connectedSidebar);
const routedSidebar = withRouter(styledSidebar);

export { routedSidebar as Sidebar };
