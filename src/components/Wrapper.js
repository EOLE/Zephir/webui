import React from 'react';
import { withStyles } from '@material-ui/core/styles';

class Wrapper extends React.Component {

  render() {
    const { classes, style, ...props } = this.props;

    return (
      <div className={classes.wrapper} style={style} {...props} />
    );
  }
}

const styles = () => ({
  wrapper: {
    zIndex: 0,
    position: 'absolute',
    width: '100%',
    height: '100%'
  }
});

const styledWrapper = withStyles(styles)(Wrapper);
export { styledWrapper as Wrapper };
