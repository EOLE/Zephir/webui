import React from 'react';
import * as ZephirIcons from '../assets/icons/';
import * as MaterialIcons from '@material-ui/icons';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const appIcons = {
  ...MaterialIcons,
  ...ZephirIcons
};

const AppIcon = ({ icon, classes }) => {
  const suffixedIconName = `${icon}Icon`;
  let Icon;
  if (icon in appIcons) {
    Icon = appIcons[icon];
  } else if (suffixedIconName in appIcons) {
    Icon = appIcons[suffixedIconName];
  } else {
    Icon = appIcons.OpenInBrowser;
  }
  return <Icon className={classes.root} />;
};

AppIcon.propTypes = {
  icon: PropTypes.string,
  classes: PropTypes.object,
};

const styles = () => {
  return {
    root: {}
  };
};

const styledAppIcon = withStyles(styles)(AppIcon);
export { styledAppIcon as AppIcon };