import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
} from '@material-ui/core';

import {
  Temperature,
  Ping,
  TreeMap,
  Services
} from './';

const Sensor = ({ sensor, sm }) => (
  {
    temp: <Temperature sensor={sensor} />,
    ping: <Ping sensor={sensor} />,
    treemap: <TreeMap sensor={sensor} sm={sm} />,
    services: <Services sensor={sensor} />
  }[sensor.type]
);

const styles = theme => ({
  title: {
    marginBottom: theme.spacing.unit * 2,
    fontSize: 14,
    color: theme.palette.text.secondary,
  }
});

const styledSensor = withStyles(styles)(Sensor);
export { styledSensor as Sensor };
