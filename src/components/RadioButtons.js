import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import green from '@material-ui/core/colors/green';
import Radio from '@material-ui/core/Radio';
// import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
// import RadioButtonCheckedIcon from '@material-ui/icons/RadioButtonChecked';

const styles = {
  root: {
    color: green[600],
    '&$checked': {
      color: green[500],
    },
  },
  checked: {},
  size: {
    width: 40,
    height: 40,
  },
  sizeIcon: {
    fontSize: 20,
  },
};

class RadioButtons extends React.Component {
  state = {
    selectedValue: 'a',
  };

  handleChange = event => {
    this.setState({ selectedValue: event.target.value });
  };

  render() {
    const { value } = this.props;

    return (
      <Radio
        checked={this.state.selectedValue === {value}}
        onChange={this.handleChange}
        value={value}
        name="radio-button"
        aria-label="A"
      />
    );
  }
}

RadioButtons.propTypes = {
  classes: PropTypes.object.isRequired,
};

const styledRadioButtons = withStyles(styles)(RadioButtons);
export { styledRadioButtons as RadioButtons };
