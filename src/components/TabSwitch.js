import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { AnimatedSwitch } from './';

const TabSwitch = ({ classes, ...props }) => (
  <AnimatedSwitch
    direction="tab"
    atEnter={{ offset: 100 }}
    atLeave={{ offset: -100 }}
    atActive={{ offset: 0 }}
    mapStyles={(styles, direction) => ({
      transform: `translateX(${styles.offset * direction}%)`,
      height: '100%',
      width: '100%',
      position: 'absolute'
    })}
    className={classes.switch}
    {...props}
  />
);

const styles = () => ({
  switch: {
    position: 'relative',
    height: '100%',
    width: '100%'
  }
});

const styledTabSwitch = withStyles(styles)(TabSwitch);
export { styledTabSwitch as TabSwitch };
