import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { AnimatedSwitch } from './';

const SlidingSwitch = ({ classes, ...props }) => (
  <AnimatedSwitch
    direction="nav"
    atEnter={{ offset: 100 }}
    atLeave={{ offset: -50 }}
    atActive={{ offset: 0 }}
    mapStyles={(styles, direction) => ({
      transform: `translateX(${styles.offset * direction}%)`,
      height: '100%',
      width: '100%',
      position: 'absolute'
    })}
    className={classes.switch}
    {...props}
  />
);

const styles = () => ({
  switch: {
    position: 'relative',
    height: '100%',
    width: '100%'
  }
});

const styledSlidingSwitch = withStyles(styles)(SlidingSwitch);
export { styledSlidingSwitch as SlidingSwitch };
