import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import { selectAllServers, selectServerSelectionById } from '../selectors';
import Infinite from 'react-infinite';
import { ServersListItem, } from '../pages/ServersPage';
import {
  Grid,
  Typography,
  List,
} from '@material-ui/core';

class ServerPicker extends React.Component {

  constructor(props) {
    super(props);

    let list_server = selectAllServers(this.props.servers).map(server => server.serverid)
    let list_ss = (this.props.serverselection && this.props.serverselection.serverselectionserversid) || []
    let difference = list_server.filter(x => !list_ss.includes(x))

    this.state = {
        boxOne: difference,
        boxTwo: list_ss
    };
    this.handleEvent = this.handleEvent.bind(this);
  }


  handleEvent(itemId) {
    const isInBoxOne = this.state.boxOne.includes(itemId);
    // Heres the magic, if the item is in the first Box, filter it out,
    // and put into the second, otherwise the other way around..
    this.setState({
        boxOne: isInBoxOne
          ? this.state.boxOne.filter(i => i !== itemId)
          : [ ...this.state.boxOne, itemId ],
        boxTwo: isInBoxOne
          ? [ ...this.state.boxTwo, itemId ]
          : this.state.boxTwo.filter(i => i !== itemId)
    }, this.handleList);

  }
  handleList(){
    this.props.handleSrvList(this.state.boxTwo)
  }

  render() {
    // eslint-disable-next-line
    const { classes, servers, lastRefresh, serverselection, ...otherProps } = this.props;
    const srv = selectAllServers(servers)
    return (
      <React.Fragment>
        <Grid item xs={12} sm={6}>
          <Typography variant="subtitle1">Serveurs Disponibles</Typography>
          <Infinite
          containerHeight={400}
          elementHeight={57}>
            <ItemList dense={true} classes={classes} handleevent={this.handleEvent} items={this.state.boxOne} allItems={srv} />
          </Infinite>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Typography variant="subtitle1">Serveurs de la sélection</Typography>
          <Infinite

          containerHeight={400}
          elementHeight={57}>
            <ItemList dense={true} classes={classes} handleevent={this.handleEvent} items={this.state.boxTwo} allItems={srv} />
          </Infinite>
        </Grid>
      </React.Fragment>
    );
  }
}

class ItemList extends React.Component{

   render() {
     let itemArr = this.props.allItems;
     let myItems = this.props.items;
     let handleEvent = this.props.handleevent;


     let listItems = itemArr.map((server) => {
        if (!myItems.includes(server.serverid)) return null;

        return <ServersListItem
                key={server.serverid}
                server={server}
                onClick={() => handleEvent(server.serverid)}
                />
     });
     return (
      <List dense={true} handleevent={this.handleEvent}>
            {listItems}
      </List>
     );
   }
}


const styles = () => ({
  ul: {
    backgroundColor: 'inherit',
    padding: 0,
  }
});

const mapStateToProps = ({ servers, serverselections }, { match }) => {

  let serverselectionId = match.params.id;
  if (serverselectionId === "new") return { serverselectionId: null };

  serverselectionId =  parseInt(serverselectionId, 10);
  return {
    servers: servers.byId,
    serverselection: selectServerSelectionById(serverselections.byId, serverselectionId),
    lastRefresh: servers.lastRefresh
  };
};

const connectedServerPicker = connect(mapStateToProps)(ServerPicker);
const styledServerPicker = withStyles(styles)(connectedServerPicker);
const routedServerForm = withRouter(styledServerPicker);
export { routedServerForm as ServerPicker };
