import React from 'react';
import { withStyles } from '@material-ui/core/styles';

import {
  Grid,
  Typography
} from '@material-ui/core';

import {
  Sensor
} from './';

import sensorsData from '../helpers/fake-api/sensors.data';

const ServerSensors = ({ classes, sm }) => (
  <Grid container spacing={0} className={classes.container}>
    <Grid item xs={12}>
      <Typography variant="h5" component="h3">
        DONNÉES FACTICES
      </Typography>
    </Grid>
    {sensorsData.map((sensor, index) =>
      <Grid item
        key={`sensor-${index}`}
        xs={sensor.type === 'treemap' ? 12 : true}
        sm={sensor.type === 'treemap' ? 12 : true}
        md={sensor.type === 'treemap' ? 12 : true}
        lg={sensor.type === 'treemap' ? (sm ? 12 : 8) : true}
        className={classes.item}>
        <Sensor sensor={sensor} sm={sm} />
      </Grid>
    )}
  </Grid>
);

const styles = theme => ({
  container: {
    padding: theme.spacing.unit
  },
  item: {
    padding: theme.spacing.unit
  }
});

const styledServerSensors = withStyles(styles)(ServerSensors);
export { styledServerSensors as ServerSensors };
