import React from 'react';
import {
  TextField,
  MenuItem,
  Divider,
  InputAdornment,
  IconButton,
  Select,
  Chip
} from '@material-ui/core';
import CachedIcon from '@material-ui/icons/Cached';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { selectAllServerModels } from '../selectors';
import { servermodelActions } from '../actions';

// Un sélecteur pour les entités ServerModel
class ServerModelSelector extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      servermodelselected: []
    };

    this.handleServermodelRefreshClick = this.handleServermodelRefreshClick.bind(this);
    this.handleSelectedChange = this.handleSelectedChange.bind(this);
  }

  componentDidMount() {
    const now = new Date();
    const isStaled = !this.props.lastRefresh || now - this.props.lastRefresh > 5000;
    if (isStaled) this.props.dispatch(servermodelActions.list());
  }

  handleSelectedChange(event) {
    let joined = this.state.servermodelselected.concat(event.target.value);
    this.setState({ servermodelselected: joined });
  }
  render() {
    // eslint-disable-next-line
    const { classes, servermodelGroups, lastRefresh, multi, ...otherProps } = this.props;
    const {servermodelselected} = this.state;
    if (multi === 'true') {
      return (
        <Select
          {...otherProps}
          multiple
          value={servermodelselected}
          onChange={this.handleSelectedChange}
          renderValue={selected => (
            <div>
              {selected.map(value => (
                <Chip key={value} label={value}  />
              ))}
            </div>
          )}
        >
          {
            Object.keys(servermodelGroups).map(groupName => {
              return [...this.renderServerModelsGroup(groupName, servermodelGroups[groupName]), <Divider key={`divider-${groupName}`} />];
            })
          }
        </Select>
      );
    }else {
      return (
        <TextField {...otherProps} select
          SelectProps={{
            MenuProps: {
              className: classes.menu,
            },
          }}
          InputProps={{
            endAdornment:
            <InputAdornment position="end">
              <IconButton disabled={otherProps.disabled}
                onClick={this.handleServermodelRefreshClick}>
                <CachedIcon />
              </IconButton>
            </InputAdornment>
          }}
          disabled={otherProps.disabled}
        >
          {
            Object.keys(servermodelGroups).map(groupName => {
              return [...this.renderServerModelsGroup(groupName, servermodelGroups[groupName]), <Divider key={`divider-${groupName}`} />];
            })
          }
        </TextField>
      );
    }


  }

  renderServerModelsGroup(groupName, servermodels) {
    return servermodels.map(servermodel => (
      <MenuItem key={`servermodel-${servermodel.model.servermodelid}`}
        value={servermodel.model.servermodelid}>
        {servermodel.model.servermodelname} {servermodel.model.subreleasename} - {groupName}
      </MenuItem>
    ));
  }

  handleServermodelRefreshClick() {
    this.props.dispatch(servermodelActions.list());
  }

}

const styles = () => ({
  ul: {
    backgroundColor: 'inherit',
    padding: 0,
  }
});


export function groupBySourceAndVersion(models) {
  return models.reduce((groups, m) => {
    const groupName = `${m.servermodelversion.versiondistribution} ${m.servermodelversion.versionname} - ${m.servermodelsource.sourcename}`;
    if (!(groupName in groups)) groups[groupName] = [];
    groups[groupName].push(m);
    return groups;
  }, {});
}

const mapStateToProps = ({ servermodels }) => {
  // TEMP contournement temporaire, à corriger
  // quand servermodel.describe retournera les informations
  // complètes, voir groupBySourceAndVersion
  const servermodelGroups = {
    'Pôle EOLE': selectAllServerModels(servermodels.byId)
  };
  return {
    servermodelGroups,
    lastRefresh: servermodels.lastRefresh
  };
};

const connectedServerModelSelector = connect(mapStateToProps)(ServerModelSelector);
const styledServerModelSelector = withStyles(styles)(connectedServerModelSelector);
export { styledServerModelSelector as ServerModelSelector };
