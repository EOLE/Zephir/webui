import React from 'react';
import Media from 'react-media';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';

import { Button, IconButton } from '@material-ui/core';

class ResponsiveButton extends React.Component {

  render() {
    const { classes, theme, title, icon: Icon, side, sm: forceSm, ...props } = this.props;

    return (
      <Media query={{ maxWidth: theme.breakpoints.values.sm }}>
        {sm => sm || forceSm
          ? <IconButton
            className={classNames(classes.button, classes.smallButton)}
            color="inherit"
            {...props}
          >
            <Icon
              className={classNames(classes.icon, classes.leftIcon)}
            />
          </IconButton>
          : <Button
            className={classNames(classes.button, classes.largeButton)}
            color="inherit"
            {...props}
          >
            {side !== 'right' &&
                <Icon
                  className={classNames(classes.icon, classes.leftIcon)}
                />
            }
            {title}
            {side === 'right' &&
                <Icon
                  className={classNames(classes.icon, classes.rightIcon)}
                />
            }
          </Button>
        }
      </Media>
    );
  }
}

const styles = theme => ({
  button: {},
  smallButton: {
    '&:disabled $icon': {
      color: theme.palette.primary.contrastText
    }
  },
  largeButton: {},
  icon: {},
  leftIcon: {
    marginRight: theme.spacing.unit
  },
  rightIcon: {
    marginLeft: theme.spacing.unit
  }
});

const styledResponsiveButton = withStyles(styles, { withTheme: true })(ResponsiveButton);

export { styledResponsiveButton as ResponsiveButton };
