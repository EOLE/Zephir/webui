import React from 'react';
import Route from 'react-router-dom/Route';
import Switch from 'react-router-dom/Switch';
import matchPath from 'react-router-dom/matchPath';
import PropTypes from 'prop-types';

import { RouteTransition } from './';

const NO_MATCH = {
  key: 'no-match',
};

/**
 * Not every location object has a `key` property (e.g. HashHistory).
 */
function getLocationKey(location) {
  return typeof location.key === 'string' ? location.key : '';
}

/**
 * Some superfluous work, but something we need to do in order
 * to persist matches/allow for nesting/etc.
 */
function getMatchedRoute(children, pathname) {
  let index = 0;
  return {
    route: React.Children.toArray(children).find((child, i) => {
      const match = matchPath(pathname, {
        exact: child.props.exact,
        path: child.props.path,
      });
      if(match){
        index = i;
      }
      return match;
    }) || NO_MATCH,
    index: index
  };
}

class AnimatedSwitch extends React.Component {
  static propTypes = {
    location: PropTypes.shape({
      key: PropTypes.string,
      pathname: PropTypes.string,
    }),
  };

  state = {
    location: {
      pathname: ''
    },
    direction: 0,
    key: getLocationKey(this.props.location),
    match: getMatchedRoute(this.props.children, this.props.location.pathname),
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const nextMatch = getMatchedRoute(nextProps.children, nextProps.location.pathname );

    if (prevState.match.route.key !== nextMatch.route.key) {

      const path = prevState.location.pathname;
      const level = path === '/' ? 1 : path.split('/').length;
      const index = prevState.match.index;

      const nextPath = nextProps.location.pathname;
      const nextLevel = nextPath === '/' ? 1 : nextPath.split('/').length;
      const nextIndex = nextMatch.index;

      return {
        direction: nextProps.direction === 'nav'
          ? nextLevel - level
          : nextProps.direction === 'tab'
            ? nextIndex - index
            : 0,
        match: nextMatch,
        location: nextProps.location,
        matches: prevState.matches+1,
        key: getLocationKey(nextProps.location) + (prevState.matches+1),
      };
    }

    return null;
  }

  render() {
    const { children, location, ...routeTransitionProps } = this.props;

    return (
      <RouteTransition {...routeTransitionProps} direction={this.state.direction}>
        <Switch key={this.state.key} location={location}>
          {children}
        </Switch>
      </RouteTransition>
    );
  }
}

// inject location as a prop so we can listen for changes
// TODO: use withRouter instead
const RouteWrapper = props => (
  <Route>
    {({ location }) => (
      <AnimatedSwitch location={location} {...props} />
    )}
  </Route>
);

export { RouteWrapper as AnimatedSwitch };
