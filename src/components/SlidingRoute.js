import React from 'react';
import { withStyles } from '@material-ui/core/styles';

import { AnimatedRoute } from './';

class SlidingRoute extends React.Component {

  render() {
    const { classes, ...props } = this.props;

    return (
      <AnimatedRoute
        atEnter={{ offset: 100, zIndex: 0 }}
        atLeave={{ offset: 100, zIndex: 0 }}
        atActive={{ offset: 0, zIndex: 2 }}
        wrapperComponent={'div'}
        mapStyles={(styles) => ({
          transform: `translateX(${styles.offset}%)`
        })}
        className={classes.animatedRoute}
        {...props}
      />
    );
  }
}

const styles = () => ({
  animatedRoute: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  }
});

const styledSlidingRoute = withStyles(styles)(SlidingRoute);
export { styledSlidingRoute as SlidingRoute };

