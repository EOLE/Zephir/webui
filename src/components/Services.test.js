/* eslint-env jest */
import React from 'react';
import renderer from 'react-test-renderer';
import { Services } from './Services';
import { theme } from '../helpers/theme';
import { MuiThemeProvider } from '@material-ui/core/styles';

it('should match snapshot', () => {

  const sensor = {
    name: 'Services',
    type: 'services',
    data: [
      { name: 'apache2', status: 'ok', at: new Date() },
      { name: 'elasticsearch', status: 'ok', at: new Date() },
      { name: 'docker', status: 'ok', at: new Date() },
    ]
  };

  const component = renderer.create(
    <MuiThemeProvider theme={theme}>
      <Services sensor={sensor} />
    </MuiThemeProvider>
  );

  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();

});
