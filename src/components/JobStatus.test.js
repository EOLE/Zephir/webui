/* eslint-env jest */
import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import { JobStatus } from './JobStatus';

describe('JobStatus', () => {

  // Fix "TypeError: parentInstance.children.indexOf is not a function"
  // See https://github.com/facebook/react/issues/11565
  beforeAll(() => {
    ReactDOM.createPortal = jest.fn(element => {
      return element;
    });
  });

  it('should match snapshot', () => {

    let component = renderer.create(
      <JobStatus status={JobStatus.SUCCESS} />
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();

    component = renderer.create(
      <JobStatus status={JobStatus.ERROR} />
    );

    tree = component.toJSON();
    expect(tree).toMatchSnapshot();

    component = renderer.create(
      <JobStatus status={JobStatus.PENDING} />
    );

    tree = component.toJSON();
    expect(tree).toMatchSnapshot();

  });

});


