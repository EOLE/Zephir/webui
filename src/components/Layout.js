import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import { Drawer, Hidden } from '@material-ui/core';

import { Sidebar } from './';

import { uiActions } from '../actions';

class Layout extends React.Component {

  handleDrawerToggle = () => {
    this.props.dispatch(uiActions.toggleDrawer(false));
  };

  handleLocationChange = () => {
    if(this.props.ui.drawerOpen){
      this.props.dispatch(uiActions.toggleDrawer(false));
    }
  };

  componentDidMount() {
    this.props.history.listen(this.handleLocationChange);
  }

  render() {
    const { children, classes, theme, ui } = this.props;

    return (
      <div className={classes.root}>
        <div className={classes.appFrame}>
          <Hidden mdUp>
            <Drawer
              variant="temporary"
              anchor={theme.direction === 'rtl' ? 'right' : 'left'}
              open={ui.drawerOpen}
              classes={{
                paper: classes.drawerPaper,
              }}
              onClose={this.handleDrawerToggle}
              ModalProps={{
                keepMounted: true, // Better open performance on mobile.
              }}
            >
              <Sidebar />
            </Drawer>
          </Hidden>
          <Hidden smDown implementation="css">
            <Drawer
              variant="permanent"
              open
              classes={{
                paper: classes.drawerPaper,
              }}
            >
              <Sidebar />
            </Drawer>
          </Hidden>
          <main className={classes.content}>
            {children}
          </main>
        </div>
      </div>
    );
  }
}

const styles = theme => ({
  root: {
    width: '100%',
    height: '100%',
    zIndex: 1,
    overflow: 'hidden',
  },
  logo: {
    height: theme.spacing.unit * 8,
    marginTop: theme.spacing.unit
  },
  appFrame: {
    position: 'relative',
    display: 'flex',
    width: '100%',
    height: '100%',
  },
  drawerContainer: {
    overflowY: 'auto'
  },
  drawerPaper: {
    width: 250,
    [theme.breakpoints.up('md')]: {
      width: theme.mixins.sidebar.width,
      position: 'relative',
    },
  },
  content: {
    backgroundColor: "#CCC", //theme.palette.background.default,
    width: '100%',
    height: '100%',
    position: 'relative'
    //padding: theme.spacing.unit * 3,
  },
});

Layout.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  const { auth, ui } = state;
  const { user } = auth;
  return {
    ui,
    user
  };
}

const connectedLayout = connect(mapStateToProps)(Layout);
const styledLayout = withStyles(styles, { withTheme: true })(connectedLayout);
const routedLayout = withRouter(styledLayout);
export { routedLayout as Layout };
