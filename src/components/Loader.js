import React from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { CircularProgress } from '@material-ui/core';

class Loader extends React.Component {

  render() {
    const { classes, style, className, ...props } = this.props;

    return (
      <div className={classNames(classes.root, className)} style={style}>
        <CircularProgress {...props} />
      </div>
    );
  }
}

const styles = () => ({
  root: {
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  }
});

const styledLoader = withStyles(styles)(Loader);
export { styledLoader as Loader };
