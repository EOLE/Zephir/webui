import React from 'react';
import Route from 'react-router-dom/Route';
import matchPath from 'react-router-dom/matchPath';

import { RouteTransition } from './';

/**
 * Here we only care about whether or not the pathname matches. If so,
 * we'll use the route's path as the key, otherwise we'll default it
 * to a string signifying no match.
 */
function getKey({ pathname }, path, exact) {
  return matchPath(pathname, { exact, path }) ? 'match' : 'no-match';
}

const AnimatedRoute = ({ component, path, exact, render, children, ...routeTransitionProps }) => (
  <Route
    render={({ location }) => (
      <RouteTransition {...routeTransitionProps}>
        <Route
          key={getKey(location, path, exact)}
          path={path}
          exact={exact}
          location={location}
          component={component}
          render={render}
        >
          {children}
        </Route>
      </RouteTransition>
    )}
  />
);

export { AnimatedRoute };
