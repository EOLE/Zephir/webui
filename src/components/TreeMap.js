import React from 'react';
import Media from 'react-media';
import { withStyles } from '@material-ui/core/styles';
import { Treemap, ResponsiveContainer } from 'recharts';
import {
  Card,
  CardHeader,
  CardContent,
} from '@material-ui/core';

import {
  red, pink, purple,
  deepPurple, indigo, blue,
  lightBlue, cyan, teal,
  green, lightGreen, lime,
  yellow, amber, orange,
  deepOrange, brown, grey,
  blueGrey
} from '@material-ui/core/colors';

import {
  ViewQuilt as TreemapIcon
} from '@material-ui/icons';

import { convertSize } from '../helpers/utils';

class CustomizedContent extends React.Component {

  state = {
    isHover: false
  };

  toggleHover(isHover) {
    return () => {
      const { name, size } = this.props;
      this.setState({ isHover });
      this.props.onHover(isHover ? { name, size } : {});
    };
  }

  render() {
    //const { root, rank, payload }
    const { depth, x, y, width, height, index, colors, name, size, theme } = this.props;
    const { isHover } = this.state;

    return (
      <g>
        <rect
          ref={(ref) => this.rect = ref}
          x={x}
          y={y}
          width={width}
          height={height}
          onMouseEnter={this.toggleHover(true)}
          onMouseLeave={this.toggleHover(false)}
          style={{
            fill: !name
              ? theme.palette.grey[200]
              : isHover
                ? theme.palette.action.hover
                : depth < 2
                  ? colors[index]
                  : 'none',
            pointerEvents: 'all',
            stroke: '#fff',
            strokeWidth: 2 / (depth + 1e-10),
            strokeOpacity: 0.5 / (depth + 1e-10),
          }}
        >
        </rect>
        {
          depth === 1 ?
            <text
              x={x + width / 2}
              y={y + height / 2 + 7}
              textAnchor="middle"
              fill={name ? "#fff" : theme.palette.text.disabled}
              stroke={name ? "#fff" : theme.palette.text.disabled}
              fontSize={11}
            >
              {convertSize(size)}
            </text>
            : null
        }
        {
          depth === 1 ?
            <text
              x={x + 8}
              y={y + 24}
              fill={name ? "#fff" : theme.palette.text.disabled}
              stroke={name ? "#fff" : theme.palette.text.disabled}
              fontSize={14}
              fillOpacity={0.9}
            >
              {name || 'Libre'}
            </text>
            : null
        }
      </g>
    );
  }
}

const COLORS = [
  pink[900],
  teal[500],
  blueGrey[500],
  indigo[500],
  orange[500],
  purple[500],
  deepOrange[500],
  red[500],
  amber[700],
  blue[500],
  green[500],
  lightBlue[500],
  yellow[500],
  lime[900],
  cyan[500],
  lightGreen[500],
  brown[500],
  deepPurple[700],
  grey[500],
];

class TreeMap extends React.Component {

  state = {
    name: null,
    size: null,
    hideForResize: false
  };

  constructor(props) {
    super(props);
    this.freeze = this.freeze.bind(this);
  }

  handleHover(rect={}){
    const { name, size } = rect;
    this.setState({
      name, size
    });
  }

  componentDidMount() {
    window.addEventListener("resize", this.freeze);
    this.freeze();
  }

  componentWillUnmount() {
    this.unmounting = true;
    window.removeEventListener("resize", this.freeze);
  }

  // FIXME: getDerivedStateFromProps
  UNSAFE_componentWillReceiveProps(nextProps){
    if(this.props.sm !== nextProps.sm){
      this.freeze();
    }
  }

  freeze(){
    this.setState({
      hideForResize: true
    });
    setTimeout(() => {
      if (this.unmounting) return;
      this.setState({ hideForResize: false});
    }, 300);
  }

  render() {
    const { sensor, theme, sm } = this.props;
    const { name, size, hideForResize } = this.state;

    return (
      <Media query={{ minWidth: theme.breakpoints.values.lg }}>
        {lg =>
          <Card style={{ width: "100%" }}>
            <CardHeader
              avatar={
                <TreemapIcon style={{ color: theme.palette.success.main }}/>
              }
              title={sensor.name}
              subheader={name && size
                ? <span><code>{name}</code> : <strong>{convertSize(size)}</strong></span>
                : <em>Sélectionnez un espace</em>
              }
            />
            {!hideForResize &&
              <CardContent style={{ width: "100%", padding: 0 }}>
                <ResponsiveContainer width="100%" aspect={lg && !sm ? 16/9: 1}>
                  <Treemap
                    isAnimationActive={false}
                    animationBegin={0}
                    animationDuration={200}
                    data={sensor.data[0].value}
                    dataKey="size"
                    ratio={16/9}
                    content={
                      <CustomizedContent
                        colors={COLORS}
                        theme={theme}
                        onHover={this.handleHover.bind(this)}
                      />
                    }
                  />
                </ResponsiveContainer>
              </CardContent>
            }
          </Card>
        }
      </Media>
    );
  }
}

const styles = () => ({

});

const styledTreeMap = withStyles(styles, { withTheme: true })(TreeMap);
export { styledTreeMap as TreeMap };
