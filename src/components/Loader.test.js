/* eslint-env jest */
import React from 'react';
import renderer from 'react-test-renderer';
import { Loader } from './Loader';

it('should match snapshot', () => {
  const component = renderer.create(<Loader />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
