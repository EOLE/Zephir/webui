import React from 'react';
import { connect } from 'react-redux';

class RouteDataLoader extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      isMounting: true,
    };
    this._abortUpdate = false;
  }

  render() {
    const { isLoading, isMounting } = this.state;
    return React.cloneElement(this.props.children, {
      ...this.props,
      isLoading: isLoading || isMounting
    });
  }

  componentDidMount() {
    this.setState({ isMounting: false }, () => {
      this.loadData();
    });
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.url !== prevProps.match.url && !this.state.isLoading) {
      this.loadData();
    }
  }

  componentWillUnmount() {
    this._abortUpdate = true;
  }

  loadData() {
    const { dispatch, match } = this.props;
    this.setState({ isLoading: true }, () => {
      this.props.loadData(match, dispatch)
        .then(() => {
          if (this._abortUpdate) return;
          this.setState({ isLoading: false });
        });
    });
  }

}

const connectedRouteDataLoader = connect()(RouteDataLoader);
export { connectedRouteDataLoader as RouteDataLoader };
