import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Grid
} from '@material-ui/core';

const VSplit = ({ left, right, leftProps, rightProps, classes, ...props }) => (
  <Grid container spacing={0} className={classes.container} {...props}>

    <Grid item xs={12} sm={4} md={4} lg={3} className={classes.leftColumn} {...leftProps}>
      {left}
    </Grid>

    <Grid item xs={12} sm={8} md={8} lg={9} className={classes.rightColumn} {...rightProps}>
      {right}
    </Grid>
  </Grid>
);

const styles = theme => ({
  container: {
    height: '100%',
    flexGrow: 1,
    position: 'relative',
    flexDirection: 'row'
  },
  leftColumn: {
    backgroundColor: theme.palette.background.default,
    color: theme.palette.primary.contrastText,
    overflowY: 'auto',
    maxHeight: '100%',
    position: 'relative',
    transition: 'flex-basis .2s, max-width .2s'
  },
  rightColumn: {
    backgroundColor: theme.palette.background.paper,
    overflowY: 'auto',
    maxHeight: '100%',
    position: 'relative',
    transition: 'flex-basis .2s, max-width .2s'
  }
});

const styledVSplit = withStyles(styles)(VSplit);
export { styledVSplit as VSplit };
