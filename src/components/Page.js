import React from 'react';
import classNames from 'classnames';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router';

import { Navbar } from './';

class Page extends React.Component {

  render() {
    const {
      children, classes, style, contentStyle, ...props
    } = this.props;

    return (
      <div className={classes.page} style={style}>
        <Navbar classes={{
          appBar: classes.appBar
        }} {...props} />
        <main style={contentStyle} className={props.inner
          ? classNames(classes.content, classes.innerContent)
          : classNames(classes.content, classes.outerContent)
        }>
          {children}
        </main>
      </div>
    );
  }
}

const styles = theme => ({
  page: {
    zIndex: 1,
    width: '100%',
    height: '100%',
    overflow: 'hidden',
    position: 'absolute',
    backgroundColor: theme.palette.background.paper,
  },
  innerContent: {

  },
  outerContent: {
    marginTop: 56,
    [theme.breakpoints.up('sm')]: {
      marginTop: 64,
    },
  },
  content: {
    position: 'relative',
    width: '100%',
    height: 'calc(100% - 56px)',
    [theme.breakpoints.up('sm')]: {
      height: 'calc(100% - 64px)',
    },
    overflow: 'auto'
  },
});

Page.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};


function mapStateToProps(state) {
  const { auth } = state;
  const { user } = auth;
  return {
    user
  };
}

const connectedPage = connect(mapStateToProps)(Page);
const styledPage = withStyles(styles, { withTheme: true })(connectedPage);
const routedPage = withRouter(styledPage);
export { routedPage as Page };
