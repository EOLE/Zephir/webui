import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';


class SimpleSelect extends React.Component {
  state = {
    lang: '',
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { label, lang1, lang2, lang3, style } = this.props;
    return (
      <form autoComplete="off">
        <FormControl>
          <InputLabel htmlFor="langue" >{label}</InputLabel>
          <Select
            style={style}
            value={this.state.lang}
            onChange={this.handleChange}
            inputProps={{
              name: 'lang',
              id: 'langue',
            }}
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            <MenuItem value={1}>{lang1}</MenuItem>
            <MenuItem value={2}>{lang2}</MenuItem>
            <MenuItem value={3}>{lang3}</MenuItem>
          </Select>
        </FormControl>
      </form>
    );
  }
}

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
});

Select.propTypes = {
  classes: PropTypes.object.isRequired,
};

const styledSimpleSelect = withStyles(styles)(SimpleSelect);
export { styledSimpleSelect as SimpleSelect };
