import React from 'react';
import { withStyles } from '@material-ui/core/styles';

import { Wrapper, AnimatedRoute } from './';

class FadingRoute extends React.Component {

  render() {
    const { classes, ...props } = this.props;

    return (
      <AnimatedRoute
        atEnter={{ opacity: 0 }}
        atLeave={{ opacity: 0 }}
        atActive={{ opacity: 1 }}
        wrapperComponent={Wrapper}
        className={classes.animatedRoute}
        {...props}
      />
    );
  }
}

const styles = () => ({
  animatedRoute: {
    //position: 'absolute',
    //height: '100%',
    //zIndex: 1
  }
});

const styledFadingRoute = withStyles(styles)(FadingRoute);
export { styledFadingRoute as FadingRoute };

