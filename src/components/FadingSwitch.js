import React from 'react';
import { AnimatedSwitch } from './';

export const FadingSwitch = (props) => (
  <AnimatedSwitch
    atEnter={{ opacity: 0 }}
    atLeave={{ opacity: 0 }}
    atActive={{ opacity: 1 }}
    className="switch-wrapper"
    {...props}
  />
);

