import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import {
  Card,
  CardHeader,
  List,
  ListItem,
  ListItemText,
  ListItemIcon
} from '@material-ui/core';

import {
  SettingsApplications as ServicesIcon,
  Lens as ServiceIcon
} from '@material-ui/icons';

class Services extends React.Component {

  getServiceColor(service) {
    const { theme } = this.props;
    return {
      ok: theme.palette.success.main,
      warning: theme.palette.warning.main,
      error: theme.palette.error.main
    }[service.status] || theme.palette.text.disabled;
  }

  render() {
    const { sensor, theme, classes } = this.props;

    return (
      <Card className={classes.card}>
        <CardHeader
          style={{color: theme.palette.warning.main }}
          avatar={
            <ServicesIcon color="inherit" />
          }
          title={sensor.name}
        />
        <List>
          {sensor.data.map((service, index) =>
            <ListItem key={`service-${index}`} button>
              <ListItemIcon style={{ color: this.getServiceColor(service) }}>
                <ServiceIcon />
              </ListItemIcon>
              <ListItemText inset primary={service.name} />
            </ListItem>
          )}
        </List>
      </Card>
    );
  }

}

const styles = () => ({

});

const styledServices = withStyles(styles, { withTheme: true })(Services);
export { styledServices as Services };
