import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Card,
  CardHeader,
  CardContent,
  Typography,
} from '@material-ui/core';

import {
  Warning as WarningIcon,
  CallMissedOutgoing as PingIcon
} from '@material-ui/icons';

import {
  Sparklines,
  SparklinesLine,
  SparklinesReferenceLine,
} from 'react-sparklines';

const LIMIT = 50;
const MAX = 1500;
const MIN = 0;

class Ping extends React.Component {

  state = { data: [], ping: 60 };

  _updatePing() {
    let offset = Math.floor(Math.random() * 10) + 1;
    let offset2 = Math.floor(Math.random() * 10) + 1;
    offset = !(offset2 % 2) ? -offset*offset :
      !(offset2 % 5) ? -offset*offset*offset :
        !(offset2 % 7) ? offset*offset*offset : offset;
    let ping = this.state.ping + offset;
    ping = ping <= 0 ? 24 :
      ping > 1500 ? 1500 :
        ping ;
    this.setState({
      data: [
        ...this.state.data,
        1500-ping
      ],
      ping
    });
  }

  componentDidMount(){
    this.fakeLive = setInterval(this._updatePing.bind(this), 1000);
  }

  componentWillUnmount(){
    clearInterval(this.fakeLive);
  }

  render() {
    const { classes, theme, sensor } = this.props;

    const sparklineColor = !this.state.data.length
      ? theme.palette.text.disabled
      : this.state.ping > 1000
        ? theme.palette.error.main
        : this.state.ping > 100
          ? theme.palette.warning.main
          : theme.palette.success.main
    ;

    return (
      <Card className={classes.card}>
        <CardHeader
          style={{color: sparklineColor }}
          avatar={
            <PingIcon style={{
              fontSize: 'inherit',
              transform: 'rotate(-90deg)'
            }}/>
          }
          title={sensor.name}
        />
        <CardContent style={{ padding: 0 }}>
          <div className={classes.sparkslineContainer}>
            <Sparklines data={this.state.data} limit={LIMIT} min={MIN} max={MAX}>
              <SparklinesLine color={sparklineColor} />
              <SparklinesReferenceLine type="max" />
            </Sparklines>
            {this.state.ping > 1000
              ? <WarningIcon size={theme.typography.fontSize*4} className={classes.ping} style={{
                color: sparklineColor,
                opacity: .5,
                marginTop: theme.spacing.unit,
                height: theme.typography.fontSize * 4,
                width: 'auto'
              }}/>

              : <Typography variant="display1" component="div" className={classes.ping} style={{
                color: sparklineColor, opacity: .5
              }}>
                {this.state.ping}ms
              </Typography>
            }
          </div>
        </CardContent>
      </Card>
    );
  }
}

const styles = theme => ({
  root: {

  },
  card: {
    minWidth: 150
  },
  actions: {
    display: 'flex',
  },
  listButton: {
    justifyContent: 'left',
  },
  rightActions: {
    marginLeft: 'auto',
  },
  leftIcon: {
    marginRight: theme.spacing.unit
  },
  sparkslineContainer: {
    position: 'relative',
    display: 'flex',
    height: theme.spacing.unit * 10,
    '& svg': {
      width: '100%'
    }
  },
  ping: {
    //position: 'absolute',
    //top: 0,
    paddingRight: theme.spacing.unit * 2,
    paddingLeft: theme.spacing.unit,
    position: 'absolute',
    zIndex: 10,
    right: 0
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
});

const styledPing = withStyles(styles, { withTheme: true })(Ping);
export { styledPing as Ping };
