import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Button, Grid, Typography, Paper, CircularProgress } from '@material-ui/core';
import { JobsTable } from './JobsTable';
import { execActions } from '../../actions';
import { Refresh as RefreshIcon } from '@material-ui/icons';
import classNames from 'classnames';
import { Loader } from '../';

class ActionApp extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      jobs: [],
      isLoading: false,
      jobsOutdated: false
    };
    this.onRefreshClick = this.onRefreshClick.bind(this);
  }

  static getDerivedStateFromProps(nextProps) {
    return { jobsOutdated: nextProps.jobsOutdated };
  }

  componentDidUpdate() {
    const { jobsOutdated, isLoading } = this.state;
    if (jobsOutdated && !isLoading) this.refreshJobs();
  }

  componentDidMount() {
    const { jobsOutdated, isLoading } = this.state;
    if (jobsOutdated && !isLoading) this.refreshJobs();
  }

  render() {
    const { classes, actionsBar } = this.props;
    const { jobs, isLoading } = this.state;

    if (isLoading && jobs.length === 0) return <Loader />;

    return (
      <div className={classes.root}>
        <Grid container>
          <Grid className={classes.actionsBar} container justify="flex-end">
            { actionsBar }
          </Grid>
          <Grid className={classes.historyContainer} container>
            <Paper className={classes.history}>
              <Grid container>
                <Grid item sm={9} xs={6}>
                  <Typography variant="h5">Historique des opérations</Typography>
                </Grid>
                <Grid item sm={3} xs={6} className={classes.alignRight}>
                  <Button onClick={this.onRefreshClick}>
                    Rafraîchir
                    {
                      isLoading
                        ? <CircularProgress className={classNames(classes.rightIcon, classes.refreshProgress)} />
                        : <RefreshIcon className={classes.rightIcon} />
                    }
                  </Button>
                </Grid>
              </Grid>
              <JobsTable jobs={jobs} />
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }

  refreshJobs() {
    const { instance } = this.props;
    const { server } = instance.opts;
    this.setState({isLoading: true}, () => {
      this.props.dispatch(execActions.listServerJobs(server.serverid))
        .then(result => {
          const filteredJobs = this.filterJobs(result.jobs);
          this.setState({ jobs: filteredJobs});
          if(typeof this.props.onJobsRefreshed !== 'function') return;
          this.props.onJobsRefreshed(filteredJobs);
        })
        .finally(() => {
          this.setState({isLoading: false, jobsOutdated: false});
        });
    });
  }

  onRefreshClick() {
    this.refreshJobs();
  }

  filterJobs(jobs) {
    const { jobFilter } = this.props;
    return jobs.filter(jobFilter);
  }

}

ActionApp.propTypes = {
  classes: PropTypes.object.isRequired,
  jobFilter: PropTypes.func.isRequired,
  actionsBar: PropTypes.element,
  jobsOutdated: PropTypes.bool,
  onJobsRefreshed: PropTypes.func
};

const styles = theme => {
  return {
    root: {
      height: '100%',
      width: '100%',
      margin: 0,
      padding: theme.spacing.unit * 2,
      backgroundColor: theme.palette.background.paper,
    },
    historyContainer: {
      padding: theme.spacing.unit * 2,
      marginBottom: theme.spacing.unit
    },
    actionsBar: {
      padding: theme.spacing.unit * 2,
      marginBottom: theme.spacing.unit
    },
    rightIcon: {
      marginLeft: theme.spacing.unit,
    },
    history: {
      width: '100%',
      padding: theme.spacing.unit * 2,
    },
    alignRight: {
      textAlign: 'right'
    },
    refreshProgress: {
      width: '1.7em !important',
      height: '1.7em !important',
      display: 'inline-block',
    }
  };
};

const mapStateToProps = () => {
  return {};
};

const styledActionApp = withStyles(styles)(ActionApp);
const connectedActionApp = connect(mapStateToProps)(styledActionApp);
export { connectedActionApp as ActionApp };
