import React from 'react';
import {
  Table, TableBody,
  TableCell, TableHead,
  TableRow
} from '@material-ui/core';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { JobStatus } from '../';
import { convertJIDToDate } from '../../helpers';
import { withStyles } from '@material-ui/core/styles';

class JobsTable extends React.Component {
  render() {

    const { jobs, classes } = this.props;

    const rows = jobs.map(j => {
      return (
        <TableRow key={`job-${j.job_id}`}>
          <TableCell>{this.getJobCreationDate(j)}</TableCell>
          <TableCell>{this.getJobStatus(j)}</TableCell>
          <TableCell>{
            j.executed
              ? <Link to={`/jobs/${j.job_id}`}>Voir les détails</Link>
              : null
          }</TableCell>
        </TableRow>
      );
    });

    return (
      <div className={classes.root}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Date de création</TableCell>
              <TableCell>Statut</TableCell>
              <TableCell></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              rows.length
                ? rows :
                <TableRow>
                  <TableCell component={() =>
                    <td colSpan="4" style={{textAlign: 'center'}}>Aucune opération pour l&#39;instant.</td>
                  }/>
                </TableRow>
            }
          </TableBody>
        </Table>
      </div>
    );
  }

  getJobStatus(job) {
    if (job.executed) {
      return <JobStatus status={job.success ? JobStatus.SUCCESS : JobStatus.ERROR} />;
    }
    return <JobStatus status={JobStatus.PENDING} />;
  }

  getJobCreationDate(job) {
    const { job_id } = job;
    const creationDate = convertJIDToDate(job_id);
    return creationDate.toLocaleString();
  }

}

JobsTable.propTypes = {
  jobs: PropTypes.arrayOf(PropTypes.object)
};

const styles = () => ({
  root: {
    overflowX: 'auto'
  }
});

const styledJobsTable = withStyles(styles)(JobsTable);
export { styledJobsTable as JobsTable };
