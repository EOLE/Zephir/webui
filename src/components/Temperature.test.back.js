/* eslint-env jest */
import React from 'react';
import renderer from 'react-test-renderer';
import { Temperature } from './Temperature';
import { theme } from '../helpers/theme';
import { MuiThemeProvider } from '@material-ui/core/styles';

it('should match snapshot', () => {

  const sensor = {
    name: 'CPU 2',
    type: 'temp',
    data: [
      { value: 63, at: new Date() },
      { value: 60, at: new Date() },
      { value: 61, at: new Date() }
    ]
  };

  const component = renderer.create(
    <MuiThemeProvider theme={theme}>
      <Temperature sensor={sensor} />
    </MuiThemeProvider>
  );

  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();

});
