import React from 'react';
import PropTypes from 'prop-types';
import {
  Done as DoneIcon,
  Error as ErrorIcon,
  Schedule as ScheduleIcon,
  Help as HelpIcon,
} from '@material-ui/icons';
import { Tooltip } from '@material-ui/core';

export class JobStatus extends React.Component {
  render() {
    const { tooltipPlacement } = this.props;
    return (
      <Tooltip title={this.getLabel()} placement={tooltipPlacement || 'left'}>
        {this.renderIcon()}
      </Tooltip>
    );
  }
  renderIcon() {
    const { status } = this.props;
    switch(status) {
    case SUCCESS:
      return <DoneIcon />;
    case PENDING:
      return <ScheduleIcon />;
    case ERROR:
      return <ErrorIcon />;
    default:
      return <HelpIcon />;
    }
  }

  getLabel() {
    const { status } = this.props;
    switch(status) {
    case SUCCESS:
      return "Succès";
    case PENDING:
      return "Programmé";
    case ERROR:
      return "Erreur";
    default:
      return "État non géré";
    }
  }
}

const SUCCESS = JobStatus.SUCCESS = 'success';
const PENDING = JobStatus.PENDING = 'pending';
const ERROR = JobStatus.ERROR = 'error';

JobStatus.propTypes = {
  status: PropTypes.oneOf([SUCCESS, PENDING, ERROR]).isRequired,
  tooltipPlacement: PropTypes.string
};