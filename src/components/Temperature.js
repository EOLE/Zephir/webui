import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Card,
  CardHeader,
  CardContent,
  Typography,
} from '@material-ui/core';

import {
} from '@material-ui/icons';

import {
  Sparklines,
  SparklinesLine,
  SparklinesReferenceLine,
} from 'react-sparklines';

import {
  Thermometer as ThermometerIcon
} from '../assets/icons';

const LIMIT = 50;
const MAX = 120;
const MIN = 0;

class Temperature extends React.Component {

  state = { data: [], temp: 60 };

  _updateTemp() {
    let temp = this.state.temp + Math.floor(Math.random() * 8) - 4;
    temp = temp > 120 ? 120 : temp < 40 ? 40 : temp;
    this.setState({
      data: [
        ...this.state.data,
        temp
      ],
      temp
    });
  }

  componentDidMount(){
    this.fakeLive = setInterval(this._updateTemp.bind(this), 1000);
  }

  componentWillUnmount(){
    clearInterval(this.fakeLive);
  }

  render() {
    const { classes, theme, sensor } = this.props;

    const sparklineColor = !this.state.data.length
      ? theme.palette.text.disabled
      : this.state.temp >= 90
        ? theme.palette.error.main
        : this.state.temp >= 70
          ? theme.palette.warning.main
          : theme.palette.success.main
    ;

    return (
      <Card className={classes.card}>
        <CardHeader
          style={{color: sparklineColor }}
          avatar={
            <ThermometerIcon style={{
              fontSize: 'inherit'
            }}/>
          }
          title={sensor.name}
        />
        <CardContent style={{ padding: 0 }}>
          <div className={classes.sparkslineContainer}>
            <Sparklines data={this.state.data}
              limit={LIMIT} min={MIN} max={MAX} >
              <SparklinesLine color={sparklineColor} />
              <SparklinesReferenceLine type="avg" />
            </Sparklines>
            <Typography variant="display1" component="div" className={classes.temp} style={{
              color: sparklineColor, opacity: .5
            }}>
              {this.state.temp}°C
            </Typography>
          </div>
        </CardContent>
      </Card>
    );
  }
}

const styles = theme => ({
  root: {

  },
  card: {
    minWidth: 150
  },
  actions: {
    display: 'flex',
  },
  listButton: {
    justifyContent: 'left',
  },
  rightActions: {
    marginLeft: 'auto',
  },
  leftIcon: {
    marginRight: theme.spacing.unit
  },
  sparkslineContainer: {
    position: 'relative',
    display: 'flex',
    height: theme.spacing.unit * 10,
    '& svg': {
      width: "100%"
    }
  },
  temp: {
    paddingRight: theme.spacing.unit * 2,
    paddingLeft: theme.spacing.unit,
    position: 'absolute',
    zIndex: 1,
    right: 0
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
});

const styledTemperature = withStyles(styles, { withTheme: true })(Temperature);
export { styledTemperature as Temperature };
