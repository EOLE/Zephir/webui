module.exports = function (plop) {
  // app generator
  plop.setGenerator('app', {
    description: 'Créer une nouvelle "app" Zéphir',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'Nom de l\'application à créer ?'
      },
      {
        type: 'input',
        name: 'icon',
        message: 'Icône de l\'application ?'
      },
      {
        type: 'input',
        name: 'category',
        message: 'Catégorie de l\'application ?'
      },
    ],
    actions: [
      {
        type: 'add',
        path: 'src/apps/{{properCase name}}/{{properCase name}}.js',
        templateFile: 'scaffold/app/app.js.hbs'
      },
      {
        type: 'add',
        path: 'src/apps/{{properCase name}}/index.js',
        template: 'export * from \'./{{properCase name}}.js\';'
      },
      {
        type: 'append',
        path: 'src/apps/index.js',
        pattern: /;$/,
        template: 'export { {{properCase name}} } from \'./{{properCase name}}\';'
      }
    ]
  });
  // page generator
  plop.setGenerator('page', {
    description: 'Créer une nouvelle "page" Zéphir',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'Nom de la page à créer ? (sans le suffixe *Page)'
      },
      {
        type: 'input',
        name: 'route',
        message: 'Route de la page à créer ?'
      },
    ],
    actions: [
      {
        type: 'add',
        path: 'src/pages/{{properCase name}}Page/{{properCase name}}Page.js',
        templateFile: 'scaffold/page/page.js.hbs'
      },
      {
        type: 'add',
        path: 'src/pages/{{properCase name}}Page/{{properCase name}}NavRight.js',
        templateFile: 'scaffold/page/navright.js.hbs'
      },
      {
        type: 'add',
        path: 'src/pages/{{properCase name}}Page/index.js',
        template: 'export * from \'./{{properCase name}}Page\';'
      },
      {
        type: 'append',
        path: 'src/pages/index.js',
        pattern: /;$/,
        unique: true,
        template: 'export { {{properCase name}}Page } from \'./{{properCase name}}Page\';'
      }
    ]
  });
};