# Documentation

## Développement

- [Utilisation de l'API Zéphir en développement](./dev/zephir-api-dev.md)
- [Développement React](./dev/dev-react.md)
- [Applications embarquées dans Zéphir](./apps.md)
- [Services, notes de développement](./dev/services.md)
- [Gestion des formulaires](./dev/form.md)
