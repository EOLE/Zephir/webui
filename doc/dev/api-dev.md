# Utiliser l'API du Zéphir en développement

Il est possible de travailler localement sur sa machine tout en exécutant des appels à l'API d'un instance de Zéphir distante. Pour ce faire:

1. Se connecter en SSH sur la machine Zéphir. Si vous travaillez sur l'infrastructure OpenNebula EOLE:

  ```shell
  ssh root@zephir2.ac-test.fr
  ```

2. Se placer dans le répertoire Zéphir

  ```shell
  cd /home/zephir
  ```

3. Démarrer le service `kong-dashboard`

  ```shell
  ./bin/zephir-compose -b staging --helpers -- up kong-dashboard
  ```

4. Ouvrir l'interface de `kong-dashboard` dans votre navigateur. Si vous travaillez sur l'infrastructure OpenNebula d'EOLE:

  http://zephir2.ac-test.fr:8088/

5. Dans la section `APIs` de l'application, cliquez sur le bouton "Modifier" (l'icone "Crayon") correspondant à l'API `ui`.

6. Modifiez le champ `upstream_url` par l'URL de votre serveur de développement (avec votre IP accessible depuis la machine Zéphir). Si vous travaillez sur l'infrastructure OpenNebula d'EOLE, l'URL devrait ressembler à:

  ```
  http://192.168.230.145:3000/
  ```

7. Enregistrez les modifications en cliquant sur le bouton "Save" en bas de la page.

8. Dans votre navigateur, rechargez la page http://zephir2.ac-test.fr:30005/ (si vous travaillez sur l'infrastructure OpenNebula d'EOLE). Vous devriez accéder à votre instance de développement de Zéphir UI.
