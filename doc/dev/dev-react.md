# Développement React

Le framework React a été choisi pour le développement de Zephir UI.
- [React](https://reactjs.org/)
- [Etude et comparaison des frameworks Javascript en 2018](https://dev-eole.ac-dijon.fr/doc/zephir/branches/develop/frameworks.html)

## Modules utilisés

### React Redux

Module permettant d'intégrer une méthode Flux au développement, permettant de livrer un "store" persistant et accessible à tous les composants.

https://react-redux.js.org/

### React Router

Module permettant de gérer efficacement et intuitivement toute la partie navigation entre les pages et les composants.

https://reacttraining.com/react-router/

### Material-ui

Composant permettant d'implémenter le Google Material Design afin de faciliter le développement de l'ui.

https://material-ui.com/
