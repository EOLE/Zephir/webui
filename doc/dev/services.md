# Services: notes de développement

## Affichage de serveurs

### Affichage "Liste"

On gère des serveurs uniquement en lazy-loading :

    getServers(start=<int>, limit=<int>)

    # liste initiale : getServers(0, 30)
    # liste suite : getServers(31, 30)

### Affichage "Tree"

On gère des zones géographiques. On recupère les serveurs appartenant à une zone. On récupère en lazy-loading les sous-zones :

    getLocations(parentId=<int>)

    # noeuds racines : getLocations(null)
    # noeuds enfants : getLocations(parentId)

### Affichage "Map"

On gère des zones géographiques. On recupère les serveurs appartenant à une zone et en fonction d'un périmètre (rayon). Plus le rayon est petit, plus la carte est zoomée, plus on descend dans l'arbre de sites géographiques. A l'inverse, si l'on dézoome, nous n'afficheront que les sites géographiques parents :

    getLocationsByCoord(lat=<float>, lon=<float>, zoom=<int:[0..18]>)

    # noeuds racines / zoom min : getLocationsByCoord(21, 21, 0)
    # noeuds feuilles / zoom max : getLocationsByCoord(21, 21, 18)

### Affichage "Graph"

On gère des zones réseaux. On récupère à partir d'un serveur, la liste des serveurs joignables à travers un ou plusieurs sous-réseaux. Le format graph semble pertinent pour ce type d'affichage car nous manipulons des relations ternaires.

    getZonesByServerId(serverId=<int>)