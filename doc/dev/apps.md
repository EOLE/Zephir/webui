# Applications embarquées

## Constat

### "Actions" Zéphir

Zéphir doit permettre d'effectuer toute une série d'actions sur les serveurs appairés avec celui ci. Ces actions vont du simple redémarrage d'un service au déploiement de configuration.

Afin de modulariser/généraliser l'intégration de nouvelles fonctionnalités dans Zéphir, il a été décidé de présenter toutes les "actions" Zéphir sous forme d'applications indépendantes.

Cela permettra d'isoler les fonctionnalités liées à un domaine d'activité donné et de potentiellement faciliter la gestion d'opérations sur des sélections de serveurs plutôt que sur un serveur unique.

### Applications externes et leur cycle de vie

Zéphir doit pouvoir intégrer des applications externes au sein de sa propre interface (ex: GenConfig, EAD...).

Ces applications nécessitent parfois des opérations avant de pouvoir être affichées dans Zéphir (ex: avant d'ouvrir GenConfig, il faut d'abord demander à Zéphir de démarrer une session d'édition pour un serveur donné). De la même manière, certaines opérations sont parfois nécessaires à la fermeture d'une application.

Certaines applications sont également limités sur le nombre d'instances concurrentes potentielles. Par exemple, il ne peut y avoir qu'une instance GenConfig par serveur donné à un instant T. Il faut donc être capable de prévenir l'ouverture de sessions concurrentes, voir de ré-afficher les instances existantes lorsqu'un utilisateur demande à ouvrir une nouvelle application de ce type.

## Implémentation

Afin de répondre à ces problématiques, une première implémentation a été réalisée permettant de:

- Déclarer une liste d'applications
- Démarrer des instances de ces applications
- Spécifier des opérations de pré-lancement et post-fermeture des différentes instances

### Le répertoire `apps`

Chaque nouvelle "app" intégrée à Zéphir devrait avoir sa propre implémentation dans le répertoire `src/apps`.

#### Générateur

Un générateur a été créé afin de pouvoir générer facilement une nouvelle "app":

```
npm run gen
```

Une série de question vous sera posé afin de renseigner le nom, l'icone et la catégorie de votre application.

La classe de votre nouvelle application sera ensuite générée dans le répertoire `src/apps/<nom_app>`.

#### Structure de classe

Chaque classe de composant destinée à être utilisée comme application devrait correspondre à la signature suivante:

```js
/* globals Promise */
import React from 'react';
import PropTypes from 'prop-types';

class MyApp extends React.Component {

  /**
   * Retourne les métadonnées associées à cette application
   * @static
   * @returns {Object}
   **/
  static getAppMetadata() {
    return {
      label: "MyApp",           // Label de l'application (affiché par exemple sur les boutons pour démarrer une instance de l'application)
      icon: 'GenConfig',        // Nom de l'icône associée à l'application, voir src/components/AppIcon.js
      category: 'configuration' // Catégorie de l'application. Peut être utilisé pour grouper les applications
    };
  }

  /**
   * Retourne le label associé à une instance de l'application
   * @param {Object} instance Modèle de l'instance, voir src/reduces/apps.reducer.js
   * @returns {String}
   */
  static getInstanceLabel(instance) {
    return `MyApp - ${instance.id}`
  }

  /**
   * Hook appelé lors de l'ouverture d'une nouvelle instance de l'application.
   * Celui ci est appelé automatiquement lors de l'utilisation de l'action appActions.openApp(), voir fichier src/actions/app.actions.js
   * @param {Object} instance Nouvelle instance à démarrer
   * @param {Array} opened Liste des instances d'applications déjà ouvertes
   * @returns {Promise} Devrait se résoudre avec un objet de type "instance", modifié en fonction des spécificités de l'application
   */
  static open(instance, opened) {
    return Promise.resolve(instance);
  }

  /**
   * Hook appelé lors de la fermeture d'une instance de l'application.
   * Celui ci est appelé automatiquement lors de l'utilisation de l'action appActions.closeApp(), voir fichier src/actions/app.actions.js
   * @param {Object} instance Nouvelle instance à démarrer
   * @returns {Promise}
   */
  static close(instance) {
    return Promise.resolve();
  }

  /**
   * Effectue le rendu de l'application, la plupart du temps cela correspondra à une iframe pointant vers une application web distante.
   */
  render() {
    return <div>MyApp view...</div>
  }

}

ConfigEditor.propTypes = {
  // Le composant recevra l'objet "instance" lorsqu'il sera rendu dans la page. Voir src/pages/AppsPage/AppDetail.js
  instance: PropTypes.object.isRequired
};

```

