# Utilisation du HOC `withForm`

Le HOC `withForm` a été créé afin de faciliter la gestion du cycle de vie des formulaires. Il permet notamment de normaliser le processus de validation des données avant envoi.

## Utilisation

```js
import React from 'react';
import { withForm } from './form';

class MyForm extends React.Component {

  render() {

    // Le HOC withForm nous fournit
    // automatiquement une prop "form"
    const { form } = this.props;

    return (
      <div>
        <input
          {/*
            L'attribut "name" du champ
            est l'identifiant de celui ci.
          */}
          name="myTextInput"
          type="text"
          {/*
            form.field(fieldName, defaultValue)
            permet de "déclarer" et de "lier"
            un champ avec une valeur.
          */}
          value={ form.field("myTextInput", "default value") }
          {/*
            form.onChange
            permet d'automatiquement récupérer
            les données du champ en cas de
            modification et de les valider.
          */}
          onChange={ form.onChange }
        />
        {/*
          Les erreurs de validation seront
          automatiquement transmises via la propriété
          "errors" de l'objet "form".
        */}
        <span className="error-message">
          { form.errors.myTextInput }
        </span>

        {/*
          La gestion de la soumission du formulaire
          doit se faire via l'utilisation du
          callback "onSubmit" de l'objet "form".
        */}
        <button onClick={form.onSubmit}>Enregistrer</button>

      </div>
    );
  }

  componentDidUpdate() {

    const { form } = this.props;

    // Les propriétés "submitted" et "validated"
    // de la prop "form" permettent
    // de détecter si le formulaire
    // doit être enregistré.
    if (form.submitted && form.validated) {
      // La propriété "data" de la prop "form"
      // contient toutes les données du formulaire.
      this.saveFormData(form.data);
    }

  }

}

// Le premier argument est un objet
// représentant les différents validateurs
// des champs de notre formulaire
const managedForm = withForm({
  myTextInput: (value, formData, submitting) => {
    if (!value && submitting) return "Ce champ ne doit pas être vide.";
  }
})(MyForm);

export { managedForm as MyForm }
```