# Zephir-UI

Client web pour l'application [Zéphir](https://dev-eole.ac-dijon.fr/projects/zephir).

## Démarrer avec les sources

### Dépendances de développement

- [NodeJS >= 8.11 (avec NPM)](https://nodejs.org/en/)

### Procédure rapide

```
git clone ssh://git@dev-eole.ac-dijon.fr/zephir-ui.git
cd zephir-ui/
npm install
npm start
```

L'application sera visible sur http://<uri_zephir>:3000

### Exécuter les tests

```
npm test
```

### Compiler une version pour la production

```
npm run build
```
Le répertoire `./build` contiendra l'application prête à être déployée avec l'ensemble de ses ressources statiques.

## Documentation

La documentation technique est disponible dans le répertoire [`doc`](./doc).

## Licence

[AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0.txt)
