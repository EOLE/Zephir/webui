#!/bin/bash

TKO= curl -X POST \
  -d 'client_id=zephir-api' \
  -d 'username=yo' \
  -d 'password=yo' \
  -d 'grant_type=password' \
'http://zephir2.ac-test.fr:8888/auth/realms/zephir/protocol/openid-connect/token' \
  | jq '.access_token' | tr -d '"'
echo $TKO

curl -X POST \
  --url http://zephir2.ac-test.fr:30005/api/v1/server.create \
  --header "Host: zephir2.ac-test.fr:30005" \
  --header "Authorization: Bearer $TKO" \
  --data '{"servername":"Redhat", "serverdescription": "where s your head at", "servermodelid":2, "serverpassphrase": "MyPassPhrase"}'
curl -X POST \
  --url http://zephir2.ac-test.fr:30005/api/v1/server.create \
  --header "Host: zephir2.ac-test.fr:30005" \
  --header "Authorization: Bearer $TKO" \
  --data '{"servername":"Suse", "serverdescription": "susie de suzon", "servermodelid":2, "serverpassphrase": "MyPassPhrase"}'
curl -X POST \
  --url http://zephir2.ac-test.fr:30005/api/v1/server.create \
  --header "Host: zephir2.ac-test.fr:30005" \
  --header "Authorization: Bearer $TKO" \
  --data '{"servername":"Ubuntu", "serverdescription": "Kululubuntu", "servermodelid":1, "serverpassphrase": "MyPassPhrase"}'
curl -X POST \
  --url http://zephir2.ac-test.fr:30005/api/v1/server.create \
  --header "Host: zephir2.ac-test.fr:30005" \
  --header "Authorization: Bearer $TKO" \
  --data '{"servername":"Manjaro", "serverdescription": "for your ease", "servermodelid":1, "serverpassphrase": "MyPassPhrase"}'
curl -X POST \
  --url http://zephir2.ac-test.fr:30005/api/v1/server.create \
  --header "Host: zephir2.ac-test.fr:30005" \
  --header "Authorization: Bearer $TKO" \
  --data '{"servername":"ArchLinux", "serverdescription": "Rolling Release", "servermodelid":1, "serverpassphrase": "MyPassPhrase"}'
curl -X POST \
  --url http://zephir2.ac-test.fr:30005/api/v1/server.create \
  --header "Host: zephir2.ac-test.fr:30005" \
  --header "Authorization: Bearer $TKO" \
  --data '{"servername":"Debian", "serverdescription": "base of base", "servermodelid":1, "serverpassphrase": "MyPassPhrase"}'
curl -X POST \
  --url http://zephir2.ac-test.fr:30005/api/v1/server.create \
  --header "Host: zephir2.ac-test.fr:30005" \
  --header "Authorization: Bearer $TKO" \
  --data '{"servername":"Mint", "serverdescription": "Hollywood chewing gum", "servermodelid":2, "serverpassphrase": "MyPassPhrase"}'

