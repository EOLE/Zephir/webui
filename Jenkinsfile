pipeline {

  agent  {
    docker { image 'node:8' }
  }
  stages {
    stage('Initialisation du contexte Groovy') {
      steps {
        script {
          NOTIFICATION_LEVEL_INFO = "INFO"
          NOTIFICATION_LEVEL_WARNING = "WARNING"
          NOTIFICATION_LEVEL_ERROR = "ERROR"
          notifications = [:]
        }
      }
    }
    stage('Installation des dépendances') {
      steps {
        sh 'npm update npm -g'
        sh 'npm install --no-audit'
      }
    }
    stage('Exécution des tests unitaires') {
      steps {
        script {
          def ret = sh(
            returnStatus: true,
            script: '''CI=true npm run --silent test'''
          )
          if (ret != 0) {
            currentBuild.result = 'FAILURE'
            addNotification(NOTIFICATION_LEVEL_ERROR, 'Certains tests ont échoués.')
          }
        }
      }
    }
    stage('Vérification des mises à jour') {
      steps {
        script {
          def ret = sh(
            returnStatus: true,
            script: '''npm outdated'''
          )
          if (ret != 0) {
            addNotification(NOTIFICATION_LEVEL_WARNING, 'Certaines dépendances ne sont pas à jour.')
          }
        }
      }
    }
    stage('Vérification des règles de style') {
      steps {
        script {
          def ret = sh(
            returnStatus: true,
            script: '''npm run  --silent lint'''
          )
          if (ret != 0) {
            addNotification(NOTIFICATION_LEVEL_WARNING, 'Certains fichiers ne respectent pas les règles de style.')
          }
        }
      }
    }
    stage('Vérification des alertes de sécurité') {
      steps {
        script {
          def ret = sh(
            returnStatus: true,
            script: '''npm audit'''
          )
          if (ret != 0) {
            message = 'Certaines dépendances du projet ont soulevées des alertes de sécurité.'
            addNotification(NOTIFICATION_LEVEL_WARNING, message)
            error(message)
          }
        }
      }
    }
  }
  post {
    always {
      script {
        if (hasNotifications()) {
          sendBuildReportEmail(currentBuild.currentResult)
        }
      }
    }
  }
}

@NonCPS
def hasNotifications() {
  return getNotificationsMaxLevel() != null
}

@NonCPS
def addNotification(level, message) {
  levelMessages = notifications.get(level, [])
  levelMessages << "${message}"
  notifications[level] = levelMessages
}

@NonCPS
def getNotificationsMaxLevel() {
  Set levels = [NOTIFICATION_LEVEL_ERROR, NOTIFICATION_LEVEL_WARNING, NOTIFICATION_LEVEL_INFO]
  for(int i = 0; i < levels.size(); i++) {
    l = levels[i]
    if (notifications.get(l, []).size() > 0) {
      return l
    }
  }
  return null
}

@NonCPS
def formatNotifications() {
  str = """"""
  levels = notifications.keySet() as String[]
  for(int i = 0; i < levels.size(); i++) {
    l = levels[i]
    n = notifications.get(l, [])
    for(int j = 0; j < n.size(); j++) {
      m = n[j]
      str += """- [${l}] ${m}\n"""
    }
  }
  return str
}

@NonCPS
def sendBuildReportEmail(jenkinsBuildStatus) {

  def notificationsMaxLevel = getNotificationsMaxLevel()
  def buildStatus = jenkinsBuildStatus != "FAILURE" && notificationsMaxLevel != null ? notificationsMaxLevel : jenkinsBuildStatus

  def formattedNotifications = formatNotifications()
  def subject = "${buildStatus} - Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
  def body = """

Des éléments ont été soulevés pendant l'exécution du pipeline:

${formattedNotifications}

Voir les étapes du job: ${env.BUILD_URL}flowGraphTable

Projet: ${env.GIT_URL}
Branche: ${env.GIT_BRANCH}
Commit: ${env.GIT_COMMIT}
"""

  emailext (
    subject: subject,
    body: body,
    recipientProviders: [developers(), requestor()]
  )
}